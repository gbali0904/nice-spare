package com.example.pallaw.electromanage.REST;

import com.example.pallaw.electromanage.REST.Model.resources.User;

/**
 * Created by pallaw on 09/10/17.
 */

public class UserManage {

    /**
     * error : false
     * code : 0
     * msg : Registration successful
     * user : {"id":"1023","first_name":"Pallaw","middel_name":"Kumar","last_name":"Pathak","mobile_no":"7696487712","email_id":"pallaw.pathak@gmail.com","flag":"0","Timestamp":"2017-10-08 18:56:22.362812","password":"&gV-2nw%","admin_id":"1000"}
     */

    private boolean error;
    private int code;
    private String msg;
    private User user;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
