package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.ProductModel;

import java.util.List;

/**
 * Created by pallaw on 12/10/17.
 */

public class AllProductModels {

    /**
     * error : false
     * code : 0
     * msg : List of all model fetched successfully
     * models : [{"id":"2","name":"Y20TVB","Description":"model name ","flag":"1","admin_id":"1000","time_stamp":"2017-10-11 07:06:05.274428"}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<ProductModel> models;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ProductModel> getModels() {
        return models;
    }

    public void setModels(List<ProductModel> models) {
        this.models = models;
    }

}
