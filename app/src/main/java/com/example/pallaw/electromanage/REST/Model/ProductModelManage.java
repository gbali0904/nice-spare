package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.ProductModel;

/**
 * Created by pallaw on 12/10/17.
 */

public class ProductModelManage {

    /**
     * error : false
     * msg : Model Added successful
     * model : {"id":"3","name":"Y20TVB","Description":"model name ","flag":"1","admin_id":"1000","time_stamp":"2017-10-11 21:43:58.064341"}
     */

    private boolean error;
    private String msg;
    private ProductModel model;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ProductModel getModel() {
        return model;
    }

    public void setModel(ProductModel model) {
        this.model = model;
    }

}
