package com.example.pallaw.electromanage.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllEmployees;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.ModelForResetPassword;
import com.example.pallaw.electromanage.REST.Model.resources.User;
import com.example.pallaw.electromanage.UserModule.UserActivity;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.FragmentManagerUtil;
import com.example.pallaw.electromanage.Utils.UserManager;
import com.example.pallaw.electromanage.Utils.Util;
import com.example.pallaw.electromanage.brand.BrandListFragment;
import com.example.pallaw.electromanage.category.CategoryFragment;
import com.example.pallaw.electromanage.employee.EmpListFragment;
import com.example.pallaw.electromanage.file.FileFragment;
import com.example.pallaw.electromanage.product.ProductListFragment;
import com.example.pallaw.electromanage.productmodel.ProductModelListFragment;
import com.example.pallaw.electromanage.subcategory.SubCategoryFragment;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    private TextView txtHeaderEmail;
    private TextView txtHeaderName;
    private List<User> user;
    private Spinner spinnerEmply;
    private User selectedUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        // Setup navigation drawer
        setupNavigationDrawer();
        //add first page according to user type
        addFirstPageToDrawer();
        downlodUserList();

    }

    private void addFirstPageToDrawer() {
        if ((Authentication.getInstance().getData().getStatus().equalsIgnoreCase(AppConstants.USER_TYPE_ADMIN))) {
            FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, new ProductListFragment(), false, ProductListFragment.TAG);
        }
        else{
            if (Authentication.getInstance().getData().getPass_status().equals("0")) {
                changePassword();
            }
            else {
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, new ProductListFragment(), false, ProductListFragment.TAG);
            }
        }
        navView.getMenu().getItem(0).setChecked(true);
    }

    private void changePassword() {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.change_password_activity, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialoglayout);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        final EditText etforpass = (EditText) dialoglayout.findViewById(R.id.edtLoginPassword);
        final  EditText etforpassconfirm = (EditText) dialoglayout.findViewById(R.id.edtLoginPassword);
        Button submitpass = (Button) dialoglayout.findViewById(R.id.btnchnagePass);
        submitpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass = etforpass.getText().toString().trim();
                String passconf = etforpassconfirm.getText().toString().trim();
                if (TextUtils.isEmpty(pass)) {
                    etforpass.setError("Please Enter Password");
                    return;
                }
                if (TextUtils.isEmpty(passconf)) {
                    etforpassconfirm.setError("Please Confirm Password");
                    return;
                }
                if (!pass.equals(passconf)) {
                    etforpassconfirm.setError("Password Not Match");
                    return;
                }
                changePasswordApi(alertDialog,pass);
            }
        });
        alertDialog.show();
    }

    private void changePasswordApi(final AlertDialog alertDialog, String pass) {
        Util.showProgressDialog(this);
        DataManager.getInstance().changePassword(pass,Authentication.getInstance().getData().getId(), new Callback<Authentication>() {
            @Override
            public void onResponse(Call<Authentication> call, Response<Authentication> response) {
                Util.hideProgressDialog();
                alertDialog.dismiss();
                String responseJson = new Gson().toJson(response.body(), Authentication.class);
                Timber.i("downloadProductModels", responseJson);
                Toast.makeText(MainActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                if(!(response.body().isError())){
                    UserManager.getInstance().logoutUser();
                    UserManager.getInstance().createLoginSession(response.body());
                    FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, new ProductListFragment(), false, ProductListFragment.TAG);
                }
                else {
                    UserManager.getInstance().logoutUser();
                    //After logout redirect user to Loing Activity
                    Intent i = new Intent(MainActivity.this, UserActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    // Staring Login Activity
                    startActivity(i);
                }
            }
            @Override
            public void onFailure(Call<Authentication> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadProductModels", t.getMessage());
                UserManager.getInstance().logoutUser();
                //After logout redirect user to Loing Activity
                Intent i = new Intent(MainActivity.this, UserActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                // Staring Login Activity
                startActivity(i);
                t.printStackTrace();
            }
        });
    }

    private void setupNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        navView.setNavigationItemSelectedListener(this);

        View headerView = navView.getHeaderView(0);
        txtHeaderEmail = (TextView) headerView.findViewById(R.id.txtHeaderEmail);
        txtHeaderName = (TextView) headerView.findViewById(R.id.txtHeaderName);

        //if current session is for normal user then invalidate admin menu items
        if (!(Authentication.getInstance().getData().getStatus().equalsIgnoreCase(AppConstants.USER_TYPE_ADMIN))) {
            navView.getMenu().clear();
            navView.inflateMenu(R.menu.activity_main_drawer_user);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        setupDashBoardData();
    }

    private void setupDashBoardData() {

        if (UserManager.getInstance().isLoggedIn()) {
            User user = Authentication.getInstance().getData();
            txtHeaderName.setText(String.format("%s %s %s", user.getFirst_name(), user.getMiddel_name(), user.getLast_name()));
            //if current session is for normal user then invalidate admin menu items
            txtHeaderEmail.setText(user.getMobile_no());



        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_logout:
                UserManager.getInstance().logoutUser();

                //After logout redirect user to Loing Activity
                Intent i = new Intent(MainActivity.this, UserActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                // Staring Login Activity
                startActivity(i);
                break;

            case R.id.nav_user:
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, EmpListFragment.getInstance(), false, EmpListFragment.TAG);
                break;
            case R.id.nav_category:
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, new CategoryFragment(), false, CategoryFragment.TAG);
                break;
            case R.id.nav_sub_category:
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, new SubCategoryFragment(), false, SubCategoryFragment.TAG);
                break;
            case R.id.nav_brands:
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, new BrandListFragment(), false, BrandListFragment.TAG);
                break;
            case R.id.nav_model:
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, new ProductModelListFragment(), false, ProductModelListFragment.TAG);
                break;
            case R.id.nav_product:
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, new ProductListFragment(), false, ProductListFragment.TAG);
                break;
            case R.id.nav_file:
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.frameMain, new FileFragment(), false, FileFragment.TAG);
                break;
            case R.id.nav_rest_user:
                 LayoutInflater inflater = getLayoutInflater();
                View dialoglayout = inflater.inflate(R.layout.reset_password_activity, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setView(dialoglayout);
                builder.setCancelable(false);
                final AlertDialog alertDialog = builder.create();
                Button submit = (Button) dialoglayout.findViewById(R.id.btnresetPass);
                Button cancel = (Button) dialoglayout.findViewById(R.id.btncancel);
                spinnerEmply = (Spinner) dialoglayout.findViewById(R.id.spiForEmpl);
                spinnerEmply.setAdapter(new EmployeeAdapter(MainActivity.this, R.layout.item_spinner, user));
                spinnerEmply.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        selectedUsers = user.get(position);
                        //download product model
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resetPassword(alertDialog);
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();


                break;

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void resetPassword(final AlertDialog alertDialog) {
        //selectedUsers

        if (selectedUsers.getId().equals("0"))
        {
            Toast.makeText(getApplicationContext(),"Please Select one user",Toast.LENGTH_LONG).show();
        }
        else {
            Util.showProgressDialog(MainActivity.this);
            DataManager.getInstance().resetPassword(selectedUsers.getId(), new Callback<ModelForResetPassword>() {
                @Override
                public void onResponse(Call<ModelForResetPassword> call, Response<ModelForResetPassword> response) {
                    Util.hideProgressDialog();
                    String responseJson = new Gson().toJson(response.body());
                    Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_LONG).show();
                    alertDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ModelForResetPassword> call, Throwable t) {
                    Util.hideProgressDialog();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    alertDialog.dismiss();
                }
            });
        }
    }
    private void downlodUserList() {
        DataManager.getInstance().getAllUsers("", "", "", "","0", new Callback<AllEmployees>() {
            @Override
            public void onResponse(Call<AllEmployees> call, Response<AllEmployees> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllEmployees.class);
                user = response.body().getUser();
            }

            @Override
            public void onFailure(Call<AllEmployees> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downlodUserList", t.getMessage());
                t.printStackTrace();
            }
        });
    }
    public class EmployeeAdapter  extends ArrayAdapter<User> {

        private final List<User> user;

        public EmployeeAdapter(MainActivity mainActivity, int item_spinner, List<User> user) {


            super(mainActivity, item_spinner, user);
            // TODO Auto-generated constructor stub
            this.user = user;
        }


        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.item_spinner, parent, false);
            TextView label = (TextView) row.findViewById(R.id.txtItemSpinnerCategory);
            label.setText(user.get(position).getFirst_name() +user.get(position).getLast_name()  );
            return row;
        }
    }
}
