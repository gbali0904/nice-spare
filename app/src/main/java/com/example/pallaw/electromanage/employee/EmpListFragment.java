package com.example.pallaw.electromanage.employee;


import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllEmployees;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleUser;
import com.example.pallaw.electromanage.REST.Model.resources.User;
import com.example.pallaw.electromanage.REST.UserManage;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.example.pallaw.electromanage.category.SortDialogForCategory;
import com.example.pallaw.electromanage.product.SortDialog;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmpListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener {

    static List<User> list_items = new ArrayList<>();

    public static final String TAG = EmpListFragment.class.getSimpleName();
    private static EmpListFragment mInstance;
    @BindView(R.id.list)
    SuperRecyclerView list;
    @BindView(R.id.fab)
    Button fab;
    private LinearLayoutManager layoutManager;
    private EmpListAdapter adapter;
    private SparseItemRemoveAnimator mSparseAnimator;
    private MenuItem searchMenuItem;
    private MenuItem sortMenuItem;
    private SearchView searchView;
    private ArrayList<User> selected_usersList;
    private StringBuilder builder;
    private android.view.ActionMode mActionMode;

    private String deleteMultipUser;
    private List<User> listForSearch=new ArrayList<>();

    public EmpListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.product, menu);
        setupOptionMenu(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private boolean setupOptionMenu(Menu menu) {
        searchMenuItem = menu.findItem(R.id.searchItem);
        sortMenuItem = menu.findItem(R.id.action_sort);

        if (searchMenuItem != null) {
            searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
            if (null == searchView) {
                return true;
            }
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    submitQuerry(query);
                    return true;
                }
            });
            searchView.setQueryHint(getString(R.string.action_search_emp));
            //applying the font to hint text inside of SearchView
            SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
            searchText.setHintTextColor(getContext().getResources().getColor(R.color.colorAccent));
            searchText.setTextColor(getContext().getResources().getColor(R.color.white));
            searchText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        return false;
    }

    private void submitQuerry(String query) {
//        remoteSearch(query);
        localSearch(query);
    }

    private void localSearch(String query) {
        Timber.e(query);
        ArrayList<User> searchList=new ArrayList<>();
        for (User user : listForSearch) {
            if (user.getFirst_name().toLowerCase().contains(query)||
                    user.getLast_name().toLowerCase().contains(query)||
                    user.getMiddel_name().toLowerCase().contains(query)||
                    user.getMobile_no().toLowerCase().contains(query)) {
                searchList.add(user);
            }
        }

        if(searchList.size()>0){//if found something related to search querry
            list_items.clear();
            list_items.addAll(searchList);
            adapter.notifyDataChanged();
        }
        Timber.e("querry- "+query+", search result size-"+searchList.size());
    }

    private void remoteSearch(String query) {
        DataManager.getInstance().getAllUsers("", query, "", "","", new Callback<AllEmployees>() {
            @Override
            public void onResponse(Call<AllEmployees> call, Response<AllEmployees> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllEmployees.class);
                Timber.i("downlodUserList", responseJson);
                //  Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                if (!response.body().isError() ) {
                    list_items.clear();
                    list_items.addAll(response.body().getUser());
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<AllEmployees> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downlodUserList", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                SortDialogForCategory sortDialog = SortDialogForCategory.getInstance(new SortDialogForCategory.OnCategorySortListener() {
                    @Override
                    public void onCategorySort(Dialog dialog, String sortingType) {
                        dialog.dismiss();


                        sortListAccTo(sortingType);
                    }
                });
                sortDialog.show(getActivity().getSupportFragmentManager(), SortDialog.TAG);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortListAccTo(String sortingType) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllUsers("", "", "","", sortingType, new Callback<AllEmployees>() {
            @Override
            public void onResponse(Call<AllEmployees> call, Response<AllEmployees> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllEmployees.class);
                Timber.i("downlodUserList", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    list_items.clear();
                    list_items.addAll(response.body().getUser());
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<AllEmployees> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downlodUserList", t.getMessage());
                t.printStackTrace();
            }
        });

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_emp_list, container, false);
        ButterKnife.bind(this, view);
        layoutManager = new LinearLayoutManager(getActivity());

        adapter = new EmpListAdapter(list_items, new OnEmpSelectedListener() {
            @Override
            public void onEmpSelected(ArrayList<User> user, int adapterPosition) {
                // removeUser(user, adapterPosition);
                selected_usersList = user;
                if (selected_usersList.size()>0) {
                    if (mActionMode == null)
                    {
                        mActionMode = getActivity().startActionMode(mActionModeCallback);
                    }
                }
                else {
                    if (mActionMode != null) {
                        mActionMode.finish();
                        selected_usersList.clear();
                    }
                }
            }
        }, new OnEmpStatusChangeListener() {
            @Override
            public void onEmpStatusChange(User mItem, int adapterPosition, String status) {
                changeEmpStatus(mItem, adapterPosition, status);
            }
        }, new OnEmpUpdateListener() {
            @Override
            public void onEmpUpdate(User mItem, int adapterPosition) {
                Intent toUpdateEmp= new Intent(getActivity(),AddEmpActivity.class);
                toUpdateEmp.putExtra(AppConstants.EXTRA_UPDATE_ITEM,mItem);
                toUpdateEmp.putExtra(AppConstants.EXTRA_ADAPTER_POSITION,adapterPosition);
                startActivityForResult(toUpdateEmp,AppConstants.REQUEST_CODE_TO_UPADATE);
            }
        });

        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        list.setRefreshListener(this);
        list.setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);
//        list.setupMoreListener(this, 1);

        downlodUserList();
        return view;
    }

    /*
    * Action context menu
    * */
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            mActionMode.getMenuInflater().inflate(R.menu.context, menu);
            return true;

        }
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    showItemDeleteDialog();
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    private void showItemDeleteDialog() {
        AlertDialog.Builder dilaogbuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
        } else {
            dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
        }
        dilaogbuilder.setTitle("Delete Product")
                .setCancelable(true)
                .setMessage("Are you sure you want to delete this Product?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        builder=new StringBuilder();
                        for (User user :
                                selected_usersList) {
                            builder.append(user.getId()).append(",");
                        }
                        deleteMultipUser = builder.toString().substring(0, builder.length() - 1);

                        deleteUsers(deleteMultipUser);
                        Log.e("this","Ids are :"+builder.toString().substring(0, builder.length() - 1));
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void deleteUsers(final String deleteMultipUser) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().deleteMultipleUsers(deleteMultipUser, new Callback<DeleteMultipleUser>() {
            @Override
            public void onResponse(Call<DeleteMultipleUser> call, Response<DeleteMultipleUser> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), DeleteMultipleUser.class);
                Timber.i("removeProduct", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    adapter.notifyDataSetChanged();
                    mActionMode.finish();
                    list_items.clear();
                    selected_usersList.clear();
                    downlodUserList();

                }
            }

            @Override
            public void onFailure(Call<DeleteMultipleUser> call, Throwable t) {
                Timber.e("removeProduct", t.getMessage());
                t.printStackTrace();
            }
        });

    }

    private void changeEmpStatus(User user, final int adapterPosition, String status) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().userManage(user.getFirst_name(), user.getMiddel_name(), user.getLast_name(), user.getMobile_no(), user.getEmail_id(), AppConstants.ACTION_TYPE_UPDATE, user.getId(), status, new Callback<UserManage>() {
            @Override
            public void onResponse(Call<UserManage> call, Response<UserManage> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), UserManage.class);
                Timber.i("removeUser",responseJson);
                Toast.makeText(getActivity(), ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if(!(response.body().isError())){
                    adapter.updateStatus(adapterPosition,response.body().getUser().getFlag());
                }
            }

            @Override
            public void onFailure(Call<UserManage> call, Throwable t) {
                Util.hideProgressDialog();
            }
        });
    }

    private void removeUser(User user, final int adapterPosition) {
        Util.showProgressDialog(getActivity());
        String admin_id = Authentication.getInstance().getData().getAdmin_id();
        DataManager.getInstance().userManage(user.getFirst_name(), user.getMiddel_name(), user.getLast_name(), user.getMobile_no(), user.getEmail_id(), AppConstants.ACTION_TYPE_DELETE, user.getId(), "0", new Callback<UserManage>() {
            @Override
            public void onResponse(Call<UserManage> call, Response<UserManage> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), UserManage.class);
                Timber.i("removeUser",responseJson);
                Toast.makeText(getActivity(), ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if(!(response.body().isError())){
                    adapter.remove(adapterPosition);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<UserManage> call, Throwable t) {
                Util.hideProgressDialog();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getActivity().getResources().getString(R.string.title_employees_list_fragment));

    }

    private void downlodUserList() {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllUsers("", "", "", "","", new Callback<AllEmployees>() {
            @Override
            public void onResponse(Call<AllEmployees> call, Response<AllEmployees> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllEmployees.class);
                Timber.i("downlodUserList", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    list_items.clear();
                    list_items.addAll(response.body().getUser());
                    adapter.notifyDataChanged();

                    listForSearch.clear();
                    listForSearch.addAll(list_items);
                }

            }

            @Override
            public void onFailure(Call<AllEmployees> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downlodUserList", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
//        Toast.makeText(getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
        downlodUserList();
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        Toast.makeText(getActivity(), "More", Toast.LENGTH_SHORT).show();
        User user = new User();
        user.setFirst_name("More asked, more served");
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivityForResult(new Intent(getActivity(),AddEmpActivity.class),AppConstants.REQUEST_CODE_TO_ADD);
    }

    public static EmpListFragment getInstance() {
        if(mInstance==null){
            mInstance=new EmpListFragment();
        }
        return mInstance;
    }

    interface OnEmpSelectedListener {
        void onEmpSelected(ArrayList<User> mItem, int adapterPosition);
    }

    interface OnEmpStatusChangeListener {
        void onEmpStatusChange(User mItem, int adapterPosition, String status);
    }

    interface OnEmpUpdateListener {
        void onEmpUpdate(User mItem, int adapterPosition);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK) {
            switch (requestCode){
                case AppConstants.REQUEST_CODE_TO_ADD:
                    list_items.add(((User) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM)));
                    adapter.notifyDataSetChanged();
                    break;
                case AppConstants.REQUEST_CODE_TO_UPADATE:
                    list_items.set(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION,0),
                            ((User) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM)));
                    adapter.notifyItemChanged(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION,0));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
