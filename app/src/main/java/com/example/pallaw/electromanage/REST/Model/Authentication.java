package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.User;
import com.google.gson.Gson;

/**
 * Created by pallaw on 04/10/17.
 */

public class Authentication {
    private static Authentication ourInstance = new Authentication();

    public static Authentication getInstance() {
        return ourInstance;
    }

    private Authentication() {
    }

    public static void setInstance(String json) {
        ourInstance=null;
        Authentication authentication = new Gson().fromJson(json, Authentication.class);
        ourInstance=authentication;
    }

    public static void setInstance(Authentication authResponse) {
        ourInstance=authResponse;
    }

    /**
     * error : false
     * code : 0
     * msg : UserModel Successful
     * data : {"id":"1000","FirstName":"admin","Middel Name":"admin","Last Name":"admin","Mobile No":"1234567890","Email Id":"admin@gmail.com","Flag":"1","TimeStamp":"2017-10-02 19:57:31.205916","password":"123456"}
     */

    private boolean error;
    private int code;
    private String msg;
    private User data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }


}
