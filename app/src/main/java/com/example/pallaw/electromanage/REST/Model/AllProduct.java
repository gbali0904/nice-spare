package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.Product;

import java.util.List;

/**
 * Created by pallaw on 13/10/17.
 */

public class AllProduct {

    /**
     * error : false
     * code : 0
     * msg : List of all Product fetched successfully
     * products : [{"id":"6","Description":"here is my product","flag":"0","product_name":"product name","stock_status":"n","timestamp":"2017-10-12 05:22:50.371196","unit_flag":"10","price":"10000","admin_id":"1000","brand":{"id":"8","name":"lenovo"},"model":{"id":"2","name":"Y20TVB"},"category":{"id":"21","name":"Cooler"},"sub_category":{"id":"9","name":"New cooler"}},{"id":"7","Description":"here is my product","flag":"0","product_name":"product name","stock_status":"n","timestamp":"2017-10-12 05:23:29.020298","unit_flag":"10","price":"10000","admin_id":"1000","brand":{"id":"8","name":"lenovo"},"model":{"id":"2","name":"Y20TVB"},"category":{"id":"21","name":"Cooler"},"sub_category":{"id":"9","name":"New cooler"}},{"id":"8","Description":"here is my testing","flag":"0","product_name":"testing","stock_status":"n","timestamp":"2017-10-12 05:24:00.643898","unit_flag":"10","price":"10000","admin_id":"1000","brand":{"id":"8","name":"lenovo"},"model":{"id":"2","name":"Y20TVB"},"category":{"id":"31","name":"laptop"},"sub_category":{"id":"5","name":"sony"}},{"id":"9","Description":"here is my computer","flag":"0","product_name":"pavilion","stock_status":"y","timestamp":"2017-10-12 05:25:24.613126","unit_flag":"10","price":"45000","admin_id":"1000","brand":{"id":"10","name":"lenovo"},"model":{"id":"5","name":"DEF Model"},"category":{"id":"31","name":"laptop"},"sub_category":{"id":"8","name":"pavilion"}}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<Product> products;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
