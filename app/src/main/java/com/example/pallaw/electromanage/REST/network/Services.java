/*
 * Copyright (c) Joaquim Ley 2016. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.pallaw.electromanage.REST.network;

import com.example.pallaw.electromanage.REST.Model.AllBrands;
import com.example.pallaw.electromanage.REST.Model.AllCategories;
import com.example.pallaw.electromanage.REST.Model.AllEmployees;
import com.example.pallaw.electromanage.REST.Model.AllFilterProduct;
import com.example.pallaw.electromanage.REST.Model.AllProduct;
import com.example.pallaw.electromanage.REST.Model.AllProductModels;
import com.example.pallaw.electromanage.REST.Model.AllSubCategory;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.BrandManage;
import com.example.pallaw.electromanage.REST.Model.CategoryOperation;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleBrand;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleCategory;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleModel;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleProduct;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleSubcategory;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleUser;
import com.example.pallaw.electromanage.REST.Model.ModelForFilterList;
import com.example.pallaw.electromanage.REST.Model.ModelForResetPassword;
import com.example.pallaw.electromanage.REST.Model.ProductModelManage;
import com.example.pallaw.electromanage.REST.Model.ProductOperation;
import com.example.pallaw.electromanage.REST.Model.SearchResult;
import com.example.pallaw.electromanage.REST.Model.SubCategoryOperation;
import com.example.pallaw.electromanage.REST.Model.resources.UploadFile;
import com.example.pallaw.electromanage.REST.UserManage;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface Services {

    @FormUrlEncoded
    @POST("login.php")
    Call<Authentication> login(@Field("userid") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("geAllUser.php")
    Call<AllEmployees> getallUsers(@Field("id") String id,
                                   @Field("name") String name,
                                   @Field("mobileNo") String mobileNo,
                                   @Field("short_name") String short_name,
                                   @Field("flag") String flag

    );

    @FormUrlEncoded
    @POST("userManage.php")
    Call<UserManage> userManage(@Field("name") String name,
                                @Field("MiddelName") String MiddelName,
                                @Field("LastName") String LastName,
                                @Field("phone") String phone,
                                @Field("email") String email,
                                @Field("action_type") String action_type,
                                @Field("user_id") String user_id,
                                @Field("admin_id") String admin_id,
                                @Field("Flag") String Flag
    );

    @FormUrlEncoded
    @POST("getAllCategory.php")
    Call<AllCategories> getAllCategories(
            @Field("id") String id,
            @Field("name") String name,
            @Field("model_id") String model_id,
            @Field("brand_id") String brand_id,
            @Field("flag") String flag,
            @Field("short_name") String short_name
    );

    @FormUrlEncoded
    @POST("categoryOperation.php")
    Call<CategoryOperation> categoryOperation(
            @Field("name") String name,
            @Field("Description") String Description,
            @Field("flag") String flag,
            @Field("adminid") String adminid,
            @Field("action_type") String action_type,
            @Field("category_id") String category_id
    );

    @FormUrlEncoded
    @POST("getAllSubCategory.php")
    Call<AllSubCategory> getAllSubCategory(
            @Field("id") String id,
            @Field("name") String name,
            @Field("flag") String flag,
            @Field("short_name") String short_name,
            @Field("category_id") String category_id

    );

    @FormUrlEncoded
    @POST("subCategoryOperation.php")
    Call<SubCategoryOperation> subCategoryOperation(
            @Field("name") String name,
            @Field("Description") String Description,
            @Field("flag") String flag,
            @Field("adminid") String adminid,
            @Field("category_id") String category_id,
            @Field("action_type") String action_type,
            @Field("subcategory_id") String subcategory_id
    );

    @FormUrlEncoded
    @POST("getAllBrand.php")
    Call<AllBrands> getAllBrands(
            @Field("id") String id,
            @Field("name") String category_id,
            @Field("flag") String flag,
            @Field("short_name") String short_name
    );

    @FormUrlEncoded
    @POST("getAllModel.php")
    Call<AllProductModels> getAllProductModel(
            @Field("id") String id,
            @Field("name") String category_id,
            @Field("flag") String flag,
            @Field("short_name") String short_name,
            @Field("brands_id") String brands_id
    );



    @FormUrlEncoded
    @POST("brandManage.php")
    Call<BrandManage> brandManage(
            @Field("name") String name,
            @Field("Description") String Description,
            @Field("flag") String flag,
            @Field("adminid") String adminid,
            @Field("action_type") String action_type,
            @Field("brand_id") String brand_id
    );

    @FormUrlEncoded
    @POST("modelManage.php")
    Call<ProductModelManage> productModelManage(
            @Field("name") String name,
            @Field("Description") String Description,
            @Field("flag") String flag,
            @Field("adminid") String adminid,
            @Field("action_type") String action_type,
            @Field("model_id") String model_id,
            @Field("brand_id") String brand_id
    );

    @FormUrlEncoded
    @POST("getProductList.php")
    Call<AllProduct> getAllProduct(
            @Field("product_name") String product_name,
            @Field("category_id") String category_id,
            @Field("subcategory_id") String subcategory_id,
            @Field("brand_id") String brand_id,
            @Field("model_id") String model_id,
            @Field("stock_status") String stock_status,
            @Field("price") String price,
            @Field("filter") String filter,
            @Field("flag") String flag
    );

    @FormUrlEncoded
    @POST("productOperation.php")
    Call<ProductOperation> productOperation(
            @Field("product_name") String product_name,
            @Field("Description") String Description,
            @Field("flag") String flag,
            @Field("adminid") String adminid,
            @Field("category_id") String category_id,
            @Field("subcategory_id") String subcategory_id,
            @Field("action_type") String action_type,
            @Field("product_id") String product_id,
            @Field("brand_id") String brand_id,
            @Field("model_id") String model_id,
            @Field("stock_status") String stock_status,
            @Field("price") String price,
            @Field("unit_flag") String unit_flag
    );

    @FormUrlEncoded
    @POST("searchAllProduct.php")
    Call<SearchResult> searchProduct(
            @Field("name") String name,
            @Field("flag") String flag
    );


    @Multipart
    @POST("csvUpdate.php")
    Call<UploadFile> uploadcsvFile(@PartMap() HashMap<String, RequestBody> multipartRequest, @Part MultipartBody.Part uploadFile);

    @Multipart
    @POST("brandUpdate.php")
    Call<UploadFile> branduploadcsvFile(@PartMap() HashMap<String, RequestBody> multipartRequest, @Part MultipartBody.Part uploadFile);


    @Multipart
    @POST("modelUpdate.php")
    Call<UploadFile> modelUploadcsvFile(@PartMap() HashMap<String, RequestBody> multipartRequest, @Part MultipartBody.Part uploadFile);


    @Multipart
    @POST("categoryUpdate.php")
    Call<UploadFile> categoryUploadcsvFile(@PartMap() HashMap<String, RequestBody> multipartRequest, @Part MultipartBody.Part uploadFile);

    @Multipart
    @POST("subcategoryUpdate.php")
    Call<UploadFile> subcategoryUploadcsvFile(@PartMap() HashMap<String, RequestBody> multipartRequest, @Part MultipartBody.Part uploadFile);



    @FormUrlEncoded
    @POST("deleteMultipleproduct.php")
    Call<DeleteMultipleProduct> deleteMultipleproduct(@Field("product_id") String product_id);

    @FormUrlEncoded
    @POST("deleteMultipleUser.php")
    Call<DeleteMultipleUser> deleteMultipleUsers(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("deleteMultipleCategory.php")
    Call<DeleteMultipleCategory> deleteMultipleCategory(@Field("category_id") String category_id);
    @FormUrlEncoded
    @POST("deleteMultiplesubCategory.php")
    Call<DeleteMultipleSubcategory> deleteMultipleSubCategory(@Field("subcategory_id") String subcategory_id);

    @FormUrlEncoded
    @POST("deleteMultipleBrand.php")
    Call<DeleteMultipleBrand> deleteMultipleBrand(@Field("brand_id") String brand_id);


    @FormUrlEncoded
    @POST("deleteMultipleModel.php")
    Call<DeleteMultipleModel> deleteMultipleModel(@Field("model_id") String model_id);

    @FormUrlEncoded
    @POST("getAllFilterCategory.php")
    Call<AllSubCategory> getAllFilterCategory(@Field("category_id") String category_id);

    @FormUrlEncoded
    @POST("getAllFilterModel.php")
    Call<AllProductModels> getAllFilterModel(@Field("model_id") String model_id);

    @FormUrlEncoded
    @POST("getAllFilterProduct.php")
    Call<AllFilterProduct> getAllFilterProduct(@Field("filter_data") String filter_data,@Field("flag") String flag);


    @GET("getFilterList.php")
    Call<ModelForFilterList> getFilterList();

    @FormUrlEncoded
    @POST("changePassword.php")
    Call<Authentication> changePassword(@Field("pass") String pass,@Field("id") String id);

    @FormUrlEncoded
    @POST("resetPassword.php")
    Call<ModelForResetPassword> resetPassword(@Field("id") String id);

    @FormUrlEncoded
    @POST("categoryUpdateOperation.php")
    Call<CategoryOperation> categoryUpdateOperations(@Field("category")  String category);

}


