package com.example.pallaw.electromanage.product;

import android.util.Log;

import com.example.pallaw.electromanage.REST.Model.ModelForFilterList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenreDataFactoryForFilter {


    private static List<ModelForFilterList.FilterBean> filterlist=new ArrayList<>();
    private static FilterList brandlist;
    private static List<FilterList> artistsBrands=new ArrayList<>();

    private static List<FilterList> artistModel=new ArrayList<>();
    private static FilterList modellist;

    private static List<FilterList> artistCategory=new ArrayList<>();
    private static FilterList categorylist;


    private static List<FilterList> artistSubCategory=new ArrayList<>();
    private static FilterList subcategorylist;
    private static List<ModelForFilterList.FilterBean.BrandsBean> brands=new ArrayList<>();
    private static List<ModelForFilterList.FilterBean.ModelBean> model=new ArrayList<>();
    private static List<ModelForFilterList.FilterBean.CategoryBean> category=new ArrayList<>();
    private static List<ModelForFilterList.FilterBean.SubcategoryBean> subcategory=new ArrayList<>();

    public static List<MultiCheckGenre> makeMultiCheckFilters(List<ModelForFilterList.FilterBean> filterResponse) {
        filterlist.clear();
        brands.clear();
        model.clear();
        category.clear();
        subcategory.clear();
        artistsBrands.clear();
        artistModel.clear();
        artistCategory.clear();
        artistSubCategory.clear();


        filterlist=filterResponse;
        return Arrays.asList(makeMultiCheckRockBrands(),
                makeMultiCheckModels(),
                makeMultiCheckCategories(),
                makeMultiCheckSubCategories());
    }


    public static MultiCheckGenre makeMultiCheckRockBrands() {
        return new MultiCheckGenre("Brands", makeBrandFilter());
    }


    public static List<FilterList> makeBrandFilter() {


     brands = filterlist.get(0).getBrands();
        for (ModelForFilterList.FilterBean.BrandsBean brandsBean :
                brands) {
            brandlist = new FilterList(brandsBean.getName(),brandsBean.getId(),"brands", false);
            artistsBrands.add(brandlist);
        }
        return artistsBrands;
    }

    public static MultiCheckGenre makeMultiCheckModels() {
        return new MultiCheckGenre("Model", makeModelFilter());
    }



    public static List<FilterList> makeModelFilter() {
         model = filterlist.get(1).getModel();
        for (ModelForFilterList.FilterBean.ModelBean brandsBean :
                model) {
            modellist = new FilterList(brandsBean.getName(), brandsBean.getId(), "models", false);
            artistModel.add(modellist);
        }
        return artistModel;

    }

    public static MultiCheckGenre makeMultiCheckCategories() {
        return new MultiCheckGenre("Categories", makeCategoryFilter());
    }


    public static List<FilterList> makeCategoryFilter() {
        category = filterlist.get(2).getCategory();
        for (ModelForFilterList.FilterBean.CategoryBean brandsBean :
                category) {
            categorylist = new FilterList(brandsBean.getName(), brandsBean.getId(), "category", false);
            artistCategory.add(categorylist);
        }
        return artistCategory;
    }


    public static MultiCheckGenre makeMultiCheckSubCategories() {
        return new MultiCheckGenre("Sub-Categories", makeSubCategoryFilter());
    }


    public static List<FilterList> makeSubCategoryFilter() {
        subcategory = filterlist.get(3).getSubcategory();
        for (ModelForFilterList.FilterBean.SubcategoryBean brandsBean :
                subcategory) {
            subcategorylist = new FilterList(brandsBean.getName(), brandsBean.getId(), "subcategory", false);
            artistSubCategory.add(subcategorylist);
        }
        return artistSubCategory;
    }






}

