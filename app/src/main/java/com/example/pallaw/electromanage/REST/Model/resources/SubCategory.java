package com.example.pallaw.electromanage.REST.Model.resources;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pallaw on 10/10/17.
 */

public class SubCategory implements Parcelable {

    /**
     * id : 4
     * name : sony
     * description : here is my sony tv
     * flag : 1
     * timestamp : 2017-10-09 16:37:20.626816
     * adminid : 1000
     * category_id : 16
     */

    private String id;
    private String name;
    private String description;
    private String flag;
    private String timestamp;
    private String adminid;
    private Category category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAdminid() {
        return adminid;
    }

    public void setAdminid(String adminid) {
        this.adminid = adminid;
    }





    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.flag);
        dest.writeString(this.timestamp);
        dest.writeString(this.adminid);
        dest.writeParcelable(this.category, flags);
    }


    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }



    protected SubCategory(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.flag = in.readString();
        this.timestamp = in.readString();
        this.adminid = in.readString();

        this.category = in.readParcelable(Category.class.getClassLoader());
    }

    public static final Parcelable.Creator<SubCategory> CREATOR = new Parcelable.Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel source) {
            return new SubCategory(source);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };
}
