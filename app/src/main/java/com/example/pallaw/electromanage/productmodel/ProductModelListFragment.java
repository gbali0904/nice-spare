package com.example.pallaw.electromanage.productmodel;


import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllProductModels;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleModel;
import com.example.pallaw.electromanage.REST.Model.ProductModelManage;
import com.example.pallaw.electromanage.REST.Model.resources.ProductModel;
import com.example.pallaw.electromanage.REST.Model.resources.User;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.example.pallaw.electromanage.category.SortDialogForCategory;
import com.example.pallaw.electromanage.product.SortDialog;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductModelListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener {

    public static final String TAG = ProductModelListFragment.class.getSimpleName();
    static List<ProductModel> productModelList = new ArrayList<>();
    @BindView(R.id.list)
    SuperRecyclerView list;
    @BindView(R.id.fab)
    Button fab;
    private LinearLayoutManager layoutManager;
    private ProductModelListAdapter adapter;
    private SparseItemRemoveAnimator mSparseAnimator;
    private MenuItem searchMenuItem;
    private MenuItem sortMenuItem;
    private SearchView searchView;
    private ArrayList<ProductModel> selected_modelList;

    private StringBuilder builder;
    private android.view.ActionMode mActionMode;

    private String deleteMultipmodel;
    private MenuItem filterMenuItem;
    private String substring;
    private List<ProductModel> productModelListForSearch=new ArrayList<>();

    public ProductModelListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.product, menu);
        setupOptionMenu(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private boolean setupOptionMenu(Menu menu) {
        searchMenuItem = menu.findItem(R.id.searchItem);
        sortMenuItem = menu.findItem(R.id.action_sort);
        filterMenuItem = menu.findItem(R.id.action_filter);
        filterMenuItem.setVisible(true);

        if (searchMenuItem != null) {
            searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
            if (null == searchView) {
                return true;
            }
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String query) {

                    submitQuerry(query);
                    return true;
                }
            });
            searchView.setQueryHint(getString(R.string.action_search_model));
            //applying the font to hint text inside of SearchView
            SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
            searchText.setHintTextColor(getContext().getResources().getColor(R.color.colorAccent));
            searchText.setTextColor(getContext().getResources().getColor(R.color.white));
            searchText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        return false;
    }

    private void submitQuerry(String query) {
//        remoteSearch(query);
        localSearch(query);
    }

    private void localSearch(String query) {
        Timber.e(query);
        ArrayList<ProductModel> searchList=new ArrayList<>();
        for (ProductModel productModel : productModelListForSearch) {
            if (productModel.getName().toLowerCase().contains(query)) {
                searchList.add(productModel);
            }
        }

        if(searchList.size()>0){//if found something related to search querry
            productModelList.clear();
            productModelList.addAll(searchList);
            adapter.notifyDataChanged();
        }
        Timber.e("querry- "+query+", search result size-"+searchList.size());
    }

    private void remoteSearch(String query) {
        DataManager.getInstance().getAllProductModel("", query, "", "", "", new Callback<AllProductModels>() {
            @Override
            public void onResponse(Call<AllProductModels> call, Response<AllProductModels> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllProductModels.class);
                Timber.i("downloadProductModels", responseJson);

                if (!response.body().isError()) {
                    productModelList.clear();
                    productModelList.addAll(response.body().getModels());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllProductModels> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadProductModels", t.getMessage());
                t.printStackTrace();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                SortDialogForCategory sortDialog = SortDialogForCategory.getInstance(new SortDialogForCategory.OnCategorySortListener() {
                    @Override
                    public void onCategorySort(Dialog dialog, String sortingType) {
                        dialog.dismiss();

                        sortListAccTo(sortingType);
                    }
                });
                sortDialog.show(getActivity().getSupportFragmentManager(), SortDialog.TAG);
                break;


            case R.id.action_filter:
                FilterDialogForBrands filterDialogForBrands = FilterDialogForBrands.getInstance(new FilterDialogForBrands.OnshortFilterListener() {
                    @Override
                    public void onBrandsSort(Dialog dialog, ArrayList<String> sortingType) {
                        dialog.dismiss();
                        filterListAccTo(sortingType);
                    }
                });
                filterDialogForBrands.show(getActivity().getSupportFragmentManager(), FilterDialogForBrands.TAG);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortListAccTo(String sortingType) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllProductModel("", "", "", sortingType, "", new Callback<AllProductModels>() {
            @Override
            public void onResponse(Call<AllProductModels> call, Response<AllProductModels> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllProductModels.class);
                Timber.i("downloadProductModels", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    productModelList.clear();
                    productModelList.addAll(response.body().getModels());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllProductModels> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadProductModels", t.getMessage());
                t.printStackTrace();
            }
        });

    }


    private void filterListAccTo(ArrayList<String> filterType) {

        builder=new StringBuilder();
        for (String filter :
                filterType) {
            Log.e("this","filter ids are:- "+filter);
            builder.append(filter).append(",");
        }

        substring = builder.toString().substring(0, builder.length() - 1);

        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllFilterModel(substring, new Callback<AllProductModels>() {
            @Override
            public void onResponse(Call<AllProductModels> call, Response<AllProductModels> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllProductModels.class);
                Timber.i("downloadSubCategory", responseJson);
                if (!response.body().isError()) {
                    productModelList.clear();
                    productModelList.addAll(response.body().getModels());
                    adapter.notifyDataSetChanged();

                    productModelListForSearch.clear();
                    productModelListForSearch.addAll(productModelList);
                }
                else {
                    productModelList.clear();
                    adapter.notifyDataSetChanged();

                    productModelListForSearch.clear();
                    productModelListForSearch.addAll(productModelList);

                }
            }

            @Override
            public void onFailure(Call<AllProductModels> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadSubCategory", t.getMessage());
                t.printStackTrace();
            }
        });

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);
        fab.setText("Add Model");
        layoutManager = new LinearLayoutManager(getActivity());
        adapter= new ProductModelListAdapter(productModelList, new OnProductModelDeleteListener() {
            @Override
            public void onProductModelDelete(ArrayList<ProductModel> brand, int adapterPosition) {
               // removeProductModel(mItem, adapterPosition);
                selected_modelList = brand;
                if (selected_modelList.size()>0) {
                    if (mActionMode == null)
                    {
                        mActionMode = getActivity().startActionMode(mActionModeCallback);
                    }
                }
                else {
                    if (mActionMode != null) {
                        mActionMode.finish();
                        selected_modelList.clear();
                    }
                }

            }
        }, new OnProductModelStatusChangeListener() {
            @Override
            public void onProductModelStatusChange(ProductModel mItem, int adapterPosition, String status) {
                changeBrandStatus(mItem, adapterPosition, status);
            }
        }, new OnProductModelUpdateListener() {
            @Override
            public void onProductModelUpdate(ProductModel mItem, int adapterPosition) {
                Intent toUpdateEmp= new Intent(getActivity(),AddProductModelActivity.class);
                toUpdateEmp.putExtra(AppConstants.EXTRA_UPDATE_ITEM,mItem);
                toUpdateEmp.putExtra(AppConstants.EXTRA_ADAPTER_POSITION,adapterPosition);
                startActivityForResult(toUpdateEmp,AppConstants.REQUEST_CODE_TO_UPADATE);
            }
        });
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        list.setRefreshListener(this);
        list.setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);
//        list.setupMoreListener(this, 1);
        return view;
    }
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            mActionMode.getMenuInflater().inflate(R.menu.context, menu);
            return true;

        }

        // Called each time the action mode is shown.
        // Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:

                    AlertDialog.Builder dilaogbuilder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(),R.style.AlertDialogStyle);
                    } else {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
                    }
                    dilaogbuilder.setTitle("Delete Model")
                            .setCancelable(true)
                            .setMessage("Are you sure you want to delete this Model?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    builder=new StringBuilder();
                                    for (ProductModel model :
                                            selected_modelList) {
                                        builder.append(model.getId()).append(",");
                                    }
                                    deleteMultipmodel = builder.toString().substring(0, builder.length() - 1);

                                    deleteModel(deleteMultipmodel);
                                    Log.e("this","Ids are :"+builder.toString().substring(0, builder.length() - 1));
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();


                    return true;

                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    private void deleteModel(final String deleteMultipmodel) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().deleteMultipleModel(deleteMultipmodel, new Callback<DeleteMultipleModel>() {
            @Override
            public void onResponse(Call<DeleteMultipleModel> call, Response<DeleteMultipleModel> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), DeleteMultipleModel.class);
                Timber.i("removeCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    mActionMode.finish();
                    productModelList.clear();
                    selected_modelList.clear();
                    downloadProductModels();
                }
            }

            @Override
            public void onFailure(Call<DeleteMultipleModel> call, Throwable t) {
                Timber.e("removeProduct", t.getMessage());
                t.printStackTrace();
            }
        });

    }

    private void changeBrandStatus(ProductModel mItem, final int adapterPosition, String status) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().productModelManage(mItem.getName(), mItem.getDescription(), status, mItem.getBrand().getId(), mItem.getAdmin_id(), AppConstants.ACTION_TYPE_UPDATE, mItem.getId(), new Callback<ProductModelManage>() {
            @Override
            public void onResponse(Call<ProductModelManage> call, Response<ProductModelManage> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), ProductModelManage.class);
                Timber.i("changeBrandStatus",responseJson);
                Toast.makeText(getActivity(), ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if(!(response.body().isError())){
                    adapter.updateStatus(adapterPosition,response.body().getModel().getFlag());
                }
            }

            @Override
            public void onFailure(Call<ProductModelManage> call, Throwable t) {
                Util.hideProgressDialog();
                t.printStackTrace();
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getActivity().getResources().getString(R.string.title_product_model_list_fragment));
        downloadProductModels();


    }

    private void downloadProductModels() {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllProductModel("", "", "", "", "", new Callback<AllProductModels>() {
            @Override
            public void onResponse(Call<AllProductModels> call, Response<AllProductModels> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllProductModels.class);
                Timber.i("downloadProductModels", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    productModelList.clear();
                    productModelList.addAll(response.body().getModels());
                    adapter.notifyDataChanged();

                    productModelListForSearch.clear();
                    productModelListForSearch.addAll(productModelList);
                }
            }

            @Override
            public void onFailure(Call<AllProductModels> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadProductModels", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
//        Toast.makeText(getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
        downloadProductModels();
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        Toast.makeText(getActivity(), "More", Toast.LENGTH_SHORT).show();
        User user = new User();
        user.setFirst_name("More asked, more served");
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivityForResult(new Intent(getActivity(), AddProductModelActivity.class), AppConstants.REQUEST_CODE_TO_ADD);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK) {
            switch (requestCode){
                case AppConstants.REQUEST_CODE_TO_ADD:
                    productModelList.add((ProductModel) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM));
                    adapter.notifyDataSetChanged();
                    break;
                case AppConstants.REQUEST_CODE_TO_UPADATE:
                    productModelList.set(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION,0),
                            ((ProductModel) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM)));
                    adapter.notifyItemChanged(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION,0));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    interface OnProductModelDeleteListener {
        void onProductModelDelete(ArrayList<ProductModel> mItem, int adapterPosition);
    }

    interface OnProductModelStatusChangeListener {
        void onProductModelStatusChange(ProductModel mItem, int adapterPosition, String status);
    }

    interface OnProductModelUpdateListener {
        void onProductModelUpdate(ProductModel mItem, int adapterPosition);
    }
}
