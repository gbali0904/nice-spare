package com.example.pallaw.electromanage.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.UserModule.UserActivity;
import com.example.pallaw.electromanage.main.MainActivity;
import com.google.gson.Gson;

/**
 * Created by pallaw on 04/10/17.
 */

public class UserManager {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "UserMangerPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    //user json
    private static final String JSON_USER = "json_user";



    private static  UserManager ourInstance;

    private UserManager() {
    }

    // Constructor
    public UserManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static UserManager startSession(Context context){
        ourInstance=new UserManager(context);
        return ourInstance;
    }

    public static UserManager getInstance(){
        return ourInstance;
    }


    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    /**
     * Clear session age
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        Authentication.setInstance("");

//        // After logout redirect user to Loing Activity
//        Intent i = new Intent(_context, UserActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        // Staring Login Activity
//        _context.startActivity(i);


    }

    /**
     * Create login session
     * */
    public void createLoginSession(Authentication authResponse) {
        Authentication.setInstance(authResponse);
        String userJson = new Gson().toJson(Authentication.getInstance());
        editor.putString(JSON_USER, userJson).commit();


        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true).commit();
        Intent intent = new Intent(_context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(intent);
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        Intent i;
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity



            i = new Intent(_context, UserActivity.class);
            // Closing all the Activities

        }else{
            Authentication.setInstance(pref.getString(JSON_USER,"{}"));
            i = new Intent(_context, MainActivity.class);
        }

        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Staring Login Activity
        _context.startActivity(i);

    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void forgotPassword(){
        Toast.makeText(_context,"New password sent to your email age",Toast.LENGTH_LONG).show();
    }


}