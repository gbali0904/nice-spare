package com.example.pallaw.electromanage.REST.Model;

/**
 * Created by Android on 11/6/2017.
 */

public class DeleteMultipleUser {

    /**
     * error : false
     * code : 0
     * msg : Delete User successful
     */

    private boolean error;
    private int code;
    private String msg;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
