package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.Brand;

/**
 * Created by pallaw on 10/10/17.
 */

public class BrandManage {


    /**
     * error : false
     * msg : Product Added successful
     * brand : {"id":"6","name":"Apple","Description":"here is apple","flag":"0","admin_id":"1000","time_stamp":"2017-10-10 16:27:30.464393"}
     */

    private boolean error;
    private String msg;
    private Brand brand;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

}
