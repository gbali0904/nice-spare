/*
 * Copyright (c) Joaquim Ley 2016. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.pallaw.electromanage.REST;


import com.example.pallaw.electromanage.REST.Model.AllBrands;
import com.example.pallaw.electromanage.REST.Model.AllCategories;
import com.example.pallaw.electromanage.REST.Model.AllEmployees;
import com.example.pallaw.electromanage.REST.Model.AllFilterProduct;
import com.example.pallaw.electromanage.REST.Model.AllProduct;
import com.example.pallaw.electromanage.REST.Model.AllProductModels;
import com.example.pallaw.electromanage.REST.Model.AllSubCategory;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.BrandManage;
import com.example.pallaw.electromanage.REST.Model.CategoryOperation;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleBrand;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleCategory;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleModel;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleProduct;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleSubcategory;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleUser;
import com.example.pallaw.electromanage.REST.Model.ModelForFilterList;
import com.example.pallaw.electromanage.REST.Model.ModelForResetPassword;
import com.example.pallaw.electromanage.REST.Model.ProductModelManage;
import com.example.pallaw.electromanage.REST.Model.ProductOperation;
import com.example.pallaw.electromanage.REST.Model.SearchResult;
import com.example.pallaw.electromanage.REST.Model.SubCategoryOperation;
import com.example.pallaw.electromanage.REST.Model.resources.UploadFile;
import com.example.pallaw.electromanage.REST.network.ServiceFactory;
import com.example.pallaw.electromanage.REST.network.Services;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;

/**
 * Api abstraction
 */
public class DataManager {

    private static DataManager sInstance;

    private final Services service;

    private DataManager() {
        service = ServiceFactory.getServices();
    }

    public static DataManager getInstance() {
        if (sInstance == null) {
            sInstance = new DataManager();
        }
        return sInstance;
    }

    public void login(String email, String password, Callback<Authentication> listener) {
        service.login(email, password)
                .enqueue(listener);
    }


    public void getAllUsers(String adminId, String name, String mobile, String short_name,String flag, Callback<AllEmployees> listener) {
        service.getallUsers(adminId, name, mobile,short_name,flag)
                .enqueue(listener);
    }

    public void userManage(String name, String m_name, String l_name, String phone, String email, String action_type, String user_id, String flag, Callback<UserManage> listener) {
        String admin_id = Authentication.getInstance().getData().getId();
        service.userManage(name,m_name,l_name,phone,email,action_type,user_id, admin_id,flag)
                .enqueue(listener);
    }

    public void getAllCategories(String admin_id, String name, String model_id, String brand_id, String flag, String short_name, Callback<AllCategories> listener){
        service.getAllCategories(admin_id,name,model_id,brand_id,flag,short_name)
                .enqueue(listener);
    }

    public void categoryOperations(String category_name, String category_description, String active_status, String admin_id, String action_type, String category_id, Callback<CategoryOperation> listener) {
        service.categoryOperation(category_name,category_description,active_status,admin_id,action_type,category_id)
                .enqueue(listener);
    }
    public void categoryUpdateOperations( String category, Callback<CategoryOperation> listener) {
        service.categoryUpdateOperations(category)
                .enqueue(listener);
    }

    public void getAllSubCategory(String admin_id, String name, String flag, String short_name, String category_id, Callback<AllSubCategory> listener) {
        service.getAllSubCategory(admin_id,name,flag,short_name,category_id)
                .enqueue(listener);
    }
    public void getAllFilterCategory(String category_id, Callback<AllSubCategory> listener) {
        service.getAllFilterCategory(category_id)
                .enqueue(listener);
    }
    public void getAllFilterModel(String model_id, Callback<AllProductModels> listener) {
        service.getAllFilterModel(model_id)
                .enqueue(listener);
    }

    public void subcategoryOperation(String name, String description, String flag, String admin_id, String category_id, String action_type, String subcategory_id, Callback<SubCategoryOperation> listener) {
        service.subCategoryOperation(name,description,flag,admin_id,category_id,action_type,subcategory_id)
                .enqueue(listener);
    }

    public void getAllBrands(String admin_id, String name, String flag, String short_name, Callback<AllBrands> listener) {
        service.getAllBrands(admin_id,name,flag,short_name)
                .enqueue(listener);
    }

    public void brandManage(String name, String description, String flag, String adming_id, String action_type, String brand_id, Callback<BrandManage> listener) {
        service.brandManage(name,description,flag,adming_id,action_type,brand_id)
                .enqueue(listener);
    }

    public void getAllProductModel(String admin_id, String name, String flag, String short_name, String brands_id, Callback<AllProductModels> listener) {
        service.getAllProductModel(admin_id, name,flag,short_name,brands_id)
                .enqueue(listener);
    }

    public void productModelManage(String name, String description, String flag, String brand_id, String adming_id, String action_type, String model_id, Callback<ProductModelManage> listener) {
        service.productModelManage(name,description,flag,adming_id,action_type,model_id,brand_id)
                .enqueue(listener);
    }

    public void getAllProduct(String product_name, String category_id, String subcategory_id, String brand_id, String model_id, String stock_status, String price, String filter, String flag, Callback<AllProduct> listener) {
        service.getAllProduct(product_name,category_id,subcategory_id,brand_id,model_id,stock_status,price,filter,flag)
                .enqueue(listener);
    }

    public void productOperation(String product_name, String description, String flag, String adminId, String category_id, String subcategory_id, String action_type, String product_id, String brand_id, String model_id, String stock_status, String price, String unit_flag, Callback<ProductOperation> listener) {
        service.productOperation(product_name,description,flag,adminId,category_id,subcategory_id,action_type,product_id,brand_id,model_id,stock_status,price,unit_flag)
                .enqueue(listener);
    }

    public void sortProduct(String sortingType, String flag, Callback<AllProduct> listener){
        service.getAllProduct("","","","","","","",sortingType,flag)
                .enqueue(listener);
    }

    public void searchProduct(String querry, String flag, Callback<SearchResult> listener){
        service.searchProduct(querry,flag)
                .enqueue(listener);
    }


    public void uploadcsvFile(HashMap<String, RequestBody> multipartRequest, MultipartBody.Part importfile, Callback<UploadFile> listener) {
        service.uploadcsvFile(multipartRequest,importfile)
                .enqueue(listener);
    }

    public void branduploadcsvFile(HashMap<String, RequestBody> multipartRequest, MultipartBody.Part importfile, Callback<UploadFile> listener) {
        service.branduploadcsvFile(multipartRequest,importfile)
                .enqueue(listener);
    }
    public void modelUploadcsvFile(HashMap<String, RequestBody> multipartRequest, MultipartBody.Part importfile, Callback<UploadFile> listener) {
        service.modelUploadcsvFile(multipartRequest,importfile)
                .enqueue(listener);
    }
    public void categoryUploadcsvFile(HashMap<String, RequestBody> multipartRequest, MultipartBody.Part importfile, Callback<UploadFile> listener) {
        service.categoryUploadcsvFile(multipartRequest,importfile)
                .enqueue(listener);
    }
    public void subcategoryUploadcsvFile(HashMap<String, RequestBody> multipartRequest, MultipartBody.Part importfile, Callback<UploadFile> listener) {
        service.subcategoryUploadcsvFile(multipartRequest,importfile)
                .enqueue(listener);
    }


    public void deleteMultipleproduct(String product_id, Callback<DeleteMultipleProduct> listener){
        service.deleteMultipleproduct(product_id)
                .enqueue(listener);
    }

    public void deleteMultipleUsers(String user_id, Callback<DeleteMultipleUser> listener) {
        service.deleteMultipleUsers(user_id)
                .enqueue(listener);
    }

    public void deleteMultipleCategory(String category_id, Callback<DeleteMultipleCategory> listener) {
        service.deleteMultipleCategory(category_id)
                .enqueue(listener);
    }
    public void deleteMultipleSubCategory(String subcategory_id, Callback<DeleteMultipleSubcategory> listener) {
        service.deleteMultipleSubCategory(subcategory_id)
                .enqueue(listener);
    }

    public void deleteMultipleBrand(String brand_id, Callback<DeleteMultipleBrand> listener) {
        service.deleteMultipleBrand(brand_id)
                .enqueue(listener);
    }

    public void deleteMultipleModel(String model_id, Callback<DeleteMultipleModel> listener) {
        service.deleteMultipleModel(model_id)
                .enqueue(listener);
    }



    public void getFilterList( Callback<ModelForFilterList> listener) {
        service.getFilterList()
                .enqueue(listener);
    }

    public void getFilterAllProduct(String filter_data, String flag, Callback<AllFilterProduct> listener) {
        service.getAllFilterProduct(filter_data,flag)
                .enqueue(listener);
    }
    public void changePassword(String pass ,String id, Callback<Authentication> listener) {
        service.changePassword(pass,id)
                .enqueue(listener);
    }

    public void resetPassword(String id, Callback<ModelForResetPassword> callback) {
        service.resetPassword(id)
                .enqueue(callback);
    }
}