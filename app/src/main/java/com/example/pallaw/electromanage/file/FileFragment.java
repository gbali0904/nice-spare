package com.example.pallaw.electromanage.file;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pallaw.electromanage.BuildConfig;
import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.resources.UploadFile;
import com.example.pallaw.electromanage.Utils.RetrofitUtils;
import com.example.pallaw.electromanage.Utils.Util;
import com.example.pallaw.electromanage.file.adapter.CsvUpdateListAdapter;
import com.google.gson.Gson;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.nononsenseapps.filepicker.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Android on 11/2/2017.
 */

public class FileFragment extends Fragment {
    public static final String TAG = FileFragment.class.getSimpleName();
    private static final int SELECT_FILE = 1;
    private static final MediaType MEDIA_TYPE_DOC = MediaType.parse("application/doc");
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 100;

    @BindView(R.id.btnforchoosefile)
    Button btnforchoosefile;
    @BindView(R.id.btnForUpload)
    Button btnForUpload;
    @BindView(R.id.btnFordownload)
    Button btnFordownload;
    @BindView(R.id.fileName)
    TextView fileName;
    @BindView(R.id.rvUser)
    RecyclerView rvUser;
    @BindView(R.id.lay)
    LinearLayout lay;
    private MultipartBody.Part importfile;
    private String file_Name = "";
    private LinearLayoutManager layoutManager;
    private List<UploadFile.DataSuccessBean> dataSuccess;
    private List<UploadFile.DataSuccessBean> dataError;
    private List<UploadFile.DataSuccessBean> adapterList = new ArrayList<>();
    private AlertDialog alert11;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_file, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.btnforchoosefile, R.id.btnForUpload, R.id.btnFordownload})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnforchoosefile:
                // This always works
                Intent i = new Intent(getActivity(), FilePickerActivity.class);
                // Set these depending on your use case. These are the defaults.
                i.putExtra(FilePickerActivity.EXTRA_SINGLE_CLICK, false);
                i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
                i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());
                startActivityForResult(i, SELECT_FILE);
                break;
            case R.id.btnForUpload:
                if (!file_Name.equals("")) {
                    uploadCsvFile();
                } else {
                    Toast.makeText(getActivity(), "Please Choose File", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnFordownload:
                if (!checkPermissions()) {
                    requestPermissions();
                }
                else {
                    FileTypeChooserDialog fileTypeChooserDialog = new FileTypeChooserDialog();
                    fileTypeChooserDialog.show(getActivity().getSupportFragmentManager(), "fileTypeChooserDialog");
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_FILE && resultCode == Activity.RESULT_OK) {
            // Use the provided utility method to parse the result
            List<Uri> files = Utils.getSelectedFilesFromResult(data);
            for (Uri uri : files) {
                File file = Utils.getFileForUri(uri);
                // Do something with the result...
                RequestBody requestFile = RequestBody.create(MEDIA_TYPE_DOC, file);
                importfile = MultipartBody.Part.createFormData("uploadFile", file.getName(), requestFile);
                file_Name = file.getName();
                fileName.setText(file_Name);

            }
        }
    }

    private void uploadCsvFile() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View alertDialog = inflater.inflate(R.layout.fragment_file_type_upload_dialog, null);
        dialogBuilder.setView(alertDialog);
        alert11 = dialogBuilder.create();
        RadioButton rbFTCDProducts = (RadioButton) alertDialog.findViewById(R.id.rbFTCDProducts);
        RadioButton rbFTCDBrands = (RadioButton) alertDialog.findViewById(R.id.rbFTCDBrands);
        RadioButton rbFTCDModel = (RadioButton) alertDialog.findViewById(R.id.rbFTCDModel);
        RadioButton rbFTCDCategory = (RadioButton) alertDialog.findViewById(R.id.rbFTCDCategory);
        RadioButton rbFTCDSubCategory = (RadioButton) alertDialog.findViewById(R.id.rbFTCDSubCategory);
        RadioGroup radioGroup = (RadioGroup) alertDialog.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
              switch (checkedId){
                  case R.id.rbFTCDProducts:
                      productUpdateCSV();
                      break;
                  case R.id.rbFTCDBrands:
                      brandUpdateCSV();
                      break;
                  case R.id.rbFTCDModel:
                      modelUpdateCSV();
                      break;
                  case R.id.rbFTCDCategory:
                      categoryUpdateCSV();
                      break;
                  case R.id.rbFTCDSubCategory:
                      subCategoryUpdateCSV();
                      break;
              }
            }
        });
        alert11.show();

    }

    private void subCategoryUpdateCSV() {
        Util.showProgressDialog(getActivity());
        HashMap<String, String> requestValueAddPairsMap = new HashMap<>();
        requestValueAddPairsMap.put("mode", "import");
        HashMap<String, RequestBody> multipartRequest = RetrofitUtils.createMultipartRequest(requestValueAddPairsMap);
        DataManager.getInstance().subcategoryUploadcsvFile(multipartRequest, importfile, new Callback<UploadFile>() {
            @Override
            public void onResponse(Call<UploadFile> call, Response<UploadFile> response) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setText("");
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    layoutManager = new LinearLayoutManager(getActivity());
                    dataSuccess = response.body().getDataSuccess();
                    dataError = response.body().getDataError();
                    if (dataSuccess.size() > 0) {
                        adapterList.addAll(dataSuccess);
                    }
                    if (dataError.size() > 0) {
                        adapterList.addAll(dataError);
                    }

                    CsvUpdateListAdapter adapter = new CsvUpdateListAdapter(getActivity(), adapterList);
                    rvUser.setLayoutManager(layoutManager);
                    rvUser.setAdapter(adapter);

                    String responseJson = new Gson().toJson(response.body(), UploadFile.class);
                    Timber.i("FileUploadResponse", responseJson);
                    Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("this", response.toString());
                }
                else {
                    Toast.makeText(getActivity(),response.message(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UploadFile> call, Throwable t) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.GONE);
                fileName.setText("");
                Timber.e("FileUploadError", t.getMessage() + "here is erroor");
                t.printStackTrace();
            }
        });
    }

    private void categoryUpdateCSV() {
        Util.showProgressDialog(getActivity());
        HashMap<String, String> requestValueAddPairsMap = new HashMap<>();
        requestValueAddPairsMap.put("mode", "import");
        HashMap<String, RequestBody> multipartRequest = RetrofitUtils.createMultipartRequest(requestValueAddPairsMap);
        DataManager.getInstance().categoryUploadcsvFile(multipartRequest, importfile, new Callback<UploadFile>() {
            @Override
            public void onResponse(Call<UploadFile> call, Response<UploadFile> response) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setText("");
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    layoutManager = new LinearLayoutManager(getActivity());
                    dataSuccess = response.body().getDataSuccess();
                    dataError = response.body().getDataError();
                    if (dataSuccess.size() > 0) {
                        adapterList.addAll(dataSuccess);
                    }
                    if (dataError.size() > 0) {
                        adapterList.addAll(dataError);
                    }

                    CsvUpdateListAdapter adapter = new CsvUpdateListAdapter(getActivity(), adapterList);
                    rvUser.setLayoutManager(layoutManager);
                    rvUser.setAdapter(adapter);

                    String responseJson = new Gson().toJson(response.body(), UploadFile.class);
                    Timber.i("FileUploadResponse", responseJson);
                    Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("this", response.toString());
                }
                else {
                    Toast.makeText(getActivity(),response.message(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UploadFile> call, Throwable t) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.GONE);
                fileName.setText("");
                Timber.e("FileUploadError", t.getMessage() + "here is erroor");
                t.printStackTrace();
            }
        });
    }

    private void modelUpdateCSV() {
        Util.showProgressDialog(getActivity());
        HashMap<String, String> requestValueAddPairsMap = new HashMap<>();
        requestValueAddPairsMap.put("mode", "import");
        HashMap<String, RequestBody> multipartRequest = RetrofitUtils.createMultipartRequest(requestValueAddPairsMap);
        DataManager.getInstance().modelUploadcsvFile(multipartRequest, importfile, new Callback<UploadFile>() {
            @Override
            public void onResponse(Call<UploadFile> call, Response<UploadFile> response) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setText("");
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    layoutManager = new LinearLayoutManager(getActivity());
                    dataSuccess = response.body().getDataSuccess();
                    dataError = response.body().getDataError();
                    if (dataSuccess.size() > 0) {
                        adapterList.addAll(dataSuccess);
                    }
                    if (dataError.size() > 0) {
                        adapterList.addAll(dataError);
                    }

                    CsvUpdateListAdapter adapter = new CsvUpdateListAdapter(getActivity(), adapterList);
                    rvUser.setLayoutManager(layoutManager);
                    rvUser.setAdapter(adapter);

                    String responseJson = new Gson().toJson(response.body(), UploadFile.class);
                    Timber.i("FileUploadResponse", responseJson);
                    Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("this", response.toString());
                }
                else {
                    Toast.makeText(getActivity(),response.message(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UploadFile> call, Throwable t) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.GONE);
                fileName.setText("");
                Timber.e("FileUploadError", t.getMessage() + "here is erroor");
                t.printStackTrace();
            }
        });
    }

    private void brandUpdateCSV() {
        Util.showProgressDialog(getActivity());
        HashMap<String, String> requestValueAddPairsMap = new HashMap<>();
        requestValueAddPairsMap.put("mode", "import");
        HashMap<String, RequestBody> multipartRequest = RetrofitUtils.createMultipartRequest(requestValueAddPairsMap);
        DataManager.getInstance().branduploadcsvFile(multipartRequest, importfile, new Callback<UploadFile>() {
            @Override
            public void onResponse(Call<UploadFile> call, Response<UploadFile> response) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setText("");
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    layoutManager = new LinearLayoutManager(getActivity());
                    dataSuccess = response.body().getDataSuccess();
                    dataError = response.body().getDataError();
                    if (dataSuccess.size() > 0) {
                        adapterList.addAll(dataSuccess);
                    }
                    if (dataError.size() > 0) {
                        adapterList.addAll(dataError);
                    }

                    CsvUpdateListAdapter adapter = new CsvUpdateListAdapter(getActivity(), adapterList);
                    rvUser.setLayoutManager(layoutManager);
                    rvUser.setAdapter(adapter);

                    String responseJson = new Gson().toJson(response.body(), UploadFile.class);
                    Timber.i("FileUploadResponse", responseJson);
                    Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("this", response.toString());
                }
                else {
                    Toast.makeText(getActivity(),response.message(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UploadFile> call, Throwable t) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.GONE);
                fileName.setText("");
                Timber.e("FileUploadError", t.getMessage() + "here is erroor");
                t.printStackTrace();
            }
        });
    }

    private void productUpdateCSV() {
        Util.showProgressDialog(getActivity());
        HashMap<String, String> requestValueAddPairsMap = new HashMap<>();
        requestValueAddPairsMap.put("mode", "import");
        HashMap<String, RequestBody> multipartRequest = RetrofitUtils.createMultipartRequest(requestValueAddPairsMap);
        DataManager.getInstance().uploadcsvFile(multipartRequest, importfile, new Callback<UploadFile>() {
            @Override
            public void onResponse(Call<UploadFile> call, Response<UploadFile> response) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setText("");
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    layoutManager = new LinearLayoutManager(getActivity());
                    dataSuccess = response.body().getDataSuccess();
                    dataError = response.body().getDataError();
                    if (dataSuccess.size() > 0) {
                        adapterList.addAll(dataSuccess);
                    }
                    if (dataError.size() > 0) {
                        adapterList.addAll(dataError);
                    }

                    CsvUpdateListAdapter adapter = new CsvUpdateListAdapter(getActivity(), adapterList);
                    rvUser.setLayoutManager(layoutManager);
                    rvUser.setAdapter(adapter);

                    String responseJson = new Gson().toJson(response.body(), UploadFile.class);
                    Timber.i("FileUploadResponse", responseJson);
                    Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("this", response.toString());
                }
                else {
                    Toast.makeText(getActivity(),response.message(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UploadFile> call, Throwable t) {
                Util.hideProgressDialog();
                alert11.dismiss();
                file_Name = "";
                fileName.setVisibility(View.GONE);
                lay.setVisibility(View.GONE);
                fileName.setText("");
                Timber.e("FileUploadError", t.getMessage() + "here is erroor");
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getActivity().getResources().getString(R.string.title_file_fragment));
    }
    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            startLocationPermissionRequest();

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }
    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }
    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
            } else {
                // Permission denied.
                Intent intent = new Intent();
                intent.setAction(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package",
                        BuildConfig.APPLICATION_ID, null);
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }


}
