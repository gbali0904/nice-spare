package com.example.pallaw.electromanage.file;


import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.network.ServiceFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class FileTypeChooserDialog extends DialogFragment {


    @BindView(R.id.rbFTCDBrands)
    RadioButton rbFTCDBrands;
    @BindView(R.id.rbFTCDModel)
    RadioButton rbFTCDModel;
    @BindView(R.id.rbFTCDCategory)
    RadioButton rbFTCDCategory;
    @BindView(R.id.rbFTCDSubCategory)
    RadioButton rbFTCDSubCategory;
    @BindView(R.id.rbFTCDProducts)
    RadioButton rbFTCDProducts;
    @BindView(R.id.download)
    Button download;
    @BindView(R.id.rbFTCDAll)
    RadioButton rbFTCDAll;
    private String urlname;
    private String downloadurl;

    public FileTypeChooserDialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_file_type_chooser_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() == null)
            return;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void downloadCsvFile(String url, String name) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("CSV File Downloading");
        request.setTitle(name);
    // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, name);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
        dismiss();
    }

    @OnClick({R.id.rbFTCDProducts, R.id.rbFTCDBrands, R.id.rbFTCDModel, R.id.rbFTCDCategory, R.id.rbFTCDSubCategory,R.id.rbFTCDAll, R.id.download})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rbFTCDProducts:

                downloadurl = ServiceFactory.BASE_URL+"downloadCsvFile.php";
                urlname = "productFile.csv";
                break;
            case R.id.rbFTCDBrands:

                downloadurl = ServiceFactory.BASE_URL+"downloadBrand.php";
                urlname = "brandsFile.csv";

                break;
            case R.id.rbFTCDModel:

                downloadurl = ServiceFactory.BASE_URL+"downloadModels.php";
                urlname = "modelFile.csv";
                break;
            case R.id.rbFTCDCategory:


                downloadurl = ServiceFactory.BASE_URL+"downloadCategory.php";
                urlname = "CategorysFile.csv";

                break;
            case R.id.rbFTCDSubCategory:
                downloadurl =ServiceFactory.BASE_URL+"downloadSubCategory.php";
                urlname = "SubCategorysFile.csv";
                break;
            case R.id.rbFTCDAll:
                downloadurl =ServiceFactory.BASE_URL+"getAllCSVFile.php";
                urlname = "AllFile.csv";
                break;
            case R.id.download:
                downloadCsvFile(downloadurl, urlname);
                break;
        }
    }
}
