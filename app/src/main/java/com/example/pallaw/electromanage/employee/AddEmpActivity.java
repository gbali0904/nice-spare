package com.example.pallaw.electromanage.employee;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.resources.User;
import com.example.pallaw.electromanage.REST.UserManage;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AddEmpActivity extends AppCompatActivity {

    @BindView(R.id.edtAddEmpFName)
    EditText edtAddEmpFName;
    @BindView(R.id.edtAddEmpMName)
    EditText edtAddEmpMName;
    @BindView(R.id.edtAddEmpLName)
    EditText edtAddEmpLName;
    @BindView(R.id.edtAddEmpMobile)
    EditText edtAddEmpMobile;
    @BindView(R.id.edtAddEmpEmail)
    EditText edtAddEmpEmail;
    @BindView(R.id.btnAddEmp)
    Button btnAddEmp;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnUpdateEmp)
    Button btnUpdateEmp;
    private String fName;
    private String mName;
    private String lName;
    private String mobile;
    private String email;
    private User user;
    private int update_position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_emp);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (getIntent().getExtras()!=null) {
            user = getIntent().getExtras().getParcelable(AppConstants.EXTRA_UPDATE_ITEM);
            update_position = getIntent().getExtras().getInt(AppConstants.EXTRA_ADAPTER_POSITION);
            if (user != null) {
                setTitle(getResources().getString(R.string.title_update_employee));
                updateFields();
                return;
            }
        }

        setTitle(getResources().getString(R.string.title_add_employee));


    }

    private void updateFields() {
        btnUpdateEmp.setVisibility(View.VISIBLE);
        btnAddEmp.setVisibility(View.GONE);

        edtAddEmpFName.setText(String.format("%s", user.getFirst_name()));
        edtAddEmpMName.setText(String.format("%s", user.getMiddel_name()));
        edtAddEmpLName.setText(String.format("%s", user.getLast_name()));
        edtAddEmpMobile.setText(String.format("%s", user.getMobile_no()));
        edtAddEmpEmail.setText(String.format("%s", user.getEmail_id()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(fName)) {
            isValid = false;
            edtAddEmpFName.setError(getResources().getString(R.string.error_fname));
        }

//        if (TextUtils.isEmpty(mName)) {
//            isValid = false;
//            edtAddEmpMName.setError(getResources().getString(R.string.error_mname));
//        }
//
//        if (TextUtils.isEmpty(lName)) {
//            isValid = false;
//            edtAddEmpLName.setError(getResources().getString(R.string.error_lname));
//        }

//        if (TextUtils.isEmpty(email) || !(Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
//            isValid = false;
//            edtAddEmpEmail.setError(getResources().getString(R.string.error_email));
//        }

        if (TextUtils.isEmpty(mobile) || mobile.length() != 10) {
            isValid = false;
            edtAddEmpMobile.setError(getResources().getString(R.string.error_mobile));
        }

        return isValid;
    }

    @OnClick({R.id.btnAddEmp, R.id.btnUpdateEmp})
    public void onViewClicked(View view) {
        //get all values before Add or update values
        fName = edtAddEmpFName.getText().toString().trim();
        mName = edtAddEmpMName.getText().toString().trim();
        lName = edtAddEmpLName.getText().toString().trim();
        mobile = edtAddEmpMobile.getText().toString().trim();
        email = edtAddEmpEmail.getText().toString().trim();

        switch (view.getId()) {
            case R.id.btnAddEmp:
                if (validation()) {
                    Util.showProgressDialog(AddEmpActivity.this);
                    DataManager.getInstance().userManage(fName, mName, lName, mobile, email, AppConstants.ACTION_TYPE_ADD, "", "0", new Callback<UserManage>() {
                        @Override
                        public void onResponse(Call<UserManage> call, Response<UserManage> response) {
                            Util.hideProgressDialog();
                            String responseJson = new Gson().toJson(response.body());
                            Timber.i("Add user ", responseJson);
                            Toast.makeText(AddEmpActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            if (!(response.body().isError())) {
                                Intent intent = new Intent();
                                intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getUser());
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<UserManage> call, Throwable t) {

                        }
                    });
                }
                break;
            case R.id.btnUpdateEmp:
                Util.showProgressDialog(AddEmpActivity.this);
                DataManager.getInstance().userManage(fName, mName, lName, mobile, email, AppConstants.ACTION_TYPE_UPDATE, user.getId(), "0", new Callback<UserManage>() {
                    @Override
                    public void onResponse(Call<UserManage> call, Response<UserManage> response) {
                        Util.hideProgressDialog();
                        String responseJson = new Gson().toJson(response.body());
                        Timber.i("Add user ", responseJson);
                        Toast.makeText(AddEmpActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        if (!(response.body().isError())) {
                            Intent intent = new Intent();
                            intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getUser());
                            intent.putExtra(AppConstants.EXTRA_ADAPTER_POSITION, update_position);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                    @Override
                    public void onFailure(Call<UserManage> call, Throwable t) {

                    }
                });

                break;
        }
    }
}
