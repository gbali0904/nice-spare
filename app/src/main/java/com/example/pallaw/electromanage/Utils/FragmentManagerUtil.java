package com.example.pallaw.electromanage.Utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by pallaw on 04/10/17.
 */

public class FragmentManagerUtil {
    public static void replaceFragment(FragmentManager supportFragmentManager, int container, Fragment target, boolean addToBackstack, String tag) {
        Fragment fragment = target;
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = supportFragmentManager;
        fragmentManager.executePendingTransactions();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(container, fragment, tag);
        if (addToBackstack)
            fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }
}
