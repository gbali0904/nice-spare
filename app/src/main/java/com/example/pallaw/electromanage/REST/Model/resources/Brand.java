package com.example.pallaw.electromanage.REST.Model.resources;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pallaw on 10/10/17.
 */

public class Brand implements Parcelable {

    /**
     * id : 4
     * name : Sony
     * Description : here is sony
     * flag : 0
     * admin_id : 1000
     * time_stamp : 2017-10-10 14:39:40.441276
     */

    private String id;
    private String name;
    private String Description;
    private String flag;
    private String admin_id;
    private String time_stamp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(String time_stamp) {
        this.time_stamp = time_stamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.Description);
        dest.writeString(this.flag);
        dest.writeString(this.admin_id);
        dest.writeString(this.time_stamp);
    }

    public Brand() {
    }

    protected Brand(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.Description = in.readString();
        this.flag = in.readString();
        this.admin_id = in.readString();
        this.time_stamp = in.readString();
    }

    public static final Parcelable.Creator<Brand> CREATOR = new Parcelable.Creator<Brand>() {
        @Override
        public Brand createFromParcel(Parcel source) {
            return new Brand(source);
        }

        @Override
        public Brand[] newArray(int size) {
            return new Brand[size];
        }
    };
}
