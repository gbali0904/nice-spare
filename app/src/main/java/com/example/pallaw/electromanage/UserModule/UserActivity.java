package com.example.pallaw.electromanage.UserModule;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.Utils.FragmentManagerUtil;

public class UserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FragmentManagerUtil.replaceFragment(getSupportFragmentManager(),R.id.frameUserActivity,new LoginFragment(),false,LoginFragment.TAG);
    }

}
