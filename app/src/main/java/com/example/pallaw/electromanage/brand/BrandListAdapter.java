package com.example.pallaw.electromanage.brand;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.Model.resources.Brand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BrandListAdapter extends RecyclerView.Adapter<BrandListAdapter.ViewHolder> {

    public static List<Brand> data;
    private static BrandListFragment.OnBrandDeleteListener brandDeleteListener;
    private static BrandListFragment.OnBrandStatusChangeListener brandStatusChangeListener;
    private static BrandListFragment.OnBrandUpdateListener onBrandUpdateListener;
    static SparseBooleanArray selectedItemsforDelete= new SparseBooleanArray();


    public static ArrayList<Brand> selected_brandList=new ArrayList<Brand>() ;
    public BrandListAdapter(List<Brand> data, BrandListFragment.OnBrandDeleteListener brandDeleteListener, BrandListFragment.OnBrandStatusChangeListener brandStatusChangeListener,BrandListFragment.OnBrandUpdateListener onBrandUpdateListener) {
        this.data = data;
        this.brandDeleteListener = brandDeleteListener;
        this.brandStatusChangeListener = brandStatusChangeListener;
        this.onBrandUpdateListener = onBrandUpdateListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem = data.get(position);
        holder.bindData();
        if(position %2 == 1)
        {
            holder.itemView.setBackgroundColor(Color.parseColor("#22718792"));
        }
        else
        {
            // Set the background color for alternate row/item
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void notifyDataChanged() {
        selectedItemsforDelete.clear();
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(Brand string) {
        insert(string, data.size());
    }

    public void insert(Brand string, int position) {
        data.add(position, string);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(Brand[] strings) {
        int startIndex = data.size();
        data.addAll(Arrays.asList(strings));
        notifyItemRangeInserted(startIndex, strings.length);
    }

    public void updateStatus(int position, String flag) {
        data.get(position).setFlag(flag);
        notifyItemChanged(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
       @BindView(R.id.txtItemCatName)
        TextView txtItemCatName;
       @BindView(R.id.btnItemCatDelete)
        Button btnItemCatDelete;
       @BindView(R.id.swItemCatStatus)
        Switch swItemCatStatus;
       @BindView(R.id.sno)
        TextView sno;
       @BindView(R.id.delete)
        CheckBox delete;

        public Brand mItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            delete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public String id;

                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        selected_brandList.add(mItem);
                    }
                    else {
                        selected_brandList.remove(mItem);
                    }
                    brandDeleteListener.onBrandDelete(selected_brandList,getAdapterPosition());
                    selectedItemsforDelete.put(getAdapterPosition(),b);
                }
            });



            btnItemCatDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // brandDeleteListener.onBrandDelete(mItem,getAdapterPosition());
                }
            });

            swItemCatStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int status = (swItemCatStatus.isChecked()) ? 1 : 0;
                    brandStatusChangeListener.onBrandStatusChange(mItem,getAdapterPosition(),String.valueOf(status));
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBrandUpdateListener.onBrandUpdate(mItem,getAdapterPosition());
                }
            });

        }

        public void bindData() {
            int adapterPosition = getAdapterPosition()+1;
            txtItemCatName.setText(String.format("%s",mItem.getName()));
            sno.setText(""+adapterPosition);
            swItemCatStatus.setChecked((data.get(getAdapterPosition()).getFlag().equalsIgnoreCase("0")?false:true));
            delete.setChecked(selectedItemsforDelete.get(getAdapterPosition()));
        }
    }
}
