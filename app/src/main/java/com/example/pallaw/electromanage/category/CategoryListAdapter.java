package com.example.pallaw.electromanage.category;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.Model.resources.Category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    public static List<Category> data;
    public static CategoryFragment.OnCategoryDeleteListener categoryDeleteListener;
    public static CategoryFragment.OnCategoryStatusChangeListener categoryStatusChangeListener;
    public static CategoryFragment.OnCategoryUpdateListener onCategoryUpdateListener;
    public static ArrayList<Category> selected_categoryList=new ArrayList<>();
    static SparseBooleanArray selectedItemsforDelete= new SparseBooleanArray();
     boolean updateStatus=false;

    public CategoryListAdapter(List<Category> data, CategoryFragment.OnCategoryDeleteListener categoryDeleteListener, CategoryFragment.OnCategoryStatusChangeListener categoryStatusChangeListener, CategoryFragment.OnCategoryUpdateListener onCategoryUpdateListener) {
        this.data = data;
        this.categoryDeleteListener = categoryDeleteListener;
        this.categoryStatusChangeListener = categoryStatusChangeListener;
        this.onCategoryUpdateListener = onCategoryUpdateListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem = data.get(position);
        if(position %2 == 1)
        {
            holder.itemView.setBackgroundColor(Color.parseColor("#22718792"));
        }
        else
        {
            // Set the background color for alternate row/item
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

        holder.bindData(updateStatus);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void notifyDataChanged() {
        selectedItemsforDelete.clear();
        notifyDataSetChanged();
    }

    public void add(Category string) {
        insert(string, data.size());
    }

    public void insert(Category string, int position) {
        data.add(position, string);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(Category[] strings) {
        int startIndex = data.size();
        data.addAll(Arrays.asList(strings));
        notifyItemRangeInserted(startIndex, strings.length);
    }

    public void updateStatus(int position, String flag) {
        data.get(position).setFlag(flag);
        notifyItemChanged(position);
    }

    public void setStatus(boolean status) {
        updateStatus=status;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        @BindView(R.id.txtItemCatName)
        TextView txtItemCatName;
        @BindView(R.id.btnItemCatDelete)
        Button btnItemCatDelete;
        @BindView(R.id.sno)
        TextView sno;
        @BindView(R.id.swItemCatStatus)
        Switch swItemCatStatus;
        @BindView(R.id.delete)
        CheckBox delete;

        public Category mItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            delete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public String id;

                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        selected_categoryList.add(mItem);
                    }
                    else {
                        selected_categoryList.remove(mItem);
                    }

                    categoryDeleteListener.onCategoryDelete(selected_categoryList,getAdapterPosition());
                    selectedItemsforDelete.put(getAdapterPosition(),b);
                }
            });

            btnItemCatDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // categoryDeleteListener.onCategoryDelete(mItem,getAdapterPosition());
                }
            });

            swItemCatStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int status = (swItemCatStatus.isChecked()) ? 1 : 0;
                    categoryStatusChangeListener.onCategoryStatusChange(mItem,getAdapterPosition(),String.valueOf(status));
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCategoryUpdateListener.onCategoryUpdate(mItem,getAdapterPosition());
                }
            });

        }

        public void bindData(boolean update_Status) {

            if(update_Status){
                delete.setEnabled(false);
                swItemCatStatus.setEnabled(false);
            }
            int adapterPosition = getAdapterPosition()+1;
            txtItemCatName.setText(String.format("%s",mItem.getName()));
            sno.setText(""+adapterPosition);
            swItemCatStatus.setChecked((data.get(getAdapterPosition()).getFlag().equalsIgnoreCase("0")?false:true));
            delete.setChecked(selectedItemsforDelete.get(getAdapterPosition()));
        }

    }
}
