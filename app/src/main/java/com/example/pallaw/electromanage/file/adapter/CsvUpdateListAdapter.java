package com.example.pallaw.electromanage.file.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.Model.resources.UploadFile;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Android on 12/21/2017.
 */

public class CsvUpdateListAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;
    private final List<UploadFile.DataSuccessBean> dataSuccess;


    public CsvUpdateListAdapter(FragmentActivity activity, List<UploadFile.DataSuccessBean> dataSuccess) {
        this.activity = activity;
        this.dataSuccess = dataSuccess;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.csv_deatil, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bindData(position);
    }

    @Override
    public int getItemCount() {
        return dataSuccess.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.sno)
        TextView sno;
        @BindView(R.id.txtItemCatName)
        TextView txtItemCatName;
        @BindView(R.id.txtItemproductstatus)
        TextView txtItemproductstatus;
        @BindView(R.id.lay)
        LinearLayout lay;
        @BindView(R.id.view)
        View view;
        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindData(int position) {
            UploadFile.DataSuccessBean dataSuccessBean = dataSuccess.get(position);
            sno.setText(""+dataSuccessBean.getId());
            txtItemCatName.setText(""+dataSuccessBean.getName());
            txtItemproductstatus.setText(""+dataSuccessBean.getStatus());
        }
    }
}
