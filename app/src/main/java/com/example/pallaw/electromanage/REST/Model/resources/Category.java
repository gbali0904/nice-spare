package com.example.pallaw.electromanage.REST.Model.resources;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pallaw on 10/10/17.
 */

public class Category implements Parcelable {

    /**
     * id : 17
     * name : TV
     * Description : here is tv
     * flag : 1
     * timestamp : 2017-10-09 16:25:03.507206
     * adminid : 1000
     */

    private String id;
    private String name;
    private String Description;
    private String flag;
    private String timestamp;
    private String adminid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAdminid() {
        return adminid;
    }

    public void setAdminid(String adminid) {
        this.adminid = adminid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.Description);
        dest.writeString(this.flag);
        dest.writeString(this.timestamp);
        dest.writeString(this.adminid);
    }

    public Category() {
    }

    protected Category(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.Description = in.readString();
        this.flag = in.readString();
        this.timestamp = in.readString();
        this.adminid = in.readString();
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
