package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.Product;

import java.util.List;

/**
 * Created by pallaw on 24/10/17.
 */

public class SearchResult {

    /**
     * error : false
     * code : 0
     * msg : List of all Product fetched successfully
     * products : [{"id":"7","Description":"For sounds","flag":"0","product_name":"productname","stock_status":"y","timestamp":"2017-10-10 22:49:40.935856","unit_flag":"10","price":"10000","admin_id":"1000","brand":{"id":"7","name":"samsung"},"model":{"id":"10","name":"8"},"category":{"id":"20","name":"Speaker"},"sub_category":{"id":"7","name":"fridge sub category"}},{"id":"6","Description":"here is laptop","flag":"1","product_name":"test","stock_status":"n","timestamp":"2017-10-10 21:27:29.799584","unit_flag":"10","price":"10000","admin_id":"1000","brand":{"id":"5","name":"Apple"},"model":{"id":"5","name":"4S"},"category":{"id":"31","name":"laptop"},"sub_category":{"id":"6","name":"sub cat"}}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<Product> products;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
