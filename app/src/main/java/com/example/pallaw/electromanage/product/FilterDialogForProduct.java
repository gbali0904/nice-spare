package com.example.pallaw.electromanage.product;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.ModelForFilterList;
import com.example.pallaw.electromanage.Utils.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.pallaw.electromanage.product.GenreDataFactoryForFilter.makeMultiCheckFilters;

/**
 * Created by Android on 11/17/2017.
 */

public class FilterDialogForProduct extends DialogFragment {

    public static final String TAG = FilterDialogForProduct.class.getSimpleName();
    private static FilterDialogForProduct mInstance;
    private static OnFilterListener productfilterListener;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.btnDialogfilterOk)
    Button btnDialogfilterOk;
    @BindView(R.id.btnDialogfilterCancel)
    Button btnDialogfilterCancel;

    private FilterForProduct adapter;
    private List<ModelForFilterList.FilterBean> filterResponse=new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private HashMap<String, List<String>> filterList;

    public static FilterDialogForProduct getInstance(OnFilterListener onProductFilterListener) {
        productfilterListener = onProductFilterListener;

        if (mInstance == null) {
            mInstance = new FilterDialogForProduct();
        }
        return mInstance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_filter_dialog, container, false);
        ButterKnife.bind(this, view);

        layoutManager = new LinearLayoutManager(getActivity());
        getData();
        return view;
    }


    private void getData() {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getFilterList(new Callback<ModelForFilterList>() {
            @Override
            public void onResponse(Call<ModelForFilterList> call, Response<ModelForFilterList> response) {
                Util.hideProgressDialog();
                if (response.body().getCode() == 0) {

                    filterResponse.clear();
                    filterResponse = response.body().getFilter();
                    adapter = new FilterForProduct(makeMultiCheckFilters(filterResponse));
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(adapter);
                } else {
                    Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ModelForFilterList> call, Throwable t) {
                Util.hideProgressDialog();
                Log.e("removeProduct", t.getMessage());
                t.printStackTrace();
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() == null)
            return;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.btnDialogfilterOk, R.id.btnDialogfilterCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnDialogfilterOk:
//                Log.e("paramValues",new Gson().toJson(FilterParams.getInstance()));
              productfilterListener.OnFilterListener(getDialog());
                break;
            case R.id.btnDialogfilterCancel:
                dismiss();
                break;
        }
    }


    public interface OnProductFilterListener {
        void OnProductFilterListener(HashMap<String, List<String>> selected_categoryList);
    }


    public interface OnFilterListener {
        void OnFilterListener(Dialog selected_categoryList);
    }
}
