package com.example.pallaw.electromanage.category;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.resources.Category;
import com.example.pallaw.electromanage.Utils.AppConstants;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateCategory extends Fragment {

    public static HashMap<String, ModelForUpdateCategory> Udate_hashMaps = new HashMap<String, ModelForUpdateCategory>();


    public static final String TAG = UpdateCategory.class.getSimpleName();
    private static OnUpdateListener mListener;
    @BindView(R.id.edtAddCatName)
    EditText edtAddCatName;
    @BindView(R.id.textInputLayout3)
    TextInputLayout textInputLayout3;
    @BindView(R.id.btnUpdateCat)
    Button btnUpdateCat;

    private Bundle bundle;
    private Category category;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_category, container, false);
        ButterKnife.bind(this, view);

        bundle = getArguments();
        category = bundle.getParcelable(AppConstants.EXTRA_UPDATE_ITEM);
        updateFields();


        return view;
    }

    private void updateFields() {
        btnUpdateCat.setVisibility(View.VISIBLE);
        edtAddCatName.setText(String.format("%s", category.getName()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.btnUpdateCat)
    public void onViewClicked() {
        HashMap<String, String> stringStringHashMap = new HashMap<>();

        ModelForUpdateCategory modelForUpdateCategory = new ModelForUpdateCategory();


        String name = edtAddCatName.getText().toString().trim();
        String description = "";
        String flag = category.getFlag();
        String id = Authentication.getInstance().getData().getId();
        String actionTypeUpdate = AppConstants.ACTION_TYPE_UPDATE;
        String category_id = category.getId();

        modelForUpdateCategory.setName(name);
        modelForUpdateCategory.setDescription(description);
        modelForUpdateCategory.setFlag(flag);
        modelForUpdateCategory.setAdminid(id);
        modelForUpdateCategory.setActionTypeUpdate(actionTypeUpdate);
        modelForUpdateCategory.setId(category_id);

        Udate_hashMaps.put(category_id, modelForUpdateCategory);
        mListener.onUpdate(Udate_hashMaps);

        getActivity().finish();


    }


    public static void setOnUpdateListener(OnUpdateListener listener) {
        mListener = listener;
    }

    public interface OnUpdateListener {
        void onUpdate(HashMap<String, ModelForUpdateCategory> hashMaps);
    }


}
