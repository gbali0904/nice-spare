package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.Category;

/**
 * Created by pallaw on 10/10/17.
 */

public class CategoryOperation {

    /**
     * error : false
     * msg : Category Added successful
     * category : {"id":"26","name":"Macbook","Description":"New Range of Apple macbooks","flag":"1","timestamp":"2017-10-10 10:40:08.486376","adminid":"1000"}
     */

    private boolean error;
    private String msg;
    private Category category;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


}
