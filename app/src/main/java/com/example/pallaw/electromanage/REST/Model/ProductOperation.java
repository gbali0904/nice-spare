package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.Product;

/**
 * Created by pallaw on 13/10/17.
 */

public class ProductOperation {

    /**
     * error : false
     * msg : Sub-Category Updated successful
     * product : {"id":"9","Description":"here is my computer","flag":"0","product_name":"pavilion","stock_status":"y","timestamp":"2017-10-12 05:25:24.613126","unit_flag":"10","price":"45000","admin_id":"1000","brand":{"id":"10","name":"lenovo"},"model":{"id":"5","name":"DEF Model"},"category":{"id":"31","name":"laptop"},"sub_category":{"id":"8","name":"pavilion"}}
     */

    private boolean error;
    private String msg;
    private Product product;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
