package com.example.pallaw.electromanage.subcategory;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.Model.resources.SubCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCategoryListAdapter extends RecyclerView.Adapter<SubCategoryListAdapter.ViewHolder> {

    public static List<SubCategory> data;
    private static SubCategoryFragment.OnSubCategoryDeleteListener subCategoryDeleteListener;
    private static SubCategoryFragment.OnSubCategoryStatusChangeListener onSubCategoryStatusChangeListener;
    private static SubCategoryFragment.OnSubCategoryUpdateListener subCategoryUpdateListener;
    static SparseBooleanArray selectedItemsforDelete= new SparseBooleanArray();

    public static ArrayList<SubCategory> selected_subcategoryList=new ArrayList<SubCategory>();

    public SubCategoryListAdapter(List<SubCategory> data, SubCategoryFragment.OnSubCategoryDeleteListener subCategoryDeleteListener, SubCategoryFragment.OnSubCategoryStatusChangeListener onSubCategoryStatusChangeListener, SubCategoryFragment.OnSubCategoryUpdateListener subCategoryUpdateListener) {
        this.data = data;
        this.subCategoryDeleteListener = subCategoryDeleteListener;
        this.onSubCategoryStatusChangeListener = onSubCategoryStatusChangeListener;
        this.subCategoryUpdateListener = subCategoryUpdateListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem = data.get(position);
        holder.bindData();
        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.parseColor("#22718792"));
        } else {
            // Set the background color for alternate row/item
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void notifyDataChanged() {
        selectedItemsforDelete.clear();
        notifyDataSetChanged();
    }

    public void add(SubCategory string) {
        insert(string, data.size());
    }

    public void insert(SubCategory string, int position) {
        data.add(position, string);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(SubCategory[] strings) {
        int startIndex = data.size();
        data.addAll(Arrays.asList(strings));
        notifyItemRangeInserted(startIndex, strings.length);
    }

    public void updateStatus(int position, String flag) {
        data.get(position).setFlag(flag);
        notifyItemChanged(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        @BindView(R.id.txtItemCatName)
        TextView txtItemCatName;
        @BindView(R.id.btnItemCatDelete)
        Button btnItemCatDelete;
        @BindView(R.id.swItemCatStatus)
        Switch swItemCatStatus;
        @BindView(R.id.txtItemSubCatName)
        TextView txtItemSubCatName;
        @BindView(R.id.delete)
        CheckBox delete;
        public SubCategory mItem;
        @BindView(R.id.sno)
        TextView sno;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            delete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                 public String id;

                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        selected_subcategoryList.add(mItem);
                    }
                    else {
                        selected_subcategoryList.remove(mItem);
                    }
                    subCategoryDeleteListener.onSubCategoryDelete(selected_subcategoryList,getAdapterPosition());
                    selectedItemsforDelete.put(getAdapterPosition(),b);
                }
            });




            btnItemCatDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                 //   subCategoryDeleteListener.onSubCategoryDelete(mItem, getAdapterPosition());
                }
            });

            swItemCatStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int status = (swItemCatStatus.isChecked()) ? 1 : 0;
                    onSubCategoryStatusChangeListener.onSubCategoryStatusChange(mItem, getAdapterPosition(), String.valueOf(status));
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    subCategoryUpdateListener.onSubCategoryUpdate(mItem, getAdapterPosition());
                }
            });

        }

        public void bindData() {
            int adapterPosition = getAdapterPosition()+1;
            txtItemCatName.setText(String.format("%s", mItem.getName()));
            sno.setText(""+adapterPosition);
            swItemCatStatus.setChecked((data.get(getAdapterPosition()).getFlag().equalsIgnoreCase("0") ? false : true));
            txtItemSubCatName.setText(String.format("%s", mItem.getCategory().getName()));
            delete.setChecked(selectedItemsforDelete.get(getAdapterPosition()));
        }
    }
}
