package com.example.pallaw.electromanage.REST.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Android on 11/14/2017.
 */

public class ModelForFilterList {


    /**
     * error : false
     * code : 0
     * msg : Filter List Feched successful
     * filter : [{"brands":[{"id":"9","name":"Acer","staus":"0"}]},{"model":[{"id":"11","name":"LED - Iphone 4S","staus":"0"}]},{"category":[{"id":"31","name":"laptop","staus":"0"}]},{"sub-category":[{"id":"5","name":"sony","staus":"0"}]}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<FilterBean> filter;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<FilterBean> getFilter() {
        return filter;
    }

    public void setFilter(List<FilterBean> filter) {
        this.filter = filter;
    }

    public static class FilterBean {
        private List<BrandsBean> brands;
        private List<ModelBean> model;
        private List<CategoryBean> category;
        @SerializedName("sub-category")
        private List<SubcategoryBean> subcategory;

        public List<BrandsBean> getBrands() {
            return brands;
        }

        public void setBrands(List<BrandsBean> brands) {
            this.brands = brands;
        }

        public List<ModelBean> getModel() {
            return model;
        }

        public void setModel(List<ModelBean> model) {
            this.model = model;
        }

        public List<CategoryBean> getCategory() {
            return category;
        }

        public void setCategory(List<CategoryBean> category) {
            this.category = category;
        }

        public List<SubcategoryBean> getSubcategory() {
            return subcategory;
        }

        public void setSubcategory(List<SubcategoryBean> subcategory) {
            this.subcategory = subcategory;
        }

        public static class BrandsBean {
            /**
             * id : 9
             * name : Acer
             * staus : 0
             */

            private String id;
            private String name;
            private String staus;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStaus() {
                return staus;
            }

            public void setStaus(String staus) {
                this.staus = staus;
            }
        }

        public static class ModelBean {
            /**
             * id : 11
             * name : LED - Iphone 4S
             * staus : 0
             */

            private String id;
            private String name;
            private String staus;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStaus() {
                return staus;
            }

            public void setStaus(String staus) {
                this.staus = staus;
            }
        }

        public static class CategoryBean {
            /**
             * id : 31
             * name : laptop
             * staus : 0
             */

            private String id;
            private String name;
            private String staus;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStaus() {
                return staus;
            }

            public void setStaus(String staus) {
                this.staus = staus;
            }
        }

        public static class SubcategoryBean {
            /**
             * id : 5
             * name : sony
             * staus : 0
             */

            private String id;
            private String name;
            private String staus;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStaus() {
                return staus;
            }

            public void setStaus(String staus) {
                this.staus = staus;
            }
        }
    }
}
