package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.SubCategory;

/**
 * Created by pallaw on 11/10/17.
 */

public class SubCategoryOperation {


    /**
     * error : false
     * msg : Sub-Category Updated successful
     * subcategory : {"id":"4","name":"sony","description":"here is my sony","flag":"0","timestamp":"2017-10-10 15:56:42.119677","adminid":"1000","category_id":"16"}
     */

    private boolean error;
    private String msg;
    private SubCategory subcategory;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public SubCategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(SubCategory subcategory) {
        this.subcategory = subcategory;
    }

}
