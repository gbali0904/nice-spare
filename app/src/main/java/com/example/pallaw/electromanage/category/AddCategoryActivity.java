package com.example.pallaw.electromanage.category;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.CategoryOperation;
import com.example.pallaw.electromanage.REST.Model.resources.Category;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AddCategoryActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtAddCatName)
    EditText edtAddCatName;
    @BindView(R.id.textInputLayout3)
    TextInputLayout textInputLayout3;
    @BindView(R.id.btnAddCat)
    Button btnAddCat;
    @BindView(R.id.btnUpdateCat)
    Button btnUpdateCat;
    private String name;
    private Category category;
    private int update_position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().getExtras() != null) {
            category = getIntent().getExtras().getParcelable(AppConstants.EXTRA_UPDATE_ITEM);
            update_position = getIntent().getExtras().getInt(AppConstants.EXTRA_ADAPTER_POSITION);
            if (category != null) {
                setTitle(getResources().getString(R.string.title_update_category));
                updateFields();
                return;
            }
        }

        setTitle(getResources().getString(R.string.title_add_category));

    }

    private void updateFields() {
        btnUpdateCat.setVisibility(View.VISIBLE);
        btnAddCat.setVisibility(View.GONE);
        edtAddCatName.setText(String.format("%s", category.getName()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    private boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(name)) {
            isValid = false;
            edtAddCatName.setError(getResources().getString(R.string.error_cat_name));
        }

        return isValid;
    }

    @OnClick({R.id.btnAddCat, R.id.btnUpdateCat})
    public void onViewClicked(View view) {
        name = edtAddCatName.getText().toString().trim();

        switch (view.getId()) {
            case R.id.btnAddCat:
                if (validation()) {
                    Util.showProgressDialog(AddCategoryActivity.this);
                    DataManager.getInstance().categoryOperations(name, "", "0", Authentication.getInstance().getData().getId(), AppConstants.ACTION_TYPE_ADD, "", new Callback<CategoryOperation>() {
                        @Override
                        public void onResponse(Call<CategoryOperation> call, Response<CategoryOperation> response) {
                            Util.hideProgressDialog();
                            String responseJson = new Gson().toJson(response.body());
                            Timber.i("Add category", responseJson);
                            Toast.makeText(AddCategoryActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            if (!(response.body().isError())) {
                                Intent intent = new Intent();
                                intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getCategory());
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<CategoryOperation> call, Throwable t) {

                        }
                    });
                }
                break;
            case R.id.btnUpdateCat:



               /* Util.showProgressDialog(AddCategoryActivity.this);
                DataManager.getInstance().categoryOperations(name, "", category.getFlag(), Authentication.getInstance().getData().getId(), AppConstants.ACTION_TYPE_UPDATE, category.getId(), new Callback<CategoryOperation>() {
                    @Override
                    public void onResponse(Call<CategoryOperation> call, Response<CategoryOperation> response) {
                        Util.hideProgressDialog();
                        String responseJson = new Gson().toJson(response.body());
                        Timber.i("Add category", responseJson);
                        Toast.makeText(AddCategoryActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        if (!(response.body().isError())) {
                            Intent intent = new Intent();
                            intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getCategory());
                            intent.putExtra(AppConstants.EXTRA_ADAPTER_POSITION, update_position);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryOperation> call, Throwable t) {

                    }
                });*/
                break;
        }
    }
}
