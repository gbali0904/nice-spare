package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.Brand;

import java.util.List;

/**
 * Created by pallaw on 10/10/17.
 */

public class AllBrands {

    /**
     * error : false
     * code : 0
     * msg : List of all brand fetched successfully
     * brands : [{"id":"4","name":"Sony","Description":"here is sony","flag":"0","admin_id":"1000","time_stamp":"2017-10-10 14:39:40.441276"}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<Brand> brands;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

}
