package com.example.pallaw.electromanage.subcategory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllCategories;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.SubCategoryOperation;
import com.example.pallaw.electromanage.REST.Model.resources.Category;
import com.example.pallaw.electromanage.REST.Model.resources.SubCategory;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AddSubCategoryActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtAddCatName)
    EditText edtAddCatName;
    @BindView(R.id.btnAddCat)
    Button btnAddCat;
    @BindView(R.id.spiAddSubCat)
    Spinner spiAddSubCat;
    List<String> subCategories = new ArrayList<String>();
    @BindView(R.id.btnUpdateCat)
    Button btnUpdateCat;
    private String name;
    private List<Category> categories;
    private Category selectedCategory;
    private SubCategory subCategory;
    private int update_position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sub_category);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //download categories
        downloadCategories();

        if (getIntent().getExtras() != null) {
            subCategory = getIntent().getExtras().getParcelable(AppConstants.EXTRA_UPDATE_ITEM);
            update_position = getIntent().getExtras().getInt(AppConstants.EXTRA_ADAPTER_POSITION);
            if (subCategory != null) {
                setTitle(getResources().getString(R.string.title_update_sub_category));
                updateFields();
                return;
            }
        }
        setTitle(getResources().getString(R.string.title_add_sub_category));
    }

    private void updateFields() {
        btnAddCat.setVisibility(View.GONE);
        btnUpdateCat.setVisibility(View.VISIBLE);
        edtAddCatName.setText(String.format("%s",subCategory.getName()));

    }

    private void downloadCategories() {
        Util.showProgressDialog(AddSubCategoryActivity.this);
        DataManager.getInstance().getAllCategories("", "", "", "", AppConstants.STATUS_ACTIVE, "", new Callback<AllCategories>() {
            @Override
            public void onResponse(Call<AllCategories> call, Response<AllCategories> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllCategories.class);
                Timber.i("downloadCategory", responseJson);
//                Toast.makeText(AddSubCategoryActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                if (!response.body().isError()) {
                    categories = response.body().getCategory();
                    spiAddSubCat.setAdapter(new MyCustomAdapter(AddSubCategoryActivity.this, R.layout.item_spinner, categories));

                    if (subCategory != null)
                    {

                            for(Category category:categories){
                                if (category.getId().equalsIgnoreCase(subCategory.getCategory().getId())){
                                    spiAddSubCat.setSelection(categories.indexOf(category));
                                }
                            }
                    }
                    spiAddSubCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            selectedCategory = categories.get(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<AllCategories> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadCategory", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(name)) {
            isValid = false;
            edtAddCatName.setError(getResources().getString(R.string.error_cat_name));
        }

        return isValid;
    }

    @OnClick({R.id.btnAddCat, R.id.btnUpdateCat})
    public void onViewClicked(View view) {

        name = edtAddCatName.getText().toString().trim();

        switch (view.getId()) {
            case R.id.btnAddCat:
                if (validation()) {
                    Util.showProgressDialog(AddSubCategoryActivity.this);
                    DataManager.getInstance().subcategoryOperation(name, "", "0", Authentication.getInstance().getData().getId(), selectedCategory.getId(), AppConstants.ACTION_TYPE_ADD, "", new Callback<SubCategoryOperation>() {
                        @Override
                        public void onResponse(Call<SubCategoryOperation> call, Response<SubCategoryOperation> response) {
                            Util.hideProgressDialog();
                            String responseJson = new Gson().toJson(response.body());
                            Timber.i("Add sub category", responseJson);
                            Toast.makeText(AddSubCategoryActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            if (!(response.body().isError())) {
                                Intent intent = new Intent();
                                intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getSubcategory());
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<SubCategoryOperation> call, Throwable t) {

                        }
                    });

                }
                break;
            case R.id.btnUpdateCat:
                Util.showProgressDialog(AddSubCategoryActivity.this);
                DataManager.getInstance().subcategoryOperation(name, "", subCategory.getFlag(), Authentication.getInstance().getData().getId(), selectedCategory.getId(), AppConstants.ACTION_TYPE_UPDATE, subCategory.getId(), new Callback<SubCategoryOperation>() {
                    @Override
                    public void onResponse(Call<SubCategoryOperation> call, Response<SubCategoryOperation> response) {
                        Util.hideProgressDialog();
                        String responseJson = new Gson().toJson(response.body());
                        Timber.i("Add sub category", responseJson);
                        Toast.makeText(AddSubCategoryActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        if (!(response.body().isError())) {
                            Intent intent = new Intent();
                            intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getSubcategory());
                            intent.putExtra(AppConstants.EXTRA_ADAPTER_POSITION, update_position);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<SubCategoryOperation> call, Throwable t) {

                    }
                });
                break;
        }
    }

    public class MyCustomAdapter extends ArrayAdapter<Category> {

        private final List<Category> categories;

        public MyCustomAdapter(Context context, int textViewResourceId, List<Category> objects) {
            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
            this.categories = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.item_spinner, parent, false);
            TextView label = (TextView) row.findViewById(R.id.txtItemSpinnerCategory);
            label.setText(categories.get(position).getName());
            return row;
        }
    }

}
