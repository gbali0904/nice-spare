package com.example.pallaw.electromanage.employee;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.Model.resources.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmpListAdapter extends RecyclerView.Adapter<EmpListAdapter.ViewHolder> {

    public static List<User> data;
    public static EmpListFragment.OnEmpSelectedListener empSelectedListener;
    public static EmpListFragment.OnEmpStatusChangeListener empStatusChangeListener;
    public static EmpListFragment.OnEmpUpdateListener empUpdateListener;
    SparseBooleanArray selectedItemsforDelete= new SparseBooleanArray();


    public ArrayList<User> selected_usersList=new ArrayList<>();
    public EmpListAdapter(List<User> data, EmpListFragment.OnEmpSelectedListener empSelectedListener, EmpListFragment.OnEmpStatusChangeListener empStatusChangeListener, EmpListFragment.OnEmpUpdateListener empUpdateListener) {
        this.data = data;
        this.empSelectedListener = empSelectedListener;
        this.empStatusChangeListener = empStatusChangeListener;
        this.empUpdateListener = empUpdateListener;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem=data.get(position);
        holder.itemView.setBackgroundColor(Color.parseColor((position %2 == 1)?"#22718792":"#FFFFFF"));
        holder.bindData();

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(User string) {
        insert(string, data.size());
    }

    public void insert(User string, int position) {
        data.add(position, string);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void updateStatus(int position, String flag) {
        data.get(position).setFlag(flag);
        notifyItemChanged(position);
    }

    public void clear() {
        int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(User[] strings) {
        int startIndex = data.size();
        data.addAll(startIndex, Arrays.asList(strings));
        notifyItemRangeInserted(startIndex, strings.length);
    }

    public void clearSelectedItem() {
        selected_usersList.clear();
        selectedItemsforDelete.clear();
    }

    public void notifyDataChanged() {
        selectedItemsforDelete.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        @BindView(R.id.txtItemEmpListName)
        TextView txtItemEmpListName;
        @BindView(R.id.txtItemEmpListNumber)
        TextView txtItemEmpListNumber;
        @BindView(R.id.sno)
        TextView sno;
        @BindView(R.id.btnItemEmpListDelete)
        Button btnItemEmpListDelete;
        @BindView(R.id.swItemEmpListStatus)
        Switch swItemEmpListStatus;
        @BindView(R.id.delete)
        CheckBox delete;
        public User mItem;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            Log.e("this",selected_usersList.toString());

            delete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public String id;

                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                    if (compoundButton.isChecked()) {
                        selected_usersList.add(mItem);
                    }
                    else {
                        selected_usersList.remove(mItem);
                    }
                    empSelectedListener.onEmpSelected(selected_usersList, getAdapterPosition());

                    selectedItemsforDelete.put(getAdapterPosition(),isChecked);

                }
            });
            btnItemEmpListDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //    empSelectedListener.onEmpSelected(mItem,getAdapterPosition());
                }
            });

            swItemEmpListStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int status = (swItemEmpListStatus.isChecked()) ? 1 : 0;
                    empStatusChangeListener.onEmpStatusChange(mItem,getAdapterPosition(),String.valueOf(status));
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    empUpdateListener.onEmpUpdate(mItem,getAdapterPosition());
                }
            });
        }

        public void bindData() {
            int adapterPosition = getAdapterPosition()+1;
            txtItemEmpListName.setText(String.format("%s %s %s",mItem.getFirst_name(),mItem.getMiddel_name(),mItem.getLast_name()));
            sno.setText(""+adapterPosition);
            swItemEmpListStatus.setChecked((data.get(getAdapterPosition()).getFlag().equalsIgnoreCase("0")?false:true));
            delete.setChecked(selectedItemsforDelete.get(getAdapterPosition()));

            if (mItem.getPass_status().equals("0"))
            {
                txtItemEmpListNumber.setText(Html.fromHtml(mItem.getMobile_no()+
                        "<br><font color='#ff0000'>"+
                                mItem.getPassword()+"</font>"));
            }
            else {
                txtItemEmpListNumber.setText(mItem.getMobile_no());
            }
        }
    }
}
