package com.example.pallaw.electromanage.REST.Model.resources;

import java.util.List;

/**
 * Created by Android on 11/2/2017.
 */

public class UploadFile {
    /**
     * error : false
     * code : 0
     * msg : CSV Response
     * dataSuccess : [{"id":"488","status":"Updated"}]
     * dataError : [{"id":"","name":"test4","status":"Name already exists"}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<DataSuccessBean> dataSuccess;
    private List<DataSuccessBean> dataError;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataSuccessBean> getDataSuccess() {
        return dataSuccess;
    }

    public void setDataSuccess(List<DataSuccessBean> dataSuccess) {
        this.dataSuccess = dataSuccess;
    }

    public List<DataSuccessBean> getDataError() {
        return dataError;
    }

    public void setDataError(List<DataSuccessBean> dataError) {
        this.dataError = dataError;
    }

    public static class DataSuccessBean {
        /**
         * id : 488
         * status : Updated
         */

        private String id;
        private String status;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public static class DataErrorBean {
        /**
         * id :
         * name : test4
         * status : Name already exists
         */

        private String id;
        private String name;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
    /**
     * error : false
     * code : 0
     * msg : Product price updated successful
     */


}
