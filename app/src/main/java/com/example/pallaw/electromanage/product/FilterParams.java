package com.example.pallaw.electromanage.product;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by pallaw on 19/11/17.
 */

class FilterParams {
    private static FilterParams ourInstance ;
    private List<BrandsBean> brands;
    private List<SubcategoryBean> subcategory;
    private List<CategoryBean> category;
    private List<ModelsBean> models;

    public static FilterParams getInstance() {
        if(ourInstance==null){
            String initialString="{\"brands\":[],\"subcategory\":[],\"category\":[],\"models\":[]}";
            ourInstance=new Gson().fromJson(initialString,FilterParams.class);
        }
        return ourInstance;
    }

    private FilterParams() {
    }

    public List<BrandsBean> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandsBean> brands) {
        this.brands = brands;
    }

    public List<SubcategoryBean> getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(List<SubcategoryBean> subcategory) {
        this.subcategory = subcategory;
    }

    public List<CategoryBean> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryBean> category) {
        this.category = category;
    }

    public List<ModelsBean> getModels() {
        return models;
    }

    public void setModels(List<ModelsBean> models) {
        this.models = models;
    }


    public static class BrandsBean {
        /**
         * id : 9
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class SubcategoryBean {
        /**
         * id : 5
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class CategoryBean {
        /**
         * id : 50
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class ModelsBean {
        /**
         * id : 16
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
