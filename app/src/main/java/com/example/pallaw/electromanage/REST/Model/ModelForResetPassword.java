package com.example.pallaw.electromanage.REST.Model;

/**
 * Created by Gunjan on 26-11-2017.
 */

public class ModelForResetPassword {


    /**
     * error : false
     * code : 0
     * msg : Update User Password successful
     * data : {"id":"1074","first_name":"Gunjan","middel_name":"","last_name":"Bali","mobile_no":"7696343553","email_id":"gbali@gmail.com","flag":"1","Timestamp":"2017-11-26 13:32:58.271919","password":"#^msN1kx","admin_id":"1000","pass_status":"0","status":"u"}
     */

    private boolean error;
    private int code;
    private String msg;
    private DataBean data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1074
         * first_name : Gunjan
         * middel_name :
         * last_name : Bali
         * mobile_no : 7696343553
         * email_id : gbali@gmail.com
         * flag : 1
         * Timestamp : 2017-11-26 13:32:58.271919
         * password : #^msN1kx
         * admin_id : 1000
         * pass_status : 0
         * status : u
         */

        private String id;
        private String first_name;
        private String middel_name;
        private String last_name;
        private String mobile_no;
        private String email_id;
        private String flag;
        private String Timestamp;
        private String password;
        private String admin_id;
        private String pass_status;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getMiddel_name() {
            return middel_name;
        }

        public void setMiddel_name(String middel_name) {
            this.middel_name = middel_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getMobile_no() {
            return mobile_no;
        }

        public void setMobile_no(String mobile_no) {
            this.mobile_no = mobile_no;
        }

        public String getEmail_id() {
            return email_id;
        }

        public void setEmail_id(String email_id) {
            this.email_id = email_id;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getTimestamp() {
            return Timestamp;
        }

        public void setTimestamp(String Timestamp) {
            this.Timestamp = Timestamp;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAdmin_id() {
            return admin_id;
        }

        public void setAdmin_id(String admin_id) {
            this.admin_id = admin_id;
        }

        public String getPass_status() {
            return pass_status;
        }

        public void setPass_status(String pass_status) {
            this.pass_status = pass_status;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
