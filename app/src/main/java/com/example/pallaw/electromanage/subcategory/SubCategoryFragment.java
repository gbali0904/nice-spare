package com.example.pallaw.electromanage.subcategory;


import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllSubCategory;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleSubcategory;
import com.example.pallaw.electromanage.REST.Model.SubCategoryOperation;
import com.example.pallaw.electromanage.REST.Model.resources.SubCategory;
import com.example.pallaw.electromanage.REST.Model.resources.User;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.example.pallaw.electromanage.category.SortDialogForCategory;
import com.example.pallaw.electromanage.product.SortDialog;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubCategoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener {

    public static final String TAG = SubCategoryFragment.class.getSimpleName();
    static List<SubCategory> categoryList = new ArrayList<>();
    @BindView(R.id.list)
    SuperRecyclerView list;
    @BindView(R.id.fab)
    Button fab;
    private LinearLayoutManager layoutManager;
    private SubCategoryListAdapter adapter;
    private SparseItemRemoveAnimator mSparseAnimator;
    private MenuItem searchMenuItem;
    private MenuItem sortMenuItem;
    private SearchView searchView;
    private ArrayList<SubCategory> selected_subcategoryList;

    private StringBuilder builder;
    private android.view.ActionMode mActionMode;

    private String deleteMultipsubCategory;
    private MenuItem filterMenuItem;
    private String substring;
    private List<SubCategory> categoryListForSearch=new ArrayList<>();

    public SubCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.product, menu);
        setupOptionMenu(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private boolean setupOptionMenu(Menu menu) {
        searchMenuItem = menu.findItem(R.id.searchItem);
        sortMenuItem = menu.findItem(R.id.action_sort);
        filterMenuItem = menu.findItem(R.id.action_filter);
        filterMenuItem.setVisible(true);
        if (searchMenuItem != null) {
            searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
            if (null == searchView) {
                return true;
            }
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String query) {

                    submitQuerry(query);
                    return true;
                }
            });
            searchView.setQueryHint(getString(R.string.action_search_subcategory));
            //applying the font to hint text inside of SearchView
            SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
            searchText.setHintTextColor(getContext().getResources().getColor(R.color.colorAccent));
            searchText.setTextColor(getContext().getResources().getColor(R.color.white));
            searchText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        return false;
    }

    private void submitQuerry(String query) {
//        remoteSearch(query);
        localSearch(query);
    }

    private void localSearch(String query) {
        Timber.e(query);
        ArrayList<SubCategory> searchList=new ArrayList<>();
        for (SubCategory subCategory : categoryListForSearch) {
            if (subCategory.getName().toLowerCase().contains(query)||
                    subCategory.getCategory().getName().toLowerCase().contains(query)) {
                searchList.add(subCategory);
            }
        }

        if(searchList.size()>0){//if found something related to search querry
            categoryList.clear();
            categoryList.addAll(searchList);
            adapter.notifyDataChanged();
        }
        Timber.e("querry- "+query+", search result size-"+searchList.size());
    }

    private void remoteSearch(String query) {
        DataManager.getInstance().getAllSubCategory("",query, "", "", "", new Callback<AllSubCategory>() {
            @Override
            public void onResponse(Call<AllSubCategory> call, Response<AllSubCategory> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllSubCategory.class);
                Timber.i("downloadSubCategory", responseJson);


                if (!response.body().isError() ) {
                    categoryList.clear();
                    categoryList.addAll(response.body().getCategory());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllSubCategory> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadSubCategory", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                SortDialogForCategory sortDialog = SortDialogForCategory.getInstance(new SortDialogForCategory.OnCategorySortListener() {
                    @Override
                    public void onCategorySort(Dialog dialog, String sortingType) {
                        dialog.dismiss();
                        sortListAccTo(sortingType);
                    }
                });
                sortDialog.show(getActivity().getSupportFragmentManager(), SortDialog.TAG);
                break;
            case R.id.action_filter:
                FilterDialogForCategory filterDialogForCategory = FilterDialogForCategory.getInstance(new FilterDialogForCategory.OnshortFilterListener() {
                    @Override
                    public void onCategorySort(Dialog dialog, ArrayList<String> sortingType) {
                        dialog.dismiss();
                        filterListAccTo(sortingType);
                    }
                });
                filterDialogForCategory.show(getActivity().getSupportFragmentManager(), SortDialog.TAG);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void filterListAccTo(ArrayList<String> filterType) {

        builder=new StringBuilder();
        for (String filter :
                filterType) {
            Log.e("this","filter ids are:- "+filter);
            builder.append(filter).append(",");
        }

        substring = builder.toString().substring(0, builder.length() - 1);

        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllFilterCategory(substring, new Callback<AllSubCategory>() {
            @Override
            public void onResponse(Call<AllSubCategory> call, Response<AllSubCategory> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllSubCategory.class);
                Timber.i("downloadSubCategory", responseJson);
                if (!response.body().isError()) {
                    categoryList.clear();
                    categoryList.addAll(response.body().getCategory());
                    adapter.notifyDataSetChanged();

                    categoryListForSearch.clear();
                    categoryListForSearch.addAll(categoryList);
                }
                else {
                    categoryList.clear();
                    adapter.notifyDataSetChanged();

                    categoryListForSearch.clear();
                    categoryListForSearch.addAll(categoryList);

                }
            }

            @Override
            public void onFailure(Call<AllSubCategory> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadSubCategory", t.getMessage());
                t.printStackTrace();
            }
        });

    }

    private void sortListAccTo(String sortingType) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllSubCategory("","", "", sortingType, "", new Callback<AllSubCategory>() {
            @Override
            public void onResponse(Call<AllSubCategory> call, Response<AllSubCategory> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllSubCategory.class);
                Timber.i("downloadSubCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    categoryList.clear();
                    categoryList.addAll(response.body().getCategory());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllSubCategory> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadSubCategory", t.getMessage());
                t.printStackTrace();
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);

        fab.setText("Add Sub_category");
        layoutManager = new LinearLayoutManager(getActivity());
        adapter = new SubCategoryListAdapter(categoryList, new OnSubCategoryDeleteListener() {
            @Override
            public void onSubCategoryDelete(ArrayList<SubCategory> subCategories, int adapterPosition) {
                //    removeSubCategory(mItem, adapterPosition);
                selected_subcategoryList = subCategories;
                if (selected_subcategoryList.size()>0) {
                    if (mActionMode == null)
                    {
                        mActionMode = getActivity().startActionMode(mActionModeCallback);
                    }
                }
                else {
                    if (mActionMode != null) {

                        mActionMode.finish();
                        selected_subcategoryList.clear();
                    }
                }


            }
        }, new OnSubCategoryStatusChangeListener() {
            @Override
            public void onSubCategoryStatusChange(SubCategory mItem, int adapterPosition, String status) {
                changeSubCategoryStatus(mItem, adapterPosition, status);
            }
        }, new OnSubCategoryUpdateListener() {
            @Override
            public void onSubCategoryUpdate(SubCategory mItem, int adapterPosition) {
                Intent toUpdateEmp= new Intent(getActivity(),AddSubCategoryActivity.class);
                toUpdateEmp.putExtra(AppConstants.EXTRA_UPDATE_ITEM,mItem);
                toUpdateEmp.putExtra(AppConstants.EXTRA_ADAPTER_POSITION,adapterPosition);
                startActivityForResult(toUpdateEmp,AppConstants.REQUEST_CODE_TO_UPADATE);
            }
        });
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        list.setRefreshListener(this);
        list.setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);
//        list.setupMoreListener(this, 1);
        return view;
    }

    private void changeSubCategoryStatus(SubCategory mItem, final int adapterPosition, String status) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().subcategoryOperation(mItem.getName(), mItem.getDescription(), status, mItem.getAdminid(), mItem.getCategory().getId(),AppConstants.ACTION_TYPE_UPDATE, mItem.getId(), new Callback<SubCategoryOperation>() {
            @Override
            public void onResponse(Call<SubCategoryOperation> call, Response<SubCategoryOperation> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), SubCategoryOperation.class);
                Timber.i("changeSubCategoryStatus", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    adapter.updateStatus(adapterPosition, response.body().getSubcategory().getFlag());
                }
            }

            @Override
            public void onFailure(Call<SubCategoryOperation> call, Throwable t) {
                Util.hideProgressDialog();
                t.printStackTrace();
            }
        });
    }


    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            mActionMode.getMenuInflater().inflate(R.menu.context, menu);
            return true;

        }

        // Called each time the action mode is shown.
        // Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:

                    AlertDialog.Builder dilaogbuilder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(),R.style.AlertDialogStyle);
                    } else {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
                    }
                    dilaogbuilder.setTitle("Delete Sub-Category")
                            .setCancelable(true)
                            .setMessage("Are you sure you want to delete this Sub-Category?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    builder=new StringBuilder();
                                    for (SubCategory subcategory :
                                            selected_subcategoryList) {
                                        builder.append(subcategory.getId()).append(",");
                                    }
                                    deleteMultipsubCategory= builder.toString().substring(0, builder.length() - 1);

                                    deleteSubCategory(deleteMultipsubCategory);
                                    Log.e("this","Ids are :"+builder.toString().substring(0, builder.length() - 1));
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();


                    return true;

                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    private void deleteSubCategory(final String deleteMultipsubCategory) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().deleteMultipleSubCategory(deleteMultipsubCategory, new Callback<DeleteMultipleSubcategory>() {
            @Override
            public void onResponse(Call<DeleteMultipleSubcategory> call, Response<DeleteMultipleSubcategory> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), DeleteMultipleSubcategory.class);
                Timber.i("removeCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    mActionMode.finish();
                    categoryList.clear();
                    selected_subcategoryList.clear();
                    downloadSubCategory();
                }
            }

            @Override
            public void onFailure(Call<DeleteMultipleSubcategory> call, Throwable t) {
                Timber.e("removeProduct", t.getMessage());
                t.printStackTrace();
            }
        });

    }

    private void removeSubCategory(SubCategory mItem, final int adapterPosition) {
        Toast.makeText(getActivity(), "Category removed", Toast.LENGTH_SHORT).show();
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().subcategoryOperation(mItem.getName(), mItem.getDescription(), mItem.getFlag(), mItem.getAdminid(), mItem.getCategory().getId(),AppConstants.ACTION_TYPE_DELETE, mItem.getId(), new Callback<SubCategoryOperation>() {
            @Override
            public void onResponse(Call<SubCategoryOperation> call, Response<SubCategoryOperation> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), SubCategoryOperation.class);
                Timber.i("removeSubCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    adapter.remove(adapterPosition);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<SubCategoryOperation> call, Throwable t) {
                Timber.e("removeSubCategory", t.getMessage());
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getActivity().getResources().getString(R.string.title_sub_category_list_fragment));
        downloadSubCategory();
    }

    private void downloadSubCategory() {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllSubCategory("","", "", "", "", new Callback<AllSubCategory>() {
            @Override
            public void onResponse(Call<AllSubCategory> call, Response<AllSubCategory> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllSubCategory.class);
                Timber.i("downloadSubCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    categoryList.clear();
                    categoryList.addAll(response.body().getCategory());
                    adapter.notifyDataChanged();

                    categoryListForSearch.clear();
                    categoryListForSearch.addAll(categoryList);
                }
            }

            @Override
            public void onFailure(Call<AllSubCategory> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadSubCategory", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
//        Toast.makeText(getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
        downloadSubCategory();
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        Toast.makeText(getActivity(), "More", Toast.LENGTH_SHORT).show();
        User user = new User();
        user.setFirst_name("More asked, more served");
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivityForResult(new Intent(getActivity(), AddSubCategoryActivity.class), AppConstants.REQUEST_CODE_TO_ADD);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK) {
            switch (requestCode){
                case AppConstants.REQUEST_CODE_TO_ADD:
                    categoryList.add((SubCategory) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM));
                    adapter.notifyDataSetChanged();
                    break;
                case AppConstants.REQUEST_CODE_TO_UPADATE:
                    categoryList.set(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION,0),
                            ((SubCategory) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM)));
                    adapter.notifyItemChanged(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION,0));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    interface OnSubCategoryDeleteListener {
        void onSubCategoryDelete(ArrayList<SubCategory> mItem, int adapterPosition);
    }

    interface OnSubCategoryStatusChangeListener {
        void onSubCategoryStatusChange(SubCategory mItem, int adapterPosition, String status);
    }

    interface OnSubCategoryUpdateListener {
        void onSubCategoryUpdate(SubCategory mItem, int adapterPosition);
    }
}
