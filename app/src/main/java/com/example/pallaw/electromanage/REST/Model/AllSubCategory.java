package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.SubCategory;

import java.util.List;

/**
 * Created by pallaw on 10/10/17.
 */

public class AllSubCategory {

    /**
     * error : false
     * code : 0
     * msg : List of all user fetched successfully
     * Category : [{"id":"4","name":"sony","description":"here is my sony tv","flag":"1","timestamp":"2017-10-09 16:37:20.626816","adminid":"1000","category_id":"16"}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<SubCategory> Category;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<SubCategory> getCategory() {
        return Category;
    }

    public void setCategory(List<SubCategory> Category) {
        this.Category = Category;
    }

}
