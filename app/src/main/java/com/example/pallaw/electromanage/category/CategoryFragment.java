package com.example.pallaw.electromanage.category;


import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllCategories;
import com.example.pallaw.electromanage.REST.Model.CategoryOperation;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleCategory;
import com.example.pallaw.electromanage.REST.Model.resources.Category;
import com.example.pallaw.electromanage.REST.Model.resources.User;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.MainActivityFragment;
import com.example.pallaw.electromanage.Utils.Util;
import com.example.pallaw.electromanage.product.SortDialog;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener {

    public static final String TAG = CategoryFragment.class.getSimpleName();
    static List<Category> categoryList = new ArrayList<>();
    @BindView(R.id.delete)
    TextView delete;
    @BindView(R.id.lay)
    LinearLayout lay;
    @BindView(R.id.list)
    SuperRecyclerView list;
    @BindView(R.id.fab)
    Button fab;
    @BindView(R.id.transparentOverlay)
    View transparentOverlay;

    private LinearLayoutManager layoutManager;
    private CategoryListAdapter adapter;
    private SparseItemRemoveAnimator mSparseAnimator;
    private MenuItem searchMenuItem;
    private SearchView searchView;
    private MenuItem sortMenuItem;
    private ArrayList<Category> selected_categoryList;

    private StringBuilder builder;
    private ActionMode mActionMode;

    private String deleteMultipCategory;
    private List<Category> categoryListForSearch = new ArrayList<>();
    private ActionMode mActionUpdate;
    private HashMap<String, ModelForUpdateCategory> updated_categories;
    ArrayList<String> updated_strings=new ArrayList<>();
    private ArrayList<ModelForUpdateCategory> updatebuilder;
    private String updateMultipCategory;
    private boolean referesh_status=true;


    public CategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.product, menu);
        setupOptionMenu(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private boolean setupOptionMenu(Menu menu) {
        searchMenuItem = menu.findItem(R.id.searchItem);
        sortMenuItem = menu.findItem(R.id.action_sort);

        if (searchMenuItem != null) {
            searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
            if (null == searchView) {
                return true;
            }
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String query) {

                    submitQuerry(query);
                    return true;
                }
            });


            searchView.setQueryHint(getString(R.string.action_search_category));
            //applying the font to hint text inside of SearchView
            SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
            searchText.setHintTextColor(getContext().getResources().getColor(R.color.colorAccent));
            searchText.setTextColor(getContext().getResources().getColor(R.color.white));
            searchText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        return false;
    }

    private void submitQuerry(String query) {

//        remoteSearch(query);
        localSearch(query);
    }

    private void localSearch(String query) {
        Timber.e(query);
        ArrayList<Category> searchList = new ArrayList<>();
        for (Category category : categoryListForSearch) {
            if (category.getName().toLowerCase().contains(query) ||
                    category.getDescription().toLowerCase().contains(query)) {
                searchList.add(category);
            }
        }

        if (searchList.size() > 0) {//if found something related to search querry
            categoryList.clear();
            categoryList.addAll(searchList);
            adapter.notifyDataChanged();
        }
        Timber.e("querry- " + query + ", search result size-" + searchList.size());
    }

    private void remoteSearch(String query) {
        DataManager.getInstance().getAllCategories("", query, "", "", "", "", new Callback<AllCategories>() {
            @Override
            public void onResponse(Call<AllCategories> call, Response<AllCategories> response) {
                String responseJson = new Gson().toJson(response.body(), AllCategories.class);
                Timber.i("downloadCategory", responseJson);

                if (!response.body().isError()) {
                    categoryList.clear();
                    categoryList.addAll(response.body().getCategory());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllCategories> call, Throwable t) {
                Timber.e("downloadCategory", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                SortDialogForCategory sortDialog = SortDialogForCategory.getInstance(new SortDialogForCategory.OnCategorySortListener() {
                    @Override
                    public void onCategorySort(Dialog dialog, String sortingType) {
                        dialog.dismiss();


                        sortListAccTo(sortingType);
                    }
                });
                sortDialog.show(getActivity().getSupportFragmentManager(), SortDialog.TAG);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortListAccTo(String sortingType) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllCategories("", "", "", "", "", sortingType, new Callback<AllCategories>() {
            @Override
            public void onResponse(Call<AllCategories> call, Response<AllCategories> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllCategories.class);
                Timber.i("downloadCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    categoryList.clear();
                    categoryList.addAll(response.body().getCategory());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllCategories> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadCategory", t.getMessage());
                t.printStackTrace();
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);

        fab.setText("Add Category");
        layoutManager = new LinearLayoutManager(getActivity());
        adapter = new CategoryListAdapter(categoryList, new OnCategoryDeleteListener() {
            @Override
            public void onCategoryDelete(ArrayList<Category> category, int adapterPosition) {
                // removeCategory(mItem, adapterPosition);

                selected_categoryList = category;
                if (selected_categoryList.size() > 0) {
                    if (mActionMode == null) {
                        mActionMode = getActivity().startActionMode(mActionModeCallback);
                    }
                } else {
                    if (mActionMode != null) {
                        mActionMode.finish();
                        selected_categoryList.clear();
                    }
                }
            }
        }, new OnCategoryStatusChangeListener() {
            @Override
            public void onCategoryStatusChange(Category mItem, int adapterPosition, String status) {
                changeCategoryStatus(mItem, adapterPosition, status);
            }
        }, new OnCategoryUpdateListener() {
            @Override
            public void onCategoryUpdate(Category mItem, int adapterPosition) {
                /*Intent toUpdateEmp= new Intent(getActivity(),AddCategoryActivity.class);
                toUpdateEmp.putExtra(AppConstants.EXTRA_UPDATE_ITEM,mItem);
                toUpdateEmp.putExtra(AppConstants.EXTRA_ADAPTER_POSITION,adapterPosition);
                startActivityForResult(toUpdateEmp,AppConstants.REQUEST_CODE_TO_UPADATE);*/


                //
                UpdateCategory updateCategory=new UpdateCategory();
                Bundle toUpdateEmp=new Bundle();
                toUpdateEmp.putParcelable(AppConstants.EXTRA_UPDATE_ITEM,mItem);
                updateCategory.setArguments(toUpdateEmp);
                MainActivityFragment.instansiate(updateCategory, getActivity(), UpdateCategory.TAG, "Update Category");

            }
        });

        UpdateCategory.setOnUpdateListener(new UpdateCategory.OnUpdateListener() {
            @Override
            public void onUpdate(HashMap<String, ModelForUpdateCategory> hashMaps) {
           updated_categories=     hashMaps;
           referesh_status=false;
                adapter.setStatus(true);
                if (mActionUpdate == null)
                {
                    mActionUpdate = getActivity().startActionMode(mActionUpdateCallback);
                }
            }
        });



        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        list.setRefreshListener(this);
        list.setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);
//        list.setupMoreListener(this, 1);
        return view;
    }



    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            mActionMode.getMenuInflater().inflate(R.menu.context, menu);
            return true;

        }

        // Called each time the action mode is shown.
        // Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    AlertDialog.Builder dilaogbuilder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
                    } else {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
                    }
                    dilaogbuilder.setTitle("Delete Category")
                            .setCancelable(true)
                            .setMessage("Are you sure you want to delete this Category?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    builder = new StringBuilder();
                                    for (Category category :
                                            selected_categoryList) {
                                        builder.append(category.getId()).append(",");
                                    }
                                    deleteMultipCategory = builder.toString().substring(0, builder.length() - 1);

                                    deleteCategory(deleteMultipCategory);
                                    Log.e("this", "Ids are :" + builder.toString().substring(0, builder.length() - 1));
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();


                    return true;




                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };
    private ActionMode.Callback mActionUpdateCallback = new ActionMode.Callback() {
        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionUpdate = mode;
            mActionUpdate.getMenuInflater().inflate(R.menu.context_update, menu);
            return true;

        }

        // Called each time the action mode is shown.
        // Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_update:

                    updatebuilder=new ArrayList<>();

                    AlertDialog.Builder dilaogbuilder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(),R.style.AlertDialogStyle);
                    } else {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
                    }
                    dilaogbuilder.setTitle("Update Category")
                            .setCancelable(true)
                            .setMessage("Are you sure you want to update  Category?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    Set<String> keys = updated_categories.keySet();
                                    for (String key : keys) {
                                        updatebuilder.add(updated_categories.get(key));
                                    }
                                    Gson gson = new Gson();
                                    updateMultipCategory  = gson.toJson(updatebuilder);
                                    updateCategory(updateMultipCategory);

                                    Log.e("this","json Values are :"+updateMultipCategory);
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                    return true;

                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionUpdate = null;
        }
    };

    private void updateCategory(String updateMultipCategory) {
        Util.showProgressDialog(getActivity());
        referesh_status=true;
        adapter.setStatus(false);
        DataManager.getInstance().categoryUpdateOperations( updateMultipCategory, new Callback<CategoryOperation>() {
            @Override
            public void onResponse(Call<CategoryOperation> call, Response<CategoryOperation> response) {
                Util.hideProgressDialog();
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                if (!(response.body().isError())) {
                    mActionUpdate.finish();
                    downloadCategory();
                }

            }

            @Override
            public void onFailure(Call<CategoryOperation> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("removeProduct", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    private void deleteCategory(final String deleteMultipCategory) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().deleteMultipleCategory(deleteMultipCategory, new Callback<DeleteMultipleCategory>() {
            @Override
            public void onResponse(Call<DeleteMultipleCategory> call, Response<DeleteMultipleCategory> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), DeleteMultipleCategory.class);
                Timber.i("removeCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    mActionMode.finish();
                    categoryList.clear();
                    selected_categoryList.clear();
                    downloadCategory();
                }
            }

            @Override
            public void onFailure(Call<DeleteMultipleCategory> call, Throwable t) {
                Timber.e("removeProduct", t.getMessage());
                t.printStackTrace();
            }
        });

    }

    private void changeCategoryStatus(Category mItem, final int adapterPosition, String status) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().categoryOperations(mItem.getName(), mItem.getDescription(), status, mItem.getAdminid(), AppConstants.ACTION_TYPE_UPDATE, mItem.getId(), new Callback<CategoryOperation>() {
            @Override
            public void onResponse(Call<CategoryOperation> call, Response<CategoryOperation> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), CategoryOperation.class);
                Timber.i("updateCategoryStatus", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    adapter.updateStatus(adapterPosition, response.body().getCategory().getFlag());
                }
            }
            @Override
            public void onFailure(Call<CategoryOperation> call, Throwable t) {
                Util.hideProgressDialog();
                t.printStackTrace();
            }
        });
    }

    private void removeCategory(Category mItem, final int adapterPosition) {
        Toast.makeText(getActivity(), "Category removed", Toast.LENGTH_SHORT).show();
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().categoryOperations(mItem.getName(), mItem.getDescription(), mItem.getFlag(), mItem.getAdminid(), AppConstants.ACTION_TYPE_DELETE, mItem.getId(), new Callback<CategoryOperation>() {
            @Override
            public void onResponse(Call<CategoryOperation> call, Response<CategoryOperation> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), CategoryOperation.class);
                Timber.i("removeCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    adapter.remove(adapterPosition);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<CategoryOperation> call, Throwable t) {
                Timber.e("removeCategory", t.getMessage());
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        if (referesh_status) {
            getActivity().setTitle(getActivity().getResources().getString(R.string.title_category_list_fragment));
            downloadCategory();
        }
    }

    private void downloadCategory() {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllCategories("", "", "", "", "", "", new Callback<AllCategories>() {
            @Override
            public void onResponse(Call<AllCategories> call, Response<AllCategories> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllCategories.class);
                Timber.i("downloadCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    categoryList.clear();
                    categoryList.addAll(response.body().getCategory());
                    adapter.notifyDataChanged();

                    categoryListForSearch.clear();
                    categoryListForSearch.addAll(categoryList);
                }
            }

            @Override
            public void onFailure(Call<AllCategories> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadCategory", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
//        Toast.makeText(getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
        if (referesh_status) {
            downloadCategory();
        }
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        Toast.makeText(getActivity(), "More", Toast.LENGTH_SHORT).show();
        User user = new User();
        user.setFirst_name("More asked, more served");
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivityForResult(new Intent(getActivity(), AddCategoryActivity.class), AppConstants.REQUEST_CODE_TO_ADD);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstants.REQUEST_CODE_TO_ADD:
                    categoryList.add((Category) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM));
                    adapter.notifyDataSetChanged();
                    break;
                case AppConstants.REQUEST_CODE_TO_UPADATE:
                    categoryList.set(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION, 0),
                            ((Category) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM)));
                    adapter.notifyItemChanged(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION, 0));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }


    interface OnCategoryDeleteListener {
        void onCategoryDelete(ArrayList<Category> mItem, int adapterPosition);
    }

    interface OnCategoryStatusChangeListener {
        void onCategoryStatusChange(Category mItem, int adapterPosition, String status);
    }

    interface OnCategoryUpdateListener {
        void onCategoryUpdate(Category mItem, int adapterPosition);
    }
}
