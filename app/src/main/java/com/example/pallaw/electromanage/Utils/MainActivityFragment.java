package com.example.pallaw.electromanage.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivityFragment extends AppCompatActivity {


    private static Fragment fragment;
    private static Context context;
    private static String tag;
    private static String status;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.frameLay)
    FrameLayout frameLay;
    @BindView(R.id.fram_dialog)
    RelativeLayout framDialog;

    public static void instansiate(Fragment fragment, Context context, String tag, String status) {
        MainActivityFragment.fragment = fragment;
        MainActivityFragment.context = context;
        MainActivityFragment.tag = tag;
        MainActivityFragment.status = status;
        context.startActivity(new Intent(context, MainActivityFragment.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fragment);
        ButterKnife.bind(this);
        setToolBarTitle(status);
        FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.fram_dialog, fragment, false, tag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolBarTitle(String profile) {
        title.setText(profile);
    }

    @OnClick({R.id.back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                super.onBackPressed();
                break;
        }
    }
}
