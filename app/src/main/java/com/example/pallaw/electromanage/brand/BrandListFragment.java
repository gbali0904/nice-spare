package com.example.pallaw.electromanage.brand;


import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllBrands;
import com.example.pallaw.electromanage.REST.Model.BrandManage;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleBrand;
import com.example.pallaw.electromanage.REST.Model.resources.Brand;
import com.example.pallaw.electromanage.REST.Model.resources.User;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.example.pallaw.electromanage.category.SortDialogForCategory;
import com.example.pallaw.electromanage.product.SortDialog;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class BrandListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener {

    public static final String TAG = BrandListFragment.class.getSimpleName();
    static List<Brand> brandList = new ArrayList<>();
  @BindView(R.id.list)
    SuperRecyclerView list;
  @BindView(R.id.fab)
    Button fab;
    private LinearLayoutManager layoutManager;
    private BrandListAdapter adapter;
    private SparseItemRemoveAnimator mSparseAnimator;
    private MenuItem searchMenuItem;
    private MenuItem sortMenuItem;
    private SearchView searchView;
    private ArrayList<Brand> selected_brandList;

    private StringBuilder builder;
    private android.view.ActionMode mActionMode;

    private String deleteMultipbrand;
    private List<Brand> brandListForSearch=new ArrayList<>();

    public BrandListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.product, menu);
        setupOptionMenu(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private boolean setupOptionMenu(Menu menu) {
        searchMenuItem = menu.findItem(R.id.searchItem);
        sortMenuItem = menu.findItem(R.id.action_sort);

        if (searchMenuItem != null) {
            searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
            if (null == searchView) {
                return true;
            }
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String query) {

                    submitQuerry(query);
                    return true;
                }
            });
            searchView.setQueryHint(getString(R.string.action_search_brand));
            //applying the font to hint text inside of SearchView
            SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
            searchText.setHintTextColor(getContext().getResources().getColor(R.color.colorAccent));
            searchText.setTextColor(getContext().getResources().getColor(R.color.white));
            searchText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        return false;
    }

    private void submitQuerry(String query) {
//        remoteSearch(query);
        localSearch(query);
    }

    private void localSearch(String query) {
        Timber.e(query);
        ArrayList<Brand> searchList=new ArrayList<>();
        for (Brand brand : brandList) {
            if (brand.getName().toLowerCase().contains(query)) {
                searchList.add(brand);
            }
        }

        if(searchList.size()>0){//if found something related to search querry
            brandList.clear();
            brandList.addAll(searchList);
            adapter.notifyDataChanged();
        }
        Timber.e("querry- "+query+", search result size-"+searchList.size());
    }

    private void remoteSearch(String query) {
        DataManager.getInstance().getAllBrands("", query, "", "", new Callback<AllBrands>() {
            @Override
            public void onResponse(Call<AllBrands> call, Response<AllBrands> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllBrands.class);
                Timber.i("downloadBrands", responseJson);

                if (!response.body().isError()) {
                    brandList.clear();
                    brandList.addAll(response.body().getBrands());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllBrands> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadBrands", t.getMessage());
                t.printStackTrace();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                SortDialogForCategory sortDialog = SortDialogForCategory.getInstance(new SortDialogForCategory.OnCategorySortListener() {
                    @Override
                    public void onCategorySort(Dialog dialog, String sortingType) {
                        dialog.dismiss();
                        sortListAccTo(sortingType);
                    }
                });
                sortDialog.show(getActivity().getSupportFragmentManager(), SortDialog.TAG);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sortListAccTo(String sortingType) {
        Util.showProgressDialog(getActivity());

        DataManager.getInstance().getAllBrands("", "", "", sortingType, new Callback<AllBrands>() {
            @Override
            public void onResponse(Call<AllBrands> call, Response<AllBrands> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllBrands.class);
                Timber.i("downloadBrands", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    brandList.clear();
                    brandList.addAll(response.body().getBrands());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllBrands> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadBrands", t.getMessage());
                t.printStackTrace();
            }
        });

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);

        fab.setText("Add Brand");
        layoutManager = new LinearLayoutManager(getActivity());
        adapter= new BrandListAdapter(brandList, new OnBrandDeleteListener() {
            @Override
            public void onBrandDelete(ArrayList<Brand> brand, int adapterPosition) {
            //    removeBrand(mItem, adapterPosition);

                selected_brandList = brand;
                if (selected_brandList.size()>0) {
                    if (mActionMode == null)
                    {
                        mActionMode = getActivity().startActionMode(mActionModeCallback);
                    }
                }
                else {
                    if (mActionMode != null) {
                        mActionMode.finish();
                        selected_brandList.clear();
                    }
                }

            }
        }, new OnBrandStatusChangeListener() {
            @Override
            public void onBrandStatusChange(Brand mItem, int adapterPosition, String status) {
                changeBrandStatus(mItem, adapterPosition, status);
            }
        }, new OnBrandUpdateListener() {
            @Override
            public void onBrandUpdate(Brand mItem, int adapterPosition) {
                Intent toUpdateEmp= new Intent(getActivity(),AddBrandActivity.class);
                toUpdateEmp.putExtra(AppConstants.EXTRA_UPDATE_ITEM,mItem);
                toUpdateEmp.putExtra(AppConstants.EXTRA_ADAPTER_POSITION,adapterPosition);
                startActivityForResult(toUpdateEmp,AppConstants.REQUEST_CODE_TO_UPADATE);
            }
        });
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        list.setRefreshListener(this);
        list.setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);
//        list.setupMoreListener(this, 1);
        return view;
    }

    private void changeBrandStatus(Brand mItem, final int adapterPosition, String status) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().brandManage(mItem.getName(), mItem.getDescription(), status, mItem.getAdmin_id(), AppConstants.ACTION_TYPE_UPDATE, mItem.getId(), new Callback<BrandManage>() {
            @Override
            public void onResponse(Call<BrandManage> call, Response<BrandManage> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), BrandManage.class);
                Timber.i("changeBrandStatus",responseJson);
                Toast.makeText(getActivity(), ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if(!(response.body().isError())){
                    adapter.updateStatus(adapterPosition,response.body().getBrand().getFlag());
                }
            }

            @Override
            public void onFailure(Call<BrandManage> call, Throwable t) {
                Util.hideProgressDialog();
                t.printStackTrace();
            }
        });
    }



    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            mActionMode.getMenuInflater().inflate(R.menu.context, menu);
            return true;

        }

        // Called each time the action mode is shown.
        // Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:

                    AlertDialog.Builder dilaogbuilder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(),R.style.AlertDialogStyle);
                    } else {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
                    }
                    dilaogbuilder.setTitle("Delete Brand")
                            .setCancelable(true)
                            .setMessage("Are you sure you want to delete this Brand?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    builder=new StringBuilder();
                                    for (Brand brand :
                                            selected_brandList) {
                                        builder.append(brand.getId()).append(",");
                                    }
                                    deleteMultipbrand = builder.toString().substring(0, builder.length() - 1);

                                    deleteBrand(deleteMultipbrand);
                                    Log.e("this","Ids are :"+builder.toString().substring(0, builder.length() - 1));
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();


                    return true;

                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };

    private void deleteBrand(final String deleteMultiplebrand) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().deleteMultipleBrand(deleteMultiplebrand, new Callback<DeleteMultipleBrand>() {
            @Override
            public void onResponse(Call<DeleteMultipleBrand> call, Response<DeleteMultipleBrand> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), DeleteMultipleBrand.class);
                Timber.i("removeCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    mActionMode.finish();
                    brandList.clear();
                    selected_brandList.clear();
                    downloadBrands();
                }
            }

            @Override
            public void onFailure(Call<DeleteMultipleBrand> call, Throwable t) {
                Timber.e("removeProduct", t.getMessage());
                t.printStackTrace();
            }
        });

    }


    private void removeBrand(Brand mItem, final int adapterPosition) {
        Toast.makeText(getActivity(), "Category removed", Toast.LENGTH_SHORT).show();
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().brandManage(mItem.getName(), mItem.getDescription(), "", mItem.getAdmin_id(), AppConstants.ACTION_TYPE_DELETE, mItem.getId(), new Callback<BrandManage>() {
            @Override
            public void onResponse(Call<BrandManage> call, Response<BrandManage> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), BrandManage.class);
                Timber.i("removeBrand", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if(!(response.body().isError())){
                    adapter.remove(adapterPosition);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<BrandManage> call, Throwable t) {
                Timber.e("removeBrand",t.getMessage());
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getActivity().getResources().getString(R.string.title_brands_list_fragment));

        downloadBrands();


    }

    private void downloadBrands() {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllBrands("", "", "", "", new Callback<AllBrands>() {
            @Override
            public void onResponse(Call<AllBrands> call, Response<AllBrands> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllBrands.class);
                Timber.i("downloadBrands", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    brandList.clear();
                    brandList.addAll(response.body().getBrands());
                    adapter.notifyDataChanged();

                    brandListForSearch.clear();
                    brandListForSearch.addAll(brandList);
                }
            }

            @Override
            public void onFailure(Call<AllBrands> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadBrands", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
//        Toast.makeText(getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
        downloadBrands();
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        Toast.makeText(getActivity(), "More", Toast.LENGTH_SHORT).show();
        User user = new User();
        user.setFirst_name("More asked, more served");
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivityForResult(new Intent(getActivity(), AddBrandActivity.class), AppConstants.REQUEST_CODE_TO_ADD);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK) {
            switch (requestCode){
                case AppConstants.REQUEST_CODE_TO_ADD:
                    brandList.add((Brand) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM));
                    adapter.notifyDataSetChanged();
                    break;
                case AppConstants.REQUEST_CODE_TO_UPADATE:
                    brandList.set(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION,0),
                            ((Brand) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM)));
                    adapter.notifyItemChanged(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION,0));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    interface OnBrandDeleteListener {
        void onBrandDelete(ArrayList<Brand> mItem, int adapterPosition);
    }

    interface OnBrandStatusChangeListener {
        void onBrandStatusChange(Brand mItem, int adapterPosition, String status);
    }

    interface OnBrandUpdateListener {
        void onBrandUpdate(Brand mItem, int adapterPosition);
    }
}
