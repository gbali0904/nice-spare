package com.example.pallaw.electromanage.productmodel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllBrands;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.ProductModelManage;
import com.example.pallaw.electromanage.REST.Model.resources.Brand;
import com.example.pallaw.electromanage.REST.Model.resources.ProductModel;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AddProductModelActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtAddCatName)
    EditText edtAddCatName;
    @BindView(R.id.btnAddCat)
    Button btnAddCat;
    @BindView(R.id.btnUpdateCat)
    Button btnUpdateCat;
    @BindView(R.id.textInputLayout3)
    TextInputLayout textInputLayout3;
    @BindView(R.id.spiAddProductBrand)
    Spinner spiAddProductBrand;
    @BindView(R.id.lay)
    LinearLayout lay;
    private String name;
    private ProductModel productModel;
    private int update_position;
    private List<Brand> brands;
    private Brand selectedBrand;
    private String brand_id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_brand);
        ButterKnife.bind(this);
        lay.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        downloadBrands();

        if (getIntent().getExtras() != null) {
            productModel = getIntent().getExtras().getParcelable(AppConstants.EXTRA_UPDATE_ITEM);
            update_position = getIntent().getExtras().getInt(AppConstants.EXTRA_ADAPTER_POSITION);
            if (productModel != null) {
                setTitle(getResources().getString(R.string.title_update_product_model));
                updateFields();
                return;
            }
        }
        setTitle(getResources().getString(R.string.title_add_product_model));
    }

    private void updateFields() {
        btnAddCat.setVisibility(View.GONE);
        btnUpdateCat.setVisibility(View.VISIBLE);

        edtAddCatName.setText(String.format("%s", productModel.getName()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    /*
    * Brand spinner stuff
    * */
    private void downloadBrands() {
        Util.showProgressDialog(AddProductModelActivity.this);
        DataManager.getInstance().getAllBrands("", "", AppConstants.STATUS_ACTIVE, "", new Callback<AllBrands>() {
            @Override
            public void onResponse(Call<AllBrands> call, Response<AllBrands> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllBrands.class);
                Timber.i("downloadBrands", responseJson);
                if (!response.body().isError()) {
                    Util.hideProgressDialog();
                    brands = response.body().getBrands();
                    spiAddProductBrand.setAdapter(new BrandCategoryAdapter(AddProductModelActivity.this, R.layout.item_spinner, brands));

                    if (productModel != null)
                    {
                        for(Brand brand:brands){
                            if (brand.getId().equalsIgnoreCase(productModel.getBrand().getId())){
                                spiAddProductBrand.setSelection(brands.indexOf(brand));
                            }
                        }
                    }

                    spiAddProductBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            selectedBrand = brands.get(position);
                            brand_id = selectedBrand.getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                            brand_id="";
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<AllBrands> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadBrands", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    private boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(name)) {
            isValid = false;
            edtAddCatName.setError(getResources().getString(R.string.error_brand_name));
        }


        return isValid;
    }

    @OnClick({R.id.btnAddCat, R.id.btnUpdateCat})
    public void onViewClicked(View view) {
        name = edtAddCatName.getText().toString().trim();

        switch (view.getId()) {
            case R.id.btnAddCat:
                if (validation()) {
                    Util.showProgressDialog(AddProductModelActivity.this);
                    DataManager.getInstance().productModelManage(name, "", "0", brand_id, Authentication.getInstance().getData().getId(), AppConstants.ACTION_TYPE_ADD, "", new Callback<ProductModelManage>() {
                        @Override
                        public void onResponse(Call<ProductModelManage> call, Response<ProductModelManage> response) {
                            Util.hideProgressDialog();
                            String responseJson = new Gson().toJson(response.body());
                            Timber.i("Add product model", responseJson);
                            Toast.makeText(AddProductModelActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            if (!(response.body().isError())) {
                                Intent intent = new Intent();
                                intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getModel());
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<ProductModelManage> call, Throwable t) {

                        }
                    });

                }
                break;
            case R.id.btnUpdateCat:
                Util.showProgressDialog(AddProductModelActivity.this);
                DataManager.getInstance().productModelManage(name, "", productModel.getFlag(), brand_id, Authentication.getInstance().getData().getId(), AppConstants.ACTION_TYPE_UPDATE, productModel.getId(), new Callback<ProductModelManage>() {
                    @Override
                    public void onResponse(Call<ProductModelManage> call, Response<ProductModelManage> response) {
                        Util.hideProgressDialog();
                        String responseJson = new Gson().toJson(response.body());
                        Timber.i("Add product model", responseJson);
                        Toast.makeText(AddProductModelActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        if (!(response.body().isError())) {
                            Intent intent = new Intent();
                            intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getModel());
                            intent.putExtra(AppConstants.EXTRA_ADAPTER_POSITION, update_position);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductModelManage> call, Throwable t) {

                    }
                });
                break;
        }
    }


    public class BrandCategoryAdapter extends ArrayAdapter<Brand> {

        private final List<Brand> brands;

        public BrandCategoryAdapter(Context context, int textViewResourceId, List<Brand> objects) {
            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
            this.brands = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.item_spinner, parent, false);
            TextView label = (TextView) row.findViewById(R.id.txtItemSpinnerCategory);
            label.setText(brands.get(position).getName());
            return row;
        }
    }

}
