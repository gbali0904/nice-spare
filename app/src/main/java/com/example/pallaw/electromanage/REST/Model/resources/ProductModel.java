package com.example.pallaw.electromanage.REST.Model.resources;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pallaw on 12/10/17.
 */

public class ProductModel implements Parcelable {


    /**
     * id : 2
     * name : Y20TVB
     * Description : model name
     * flag : 1
     * admin_id : 1000
     * time_stamp : 2017-10-11 07:06:05.274428
     */

    private String id;
    private String name;
    private String Description;
    private String flag;
    private String admin_id;
    private String time_stamp;
    private Brand brand;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(String time_stamp) {
        this.time_stamp = time_stamp;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.Description);
        dest.writeString(this.flag);
        dest.writeString(this.admin_id);
        dest.writeString(this.time_stamp);
        dest.writeParcelable(this.brand, flags);
    }

    public ProductModel() {
    }

    protected ProductModel(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.Description = in.readString();
        this.flag = in.readString();
        this.admin_id = in.readString();
        this.time_stamp = in.readString();
        this.brand = in.readParcelable(Brand.class.getClassLoader());
    }

    public static final Parcelable.Creator<ProductModel> CREATOR = new Parcelable.Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel source) {
            return new ProductModel(source);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };
}
