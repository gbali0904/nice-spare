package com.example.pallaw.electromanage.category;

import java.util.List;

public class ModelForUpdateCategory {
        /**
         * id : 1
         * name : Camera
         * Description :
         * flag : 1
         * timestamp : 2017-12-27 10:38:59.018496
         * adminid : 1002
         */


        private String id;
    private String name;
    private String Description;
    private String flag;
    private String timestamp;
    private String adminid;



    private String actionTypeUpdate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }


    public String getAdminid() {
        return adminid;
    }

    public void setAdminid(String adminid) {
        this.adminid = adminid;
    }
    public String getActionTypeUpdate() {
        return actionTypeUpdate;
    }

    public void setActionTypeUpdate(String actionTypeUpdate) {
        this.actionTypeUpdate = actionTypeUpdate;
    }


}
