package com.example.pallaw.electromanage.product;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;
import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.thoughtbot.expandablecheckrecyclerview.viewholders.CheckableChildViewHolder;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FilterForProduct extends CheckableChildRecyclerViewAdapter<GenreViewHolder, FilterForProduct.MultiCheckArtistViewHolder> {

    private ArrayList<String> selected_category;
    private List<String> selected_List_brands = new ArrayList<>();
    private List<String> selected_List_models = new ArrayList<>();
    private HashMap<String, List<String>> selected = new HashMap<>();
    private CheckedTextView check;
    private List<String> selected_List_category = new ArrayList<>();
    private List<String> selected_List_subcategory = new ArrayList<>();

    public FilterForProduct(List<MultiCheckGenre> groups) {
        super(groups);
    }

    @Override
    public MultiCheckArtistViewHolder onCreateCheckChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_multicheck_artist, parent, false);
        check = (CheckedTextView) view.findViewById(R.id.list_item_name);
        return new MultiCheckArtistViewHolder(view);
    }

    @Override
    public void onBindCheckChildViewHolder(MultiCheckArtistViewHolder holder, int position,
                                           CheckedExpandableGroup group, int childIndex) {
        final FilterList artist = (FilterList) group.getItems().get(childIndex);
        holder.setFilterName(artist.getName(), artist.getId(), artist.getFiltertype());

    }

    @Override
    public GenreViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_genre, parent, false);
        return new GenreViewHolder(view);
    }

    @Override
    public void onBindGroupViewHolder(GenreViewHolder holder, int flatPosition,
                                      ExpandableGroup group) {
        holder.setGenreTitle(group);
    }


    public class MultiCheckArtistViewHolder extends CheckableChildViewHolder {

        private TextView name;
        private CheckedTextView check;
        private String id;
        private String filtertype;

        public MultiCheckArtistViewHolder(View itemView) {
            super(itemView);
            check = (CheckedTextView) itemView.findViewById(R.id.list_item_name);
        }

        @Override
        public Checkable getCheckable() {
            return check;
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            FilterParams instance = FilterParams.getInstance();
            switch (filtertype) {
                case "brands":
                    FilterParams.BrandsBean brandObj = new FilterParams.BrandsBean();
                    brandObj.setId(id);
                    if (check.isChecked()) {
                        instance.getBrands().add(brandObj);
                    } else {
                        instance.getBrands().remove(brandObj);
                    }
                    break;
                case "models":
                    FilterParams.ModelsBean modelObj = new FilterParams.ModelsBean();
                    modelObj.setId(id);
                    if (check.isChecked()) {
                        instance.getModels().add(modelObj);
                    } else {
                        instance.getModels().remove(modelObj);
                    }
                    break;
                case "category":
                    FilterParams.CategoryBean categoryBean = new FilterParams.CategoryBean();
                    categoryBean.setId(id);
                    if (check.isChecked()) {
                        instance.getCategory().add(categoryBean);
                    } else {
                        instance.getCategory().remove(categoryBean);
                    }
                    break;
                case "subcategory":
                    FilterParams.SubcategoryBean subcategoryBean = new FilterParams.SubcategoryBean();
                    subcategoryBean.setId(id);
                    if (check.isChecked()) {
                        instance.getSubcategory().add(subcategoryBean);
                    } else {
                        instance.getSubcategory().remove(subcategoryBean);
                    }
                    break;
            }

        }

        public void setFilterName(String artistName, String id, String filtertype) {

            this.id = id;
            this.filtertype = filtertype;
            check.setText(artistName);
        }
    }
}
