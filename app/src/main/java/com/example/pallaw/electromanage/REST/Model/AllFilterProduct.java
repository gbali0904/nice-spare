package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.Product;

import java.util.List;

/**
 * Created by pallaw on 19/11/17.
 */

public class AllFilterProduct {

    /**
     * error : false
     * code : 0
     * msg : List of all Product fetched successfully
     * products : [{"id":"54","Description":"","flag":"1","product_name":"Sony DSC-H300 Point & Shoot Camera  (Black)","stock_status":"n","timestamp":"2017-11-18 05:36:47.906673","unit_flag":"99","price":"7000","admin_id":"1000","brand":{"id":"22","name":"Sony"},"model":{"id":"11","name":"LED - Iphone 4S"},"category":{"id":"","name":""},"sub_category":{"id":"","name":""}},{"id":"55","Description":"","flag":"1","product_name":"Canon PowerShot SX430 IS Point and Shoot Camera  (Black 20 MP)","stock_status":"n","timestamp":"2017-11-18 05:37:00.447207","unit_flag":"99","price":"700","admin_id":"1000","brand":{"id":"20","name":"Apple"},"model":{"id":"21","name":"zuke"},"category":{"id":"54","name":"camera"},"sub_category":{"id":"","name":""}}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<Product> products;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

}
