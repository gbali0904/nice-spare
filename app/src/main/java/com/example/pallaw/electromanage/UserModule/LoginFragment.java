package com.example.pallaw.electromanage.UserModule;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.Utils.UserManager;
import com.example.pallaw.electromanage.Utils.Util;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();
    @BindView(R.id.edtLoginUsername)
    EditText edtLoginUsername;
    @BindView(R.id.edtLoginPassword)
    EditText edtLoginPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    Unbinder unbinder;
    private String username;
    private String password;

    public LoginFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);

        Log.i("login", "here we come");
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    @OnClick({R.id.btnLoginForgotPassword, R.id.btnLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLoginForgotPassword:
                break;
            case R.id.btnLogin:
                if (validation()) {
                    login();
                }
                break;
        }
    }

    private void login() {
        Log.i("login", "here we come111");

        Util.showProgressDialog(getActivity());
        DataManager.getInstance().login(username, password, new Callback<Authentication>() {
            @Override
            public void onResponse(Call<Authentication> call, Response<Authentication> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), Authentication.class);
                Timber.i("login", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                if (!(response.body().isError())) {
                    UserManager.getInstance().logoutUser();
                    UserManager.getInstance().createLoginSession(response.body());
                }
            }

            @Override
            public void onFailure(Call<Authentication> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("Login", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    private boolean validation() {
        boolean isValid = true;

        username = edtLoginUsername.getText().toString().trim();
        password = edtLoginPassword.getText().toString().trim();

        if (TextUtils.isEmpty(username)) {
            edtLoginUsername.setError(getResources().getString(R.string.error_mobile));
            isValid = false;
        }

        if (TextUtils.isEmpty(password)) {
            edtLoginPassword.setError(getResources().getString(R.string.error_password));
            isValid = false;
        }

        return isValid;
    }
}
