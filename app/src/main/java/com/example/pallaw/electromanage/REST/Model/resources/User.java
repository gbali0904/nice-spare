package com.example.pallaw.electromanage.REST.Model.resources;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pallaw on 04/10/17.
 */

public class User implements Parcelable {


    /**
     * id : 1000
     * first_name : Gunjan
     * middel_name :
     * last_name : Bali
     * mobile_no : 7696343553
     * email_id : gbali0904@gmail.com
     * flag : 1
     * Timestamp : 2017-10-06 05:44:19.737867
     * password : j_CEf=By
     * admin_id : 1000
     */

    private String id;
    private String first_name;
    private String middel_name;
    private String last_name;
    private String mobile_no;
    private String email_id;
    private String flag;
    private String Timestamp;
    private String password;
    private String admin_id;
    private String pass_status;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddel_name() {
        return middel_name;
    }

    public void setMiddel_name(String middel_name) {
        this.middel_name = middel_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(String Timestamp) {
        this.Timestamp = Timestamp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }


    public String getPass_status() {
        return pass_status;
    }

    public void setPass_status(String pass_status) {
        this.pass_status = pass_status;
    }

    public User() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.first_name);
        dest.writeString(this.middel_name);
        dest.writeString(this.last_name);
        dest.writeString(this.mobile_no);
        dest.writeString(this.email_id);
        dest.writeString(this.flag);
        dest.writeString(this.Timestamp);
        dest.writeString(this.password);
        dest.writeString(this.admin_id);
        dest.writeString(this.status);
        dest.writeString(this.pass_status);
    }

    protected User(Parcel in) {
        this.id = in.readString();
        this.first_name = in.readString();
        this.middel_name = in.readString();
        this.last_name = in.readString();
        this.mobile_no = in.readString();
        this.email_id = in.readString();
        this.flag = in.readString();
        this.Timestamp = in.readString();
        this.password = in.readString();
        this.admin_id = in.readString();
        this.status = in.readString();
        this.pass_status = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
