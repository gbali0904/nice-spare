package com.example.pallaw.electromanage.productmodel;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllBrands;
import com.example.pallaw.electromanage.REST.Model.resources.Brand;
import com.example.pallaw.electromanage.Utils.Util;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Android on 11/24/2017.
 */

public class FilterDialogForBrands extends DialogFragment {


    public static final String TAG = FilterDialogForBrands.class.getSimpleName();
    private static OnshortFilterListener brandSortListener;
    private static FilterDialogForBrands mInstance;

    static List<Brand> brandList = new ArrayList<>();
    @BindView(R.id.list)
    SuperRecyclerView list;
    @BindView(R.id.btnDialogfilterOk)
    Button btnDialogfilterOk;
    @BindView(R.id.btnDialogfilterCancel)
    Button btnDialogfilterCancel;
    private LinearLayoutManager layoutManager;
    private ArrayList<String> selected_brands=new ArrayList<String>();
    private BrandListAdapterForFilter adapter;

    public FilterDialogForBrands() {
        // Required empty public constructor
    }

    public static FilterDialogForBrands getInstance(OnshortFilterListener listener) {
        brandSortListener = listener;

        if (mInstance == null) {
            mInstance = new FilterDialogForBrands();
        }
        return mInstance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter_dialog, container, false);
        ButterKnife.bind(this, view);
        downloadBrands();
        return view;
    }


    /*
    * Brand spinner stuff
    * */
    private void downloadBrands() {
        DataManager.getInstance().getAllBrands("", "", "", "", new Callback<AllBrands>() {
            @Override
            public void onResponse(Call<AllBrands> call, Response<AllBrands> response) {
                if (!response.body().isError()) {
                    Util.hideProgressDialog();
                        brandList.clear();
                        brandList.addAll(response.body().getBrands());
                        layoutManager = new LinearLayoutManager(getActivity());
                        adapter = new BrandListAdapterForFilter(getActivity(), brandList, new FilterDialogForBrands.OnProductFilterListener() {
                            @Override
                            public void OnProductFilterListener(ArrayList<String> selected_categoryList, int adapterPosition) {
                                selected_brands = selected_categoryList;

                            }
                        });
                        list.setLayoutManager(layoutManager);
                        list.setAdapter(adapter);
                        adapter.notifyDataChanged();
                }
            }
            @Override
            public void onFailure(Call<AllBrands> call, Throwable t) {
                Timber.e("downloadBrands", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.btnDialogfilterOk, R.id.btnDialogfilterCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnDialogfilterOk:
                brandSortListener.onBrandsSort(getDialog(),selected_brands);

                break;
            case R.id.btnDialogfilterCancel:
                dismiss();
                break;
        }
    }


    public static interface OnshortFilterListener {

        public void onBrandsSort(Dialog dialog, ArrayList<String> sortingType);
    }

    public interface OnProductFilterListener {
        public void OnProductFilterListener(ArrayList<String> selected_categoryList, int adapterPosition);
    }
}
