package com.example.pallaw.electromanage.Utils;

import android.app.ProgressDialog;
import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by pallaw on 04/10/17.
 */

public class Util {
    private static ProgressDialog progressDialog;

    public static String getMap(String address){
        String encodedAddress="";
        try {
            encodedAddress = URLEncoder.encode(address, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url="https://maps.googleapis.com/maps/api/staticmap?center=" +
                encodedAddress +
                "&zoom=13&size=600x300&maptype=roadmap&markers=color:red%7Clabel:C%7C" +
                encodedAddress +
                "&key=AIzaSyBOSWMP6KumnHeGgDZWqQVuigHmsgoNbsc";
        return url;
    }

    public static void showProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait....");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void hideProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }


}

