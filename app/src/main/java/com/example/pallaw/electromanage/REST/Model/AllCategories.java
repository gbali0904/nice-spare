package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.Category;

import java.util.List;

/**
 * Created by pallaw on 10/10/17.
 */

public class AllCategories {

    /**
     * error : false
     * code : 0
     * msg : List of all user fetched successfully
     * Category : [{"id":"17","name":"TV","Description":"here is tv","flag":"1","timestamp":"2017-10-09 16:25:03.507206","adminid":"1000"},{"id":"18","name":"laptop","Description":"here is laptop","flag":"1","timestamp":"2017-10-09 16:25:58.133315","adminid":"1000"},{"id":"19","name":"laptop","Description":"here is laptop","flag":"1","timestamp":"2017-10-09 16:27:59.412018","adminid":"1000"},{"id":"20","name":"fridge","Description":"here is fridge","flag":"1","timestamp":"2017-10-09 16:31:31.859082","adminid":"1000"}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<com.example.pallaw.electromanage.REST.Model.resources.Category> Category;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Category> getCategory() {
        return Category;
    }

    public void setCategory(List<Category> Category) {
        this.Category = Category;
    }

}
