package com.example.pallaw.electromanage.subcategory;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllCategories;
import com.example.pallaw.electromanage.REST.Model.resources.Category;
import com.example.pallaw.electromanage.Utils.Util;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Android on 11/14/2017.
 */

public class FilterDialogForCategory extends DialogFragment  {


    private static OnshortFilterListener categorySortListener;
    private static FilterDialogForCategory mInstance;
    @BindView(R.id.list)
    SuperRecyclerView list;
    @BindView(R.id.btnDialogfilterOk)
    Button btnDialogfilterOk;
    @BindView(R.id.btnDialogfilterCancel)
    Button btnDialogfilterCancel;
    private LinearLayoutManager layoutManager;
    static List<Category> categoryList = new ArrayList<>();
    private CategoryListAdapterForFilter adapter;

    public ArrayList<String> selected_category=new ArrayList<String>();
    public FilterDialogForCategory() {
        // Required empty public constructor
    }

    public static FilterDialogForCategory getInstance(OnshortFilterListener listener) {
        categorySortListener = listener;

        if (mInstance == null) {
            mInstance = new FilterDialogForCategory();
        }
        return mInstance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filter_dialog, container, false);
        ButterKnife.bind(this, view);
        downloadCategory();
        return view;
    }


    private void downloadCategory() {
        DataManager.getInstance().getAllCategories("", "", "", "", "", "", new Callback<AllCategories>() {
            @Override
            public void onResponse(Call<AllCategories> call, Response<AllCategories> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllCategories.class);
                Timber.i("downloadCategory", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    categoryList.clear();
                    categoryList.addAll(response.body().getCategory());
                    layoutManager = new LinearLayoutManager(getActivity());
                    adapter = new CategoryListAdapterForFilter(getActivity(),categoryList ,new OnCategoryFilterListener(){
                        @Override
                        public void OnCategoryFilterListener(ArrayList<String> selected_categoryList, int adapterPosition) {
                            selected_category=selected_categoryList;

                        }
                    });
                    list.setLayoutManager(layoutManager);
                    list.setAdapter(adapter);
                    adapter.notifyDataChanged();

                }
            }

            @Override
            public void onFailure(Call<AllCategories> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadCategory", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() == null)
            return;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.btnDialogfilterOk, R.id.btnDialogfilterCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnDialogfilterOk:
                categorySortListener.onCategorySort(getDialog(),selected_category);
                break;
            case R.id.btnDialogfilterCancel:
                dismiss();
                break;
        }
    }



    public interface OnCategoryFilterListener {
        void OnCategoryFilterListener(ArrayList<String> selected_categoryList, int adapterPosition);
    }

    public interface OnshortFilterListener {
        void onCategorySort(Dialog dialog, ArrayList<String> sortingType);
    }
}
