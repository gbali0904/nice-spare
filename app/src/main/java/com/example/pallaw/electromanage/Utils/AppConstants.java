package com.example.pallaw.electromanage.Utils;

/**
 * Created by pallaw on 10/10/17.
 */

public class AppConstants {
    public static final String ACTION_TYPE_ADD= "0";
    public static final String ACTION_TYPE_UPDATE = "1";
    public static final String ACTION_TYPE_DELETE = "2";

    public static final int REQUEST_CODE_TO_ADD = 10001;
    public static final int REQUEST_CODE_TO_UPADATE = 10002;

    public static final String EXTRA_UPDATE_ITEM = "update_item";
    public static final String EXTRA_ADDED_ITEM = "added_item";
    public static final String EXTRA_ADAPTER_POSITION = "update_position";

    public static final String STOCK_STATUS_NO = "0";
    public static final String STOCK_STATUS_YES = "1";

    public static final String USER_TYPE_ADMIN = "a";

    public static final String SHORTING_TYPE_HIGH_TO_LOW = "1";
    public static final String SHORTING_TYPE_LOW_TO_HIGH= "2";
    public static final String SHORTING_TYPE_NEWEST_FIRST= "3";
    public static final String SHORTING_TYPE_ATOZ= "4";
    public static final String SHORTING_TYPE_ZTOA= "5";

    public static final String STATUS_ACTIVE = "0";
}
