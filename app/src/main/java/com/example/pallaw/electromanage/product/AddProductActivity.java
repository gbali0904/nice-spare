package com.example.pallaw.electromanage.product;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllBrands;
import com.example.pallaw.electromanage.REST.Model.AllCategories;
import com.example.pallaw.electromanage.REST.Model.AllProductModels;
import com.example.pallaw.electromanage.REST.Model.AllSubCategory;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.ProductOperation;
import com.example.pallaw.electromanage.REST.Model.resources.Brand;
import com.example.pallaw.electromanage.REST.Model.resources.Category;
import com.example.pallaw.electromanage.REST.Model.resources.Product;
import com.example.pallaw.electromanage.REST.Model.resources.ProductModel;
import com.example.pallaw.electromanage.REST.Model.resources.SubCategory;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AddProductActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtAddCatName)
    EditText edtAddCatName;
    @BindView(R.id.btnAddCat)
    Button btnAddCat;
    @BindView(R.id.btnUpdateCat)
    Button btnUpdateCat;
    @BindView(R.id.spiAddProductBrand)
    Spinner spiAddProductBrand;
    @BindView(R.id.spiAddProductCategory)
    Spinner spiAddProductCategory;
    @BindView(R.id.spiAddProductSubCategory)
    Spinner spiAddProductSubCategory;
    @BindView(R.id.spiAddProductModel)
    Spinner spiAddProductModel;
    @BindView(R.id.edtAddCatPrice)
    EditText edtAddCatPrice;
    @BindView(R.id.edtAddCatUnit)
    EditText edtAddCatUnit;
    private String name;
    private Product product;
    private int update_position;
    private List<Category> categories=new ArrayList<>();
    private Category selectedCategory;
    private List<Brand> brands=new ArrayList<>();
    private Brand selectedBrand;
    private List<SubCategory> subCategory=new ArrayList<>();
    private SubCategory selectedSubCategory;
    private List<ProductModel> productModels=new ArrayList<>();
    private ProductModel selectedProductModel;
    private String pricePerUnit;
    private String unit;
    private String model_id;
    private String brand_id;
    private String category_id;
    private String subcategoryid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().getExtras() != null) {
            product = getIntent().getExtras().getParcelable(AppConstants.EXTRA_UPDATE_ITEM);
            update_position = getIntent().getExtras().getInt(AppConstants.EXTRA_ADAPTER_POSITION);

        }

            //download required Data
        downloadRequiredData();

    }

    private void downloadRequiredData() {
        /*
        * Data will be downloaded in sequential order every next download will be started from onResponse method of current download
        * the sequence of download will be as follows
        * 1. brands
        * 2. categories
        * 3. Sub Categories
        * 4. Product models
        *
        * all intent related work will be done after downloading last sequence (check onResponse method of downloadProductModel() )
        * */

        Util.showProgressDialog(this);
        downloadBrands();

        //download category
        downloadCategories();
    }

    private void updateFields() {
        btnAddCat.setVisibility(View.GONE);
        btnUpdateCat.setVisibility(View.VISIBLE);

        edtAddCatName.setText(String.format("%s", product.getProduct_name()));
        edtAddCatPrice.setText(String.format("%s", product.getPrice()));
        edtAddCatUnit.setText(String.format("%s", product.getUnit_flag()));

    /*    for(Brand brand:brands){
            if (product.getBrand().getId().equalsIgnoreCase(brand.getId())){
                spiAddProductBrand.setSelection(brands.indexOf(brand));
                selectedBrand=brand;
            }
        }

        for(Category category:categories){
            if (product.getBrand().getId().equalsIgnoreCase(category.getId())){
                spiAddProductCategory.setSelection(categories.indexOf(category));
                selectedCategory=category;
            }
        }

        for(SubCategory subcat:subCategory){
            if (product.getBrand().getId().equalsIgnoreCase(subcat.getId())){
                spiAddProductSubCategory.setSelection(subCategory.indexOf(subcat));
                selectedSubCategory=subcat;
            }
        }

        for(ProductModel model:productModels){
            if (product.getBrand().getId().equalsIgnoreCase(model.getId())){
                spiAddProductModel.setSelection(subCategory.indexOf(model));
                selectedProductModel=model;
            }
        }

*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(name)) {
            isValid = false;
            edtAddCatName.setError(getResources().getString(R.string.error_brand_name));
        }


        if (TextUtils.isEmpty(pricePerUnit)) {
            isValid = false;
            edtAddCatPrice.setError(getResources().getString(R.string.error_product_price));
        }

        if (TextUtils.isEmpty(unit)) {
            isValid = false;
            edtAddCatUnit.setError(getResources().getString(R.string.error_product_unit));
        }

        return isValid;
    }

    @OnClick({R.id.btnAddCat, R.id.btnUpdateCat})
    public void onViewClicked(View view) {
        name = edtAddCatName.getText().toString().trim();
        pricePerUnit = edtAddCatPrice.getText().toString().trim();
        unit = edtAddCatUnit.getText().toString().trim();

        switch (view.getId()) {
            case R.id.btnAddCat:
                if (validation()) {
                    Util.showProgressDialog(AddProductActivity.this);
                    DataManager.getInstance().productOperation(name, "", "0", Authentication.getInstance().getData().getId(), category_id, subcategoryid, AppConstants.ACTION_TYPE_ADD, "", brand_id, model_id, AppConstants.STOCK_STATUS_NO, pricePerUnit,unit, new Callback<ProductOperation>() {
                        @Override
                        public void onResponse(Call<ProductOperation> call, Response<ProductOperation> response) {
                            Util.hideProgressDialog();
                            String responseJson = new Gson().toJson(response.body());
                            Timber.i("Add product model", responseJson);
                            Toast.makeText(AddProductActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            if (!(response.body().isError())) {
                                Intent intent = new Intent();
                                intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getProduct());
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }
                        @Override
                        public void onFailure(Call<ProductOperation> call, Throwable t) {

                        }
                    });

                }
                break;
            case R.id.btnUpdateCat:
                Util.showProgressDialog(AddProductActivity.this);
                DataManager.getInstance().productOperation(name,"",product.getFlag(),product.getAdmin_id(),category_id,subcategoryid,AppConstants.ACTION_TYPE_UPDATE,product.getId(),brand_id,model_id,product.getStock_status(),pricePerUnit,unit, new Callback<ProductOperation>() {
                    @Override
                    public void onResponse(Call<ProductOperation> call, Response<ProductOperation> response) {
                        Util.hideProgressDialog();
                        String responseJson = new Gson().toJson(response.body());
                        Timber.i("Add product model", responseJson);
                        Toast.makeText(AddProductActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        if (!(response.body().isError())) {
                            Intent intent = new Intent();
                            intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getProduct());
                            intent.putExtra(AppConstants.EXTRA_ADAPTER_POSITION, update_position);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductOperation> call, Throwable t) {

                    }
                });
                break;
        }
    }

    /*
    * Brand spinner stuff
    * */
    private void downloadBrands() {
        DataManager.getInstance().getAllBrands("", "", AppConstants.STATUS_ACTIVE, "", new Callback<AllBrands>() {
            @Override
            public void onResponse(Call<AllBrands> call, Response<AllBrands> response) {
                String responseJson = new Gson().toJson(response.body(), AllBrands.class);
                Timber.i("downloadBrands", responseJson);
                if (!response.body().isError()) {
                    brands = response.body().getBrands();

                    spiAddProductBrand.setAdapter(new BrandCategoryAdapter(AddProductActivity.this, R.layout.item_spinner, brands));
                    if (product != null)
                    {
                        for (Brand brand : brands) {
                            if (brand.getId().equalsIgnoreCase(product.getBrand().getId())) {
                                spiAddProductBrand.setSelection(brands.indexOf(brand));
                            }
                        }
                    }

                    spiAddProductBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            productModels.clear();
                            selectedBrand = brands.get(position);
                            brand_id = selectedBrand.getId();
                            Log.e("this","Brand id: "+brand_id);
                            //download product model
                            downloadProductModels();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                }
                else {
                    productModels.clear();
                    brand_id="0";
                }
            }

            @Override
            public void onFailure(Call<AllBrands> call, Throwable t) {
                Timber.e("downloadBrands", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public class BrandCategoryAdapter extends ArrayAdapter<Brand> {

        private final List<Brand> brands;

        public BrandCategoryAdapter(Context context, int textViewResourceId, List<Brand> objects) {
            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
            this.brands = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.item_spinner, parent, false);
            TextView label = (TextView) row.findViewById(R.id.txtItemSpinnerCategory);
            label.setText(brands.get(position).getName());
            return row;
        }
    }

    /*
    * Product model stuff
    * */
    private void downloadProductModels() {
        DataManager.getInstance().getAllProductModel("", "", AppConstants.STATUS_ACTIVE, "", brand_id, new Callback<AllProductModels>() {
            @Override
            public void onResponse(Call<AllProductModels> call, Response<AllProductModels> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllProductModels.class);
                Timber.i("downloadProductModels", responseJson);
//                Toast.makeText(AddProductActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                if (!response.body().isError()) {
                    productModels = response.body().getModels();
                    spiAddProductModel.setAdapter(new SpinnerProductModelAdapter(AddProductActivity.this, R.layout.item_spinner, productModels));
                    if (product != null)
                    {
                        for(ProductModel model:productModels){
                            if (model.getId().equalsIgnoreCase(product.getModel().getId())){
                                spiAddProductModel.setSelection(productModels.indexOf(model));
                            }
                        }
                    }
                    spiAddProductModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            selectedProductModel = productModels.get(position);
                            model_id = selectedProductModel.getId();
                            Log.e("this",model_id);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }

                else {


                    model_id="0";

                }

            }

            @Override
            public void onFailure(Call<AllProductModels> call, Throwable t) {
                Timber.e("downloadProductModels", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public class SpinnerProductModelAdapter extends ArrayAdapter<ProductModel> {

        private final List<ProductModel> categories;

        public SpinnerProductModelAdapter(Context context, int textViewResourceId, List<ProductModel> objects) {
            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
            this.categories = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.item_spinner, parent, false);
            TextView label = (TextView) row.findViewById(R.id.txtItemSpinnerCategory);
            label.setText(categories.get(position).getName());
            return row;
        }
    }


    /*
    * Category spinner stuff
    * */
    private void downloadCategories() {
        DataManager.getInstance().getAllCategories("", "", "", "", AppConstants.STATUS_ACTIVE, "", new Callback<AllCategories>() {
            @Override
            public void onResponse(Call<AllCategories> call, Response<AllCategories> response) {
                String responseJson = new Gson().toJson(response.body(), AllCategories.class);
                Timber.i("downloadCategory", responseJson);
                if (!response.body().isError()) {
                    categories = response.body().getCategory();
                    spiAddProductCategory.setAdapter(new SpinnerCategoryAdapter(AddProductActivity.this, R.layout.item_spinner, categories));
                    if (product != null)
                    {
                        for(Category category:categories){
                            if (category.getId().equalsIgnoreCase(product.getCategory().getId())){
                                spiAddProductCategory.setSelection(categories.indexOf(category));

                            }
                        }
                    }
                       spiAddProductCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            subCategory.clear();
                            selectedCategory = categories.get(position);
                            category_id = selectedCategory.getId();
                            //download sub Categories
                            downloadSubCategory();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                }
                else {
                    subCategory.clear();
                    category_id="0";
                }

            }

            @Override
            public void onFailure(Call<AllCategories> call, Throwable t) {
                Timber.e("downloadCategory", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public class SpinnerCategoryAdapter extends ArrayAdapter<Category> {

        private final List<Category> categories;

        public SpinnerCategoryAdapter(Context context, int textViewResourceId, List<Category> objects) {
            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
            this.categories = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.item_spinner, parent, false);
            TextView label = (TextView) row.findViewById(R.id.txtItemSpinnerCategory);
            label.setText(categories.get(position).getName());
            return row;
        }
    }


    /*
    * Sub Category stuff
    * */
    private void downloadSubCategory( ) {
        DataManager.getInstance().getAllSubCategory("", "", AppConstants.STATUS_ACTIVE, "", category_id, new Callback<AllSubCategory>() {
            @Override
            public void onResponse(Call<AllSubCategory> call, Response<AllSubCategory> response) {

                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllSubCategory.class);
                Timber.i("downloadSubCategory", responseJson);
//                Toast.makeText(AddProductActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                if (!response.body().isError()) {
                    subCategory = response.body().getCategory();
                    spiAddProductSubCategory.setAdapter(new SpinnerSubCategoryAdapter(AddProductActivity.this, R.layout.item_spinner, subCategory));
                    if (product != null)
                    {
                        for(SubCategory subcat:subCategory){
                            if (subcat.getId().equalsIgnoreCase(product.getSub_category().getId())){
                                spiAddProductSubCategory.setSelection(subCategory.indexOf(subcat));
                            }
                        }
                    }

                    spiAddProductSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            selectedSubCategory = subCategory.get(position);
                            subcategoryid = selectedSubCategory.getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
                else {
                    subcategoryid="0";
                }

            }

            @Override
            public void onFailure(Call<AllSubCategory> call, Throwable t) {

                Util.hideProgressDialog();
                Timber.e("downloadSubCategory", t.getMessage());
                t.printStackTrace();
            }
        });

        checkWetherActivityStartedForUpdate();
    }

    public class SpinnerSubCategoryAdapter extends ArrayAdapter<SubCategory> {

        private final List<SubCategory> categories;

        public SpinnerSubCategoryAdapter(Context context, int textViewResourceId, List<SubCategory> objects) {
            super(context, textViewResourceId, objects);
            // TODO Auto-generated constructor stub
            this.categories = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            //return super.getView(position, convertView, parent);

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.item_spinner, parent, false);
            TextView label = (TextView) row.findViewById(R.id.txtItemSpinnerCategory);
            label.setText(categories.get(position).getName());
            return row;
        }
    }

    private void checkWetherActivityStartedForUpdate() {
        if (getIntent().getExtras() != null) {
            product = getIntent().getExtras().getParcelable(AppConstants.EXTRA_UPDATE_ITEM);
            update_position = getIntent().getExtras().getInt(AppConstants.EXTRA_ADAPTER_POSITION);
            if (product != null) {
                setTitle(getResources().getString(R.string.title_update_product));
                updateFields();
                return;
            }
        }
        setTitle(getResources().getString(R.string.title_add_product));
    }

}
