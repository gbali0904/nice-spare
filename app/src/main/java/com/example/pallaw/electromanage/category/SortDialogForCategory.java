package com.example.pallaw.electromanage.category;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.product.SortDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Android on 11/2/2017.
 */

public class SortDialogForCategory extends DialogFragment {
    public static final String TAG = SortDialog.class.getSimpleName();
    private static OnCategorySortListener categorySortListener;
    private static SortDialogForCategory mInstance;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.rbDialogSortPriceHighToLow)
    RadioButton rbDialogSortPriceHighToLow;
    @BindView(R.id.rbDialogSortPriceLowToHigh)
    RadioButton rbDialogSortPriceLowToHigh;
    @BindView(R.id.rbDialogSortPriceNewestFirst)
    RadioButton rbDialogSortPriceNewestFirst;
    @BindView(R.id.rbDialogSortAtoZ)
    RadioButton rbDialogSortAtoZ;
    @BindView(R.id.rbDialogSortZtoA)
    RadioButton rbDialogSortZtoA;

    public SortDialogForCategory() {
        // Required empty public constructor
    }

    public static SortDialogForCategory getInstance(OnCategorySortListener listener) {
        categorySortListener = listener;

        if (mInstance == null) {
            mInstance = new SortDialogForCategory();
        }
        return mInstance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sort_dialog, container, false);
        ButterKnife.bind(this, view);
        rbDialogSortPriceHighToLow.setVisibility(View.GONE);
        rbDialogSortPriceLowToHigh.setVisibility(View.GONE);
        rbDialogSortPriceNewestFirst.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() == null)
            return;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.btnDialogSortOk, R.id.btnDialogSortCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnDialogSortOk:
                String sortingType = "";
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.rbDialogSortAtoZ:
                        sortingType = AppConstants.SHORTING_TYPE_ATOZ;
                        break;
                    case R.id.rbDialogSortZtoA:
                        sortingType = AppConstants.SHORTING_TYPE_ZTOA;
                        break;
                }

                categorySortListener.onCategorySort(getDialog(), sortingType);

                break;
            case R.id.btnDialogSortCancel:

                getDialog().dismiss();
                break;
        }
    }

    public interface OnCategorySortListener {
        void onCategorySort(Dialog dialog, String sortingType);
    }
}