package com.example.pallaw.electromanage.Utils;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Android on 5/26/2017.
 */

public class RetrofitUtils {
    static int compressionRatio = 1; //1 == originalImage, 2 = 50% compression, 4=25% compress
    public static final String MEDIA_TYPE_MULTIPART_FORM_DATA_VALUE = "multipart/form-data";
    public static final MediaType MEDIA_TYPE_MULTIPART_FORM_DATA = MediaType.parse(MEDIA_TYPE_MULTIPART_FORM_DATA_VALUE);
    public static final String MEDIA_TYPE_doc_VALUE = "application/doc";
    public static final MediaType MEDIA_TYPE_DOC = MediaType.parse(MEDIA_TYPE_doc_VALUE);
    private static final String MEDIA_TYPE_ALL_VALUE = "image/*";
    public static final MediaType MEDIA_TYPE_IMAGE_ALL = MediaType.parse(MEDIA_TYPE_ALL_VALUE);
    private static final String MEDIA_TYPE_PNG_VALUE = "image/png";
    public static final MediaType MEDIA_TYPE_IMAGE_PNG = MediaType.parse(MEDIA_TYPE_PNG_VALUE);
    private static final String MEDIA_TYPE_JPG_VALUE = "image/jpg";
    public static final MediaType MEDIA_TYPE_IMAGE_JPG = MediaType.parse(MEDIA_TYPE_JPG_VALUE);
    private static final String MEDIA_TYPE_JPEG_VALUE = "image/jpeg";
    public static final MediaType MEDIA_TYPE_IMAGE_JPEG = MediaType.parse(MEDIA_TYPE_JPEG_VALUE);
    private static final String MEDIA_TYPE_TEXT_PLAIN_VALUE = "text/plain";
    public static final MediaType MEDIA_TYPE_TEXT_PLAIN = MediaType.parse(MEDIA_TYPE_TEXT_PLAIN_VALUE);
    private static final String MEDIA_TYPE_UNKNOWN_VALUE = "application/octet-stream";
    public static final MediaType MEDIA_TYPE_UNKNOWN = MediaType.parse(MEDIA_TYPE_UNKNOWN_VALUE);
    private static final String MEDIA_TYPE_pdf_VALUE = "application/pdf";
    public static final MediaType MEDIA_TYPE_PDF = MediaType.parse(MEDIA_TYPE_pdf_VALUE);
    private static final String MEDIA_TYPE_ALLVIDEO_VALUE = "video/*";
    public static final MediaType MEDIA_TYPE_ALLVIDEO = MediaType.parse(MEDIA_TYPE_ALLVIDEO_VALUE);
    private static final String MEDIA_TYPE_MP4_VIDEO_VALUE = "video/mp4";
    public static final MediaType MEDIA_TYPE_MP4VIDEO = MediaType.parse(MEDIA_TYPE_MP4_VIDEO_VALUE);

    @NonNull
    public static RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MEDIA_TYPE_MULTIPART_FORM_DATA, descriptionString);
    }

    public static MultipartBody.Part createFilePart(String variableName, String filePath, MediaType mediaType) {
        File file = new File(filePath);
        /*try {
            Bitmap bitmap = BitmapFactory.decodeFile (file.getPath());
            bitmap.compress (Bitmap.CompressFormat.JPEG, compressionRatio, new FileOutputStream(file));
        }
        catch (Throwable t) {
            Log.e("ERROR", "Error compressing file." + t.toString ());
            t.printStackTrace ();
        }*/
        // create RequestBody instance from file
        RequestBody requestFile =RequestBody.create(mediaType, file);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(variableName, file.getName(), requestFile);
    }
    public static HashMap<String, RequestBody> createMultipartRequest(HashMap<String, String> requestValuePairsMap) {
        HashMap<String, RequestBody> requestMap = new HashMap<>();
        Iterator iterator = requestValuePairsMap.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            String value = requestValuePairsMap.get(key);

            requestMap.put(key, createPartFromString(value));
        }

        return requestMap;
    }


}








