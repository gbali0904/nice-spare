package com.example.pallaw.electromanage.product;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.resources.Product;
import com.example.pallaw.electromanage.Utils.AppConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    public static List<Product> data;
    private static ProductListFragment.OnProductDeleteListener brandDeleteListener;
    private static ProductListFragment.OnProductStatusChangeListener brandStatusChangeListener;
    private static ProductListFragment.OnProductStockChangeListener brandStockChangeListener;
    private static ProductListFragment.OnProductUpdateListener onProductUpdateListener;
    private static ProductListFragment.OnProductDeleteMultipleListener productDeleteMultipleListener;
    SparseBooleanArray selectedItemsforDelete = new SparseBooleanArray();

    public ArrayList<Product> selected_usersList = new ArrayList<>();


    public ProductListAdapter(List<Product> data, ProductListFragment.OnProductDeleteListener brandDeleteListener, ProductListFragment.OnProductStatusChangeListener brandStatusChangeListener,
                              ProductListFragment.OnProductUpdateListener onProductUpdateListener,
                              ProductListFragment.OnProductStockChangeListener brandStockChangeListener,
                              ProductListFragment.OnProductDeleteMultipleListener productDeleteMultipleListener) {
        this.data = data;
        this.brandDeleteListener = brandDeleteListener;
        this.brandStatusChangeListener = brandStatusChangeListener;
        this.onProductUpdateListener = onProductUpdateListener;
        this.brandStockChangeListener = brandStockChangeListener;
        this.productDeleteMultipleListener = productDeleteMultipleListener;
    }

    public void notifyDataChanged() {
        selectedItemsforDelete.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem = data.get(position);
        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.parseColor("#22718792"));
        } else {
            // Set the background color for alternate row/item
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }


        holder.bindData();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(Product string) {
        insert(string, data.size());
    }

    public void insert(Product string, int position) {
        data.add(position, string);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(Product[] strings) {
        int startIndex = data.size();
        data.addAll(Arrays.asList(strings));
        notifyItemRangeInserted(startIndex, strings.length);
    }

    public void updateStatus(int position, String flag) {
        data.get(position).setFlag(flag);
        notifyItemChanged(position);
    }

    public void updateStock(int position, String stock_status) {
        data.get(position).setStock_status(stock_status);
        notifyItemChanged(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        @BindView(R.id.sno)
        TextView sno;
        @BindView(R.id.txtItemCatName)
        TextView txtItemCatName;
        @BindView(R.id.txtItemproductprice)
        TextView txtItemproductprice;
        @BindView(R.id.swItemCatStock)
        Switch swItemCatStock;
        @BindView(R.id.delete)
        CheckBox delete;
        @BindView(R.id.btnItemCatDelete)
        Button btnItemCatDelete;
        @BindView(R.id.swItemCatStatus)
        Switch swItemCatStatus;
        @BindView(R.id.txtItemproductdate)
        TextView txtItemproductdate;
        @BindView(R.id.lay)
        LinearLayout lay;
        @BindView(R.id.sno1)
        TextView sno1;
        @BindView(R.id.txtItemCatName1)
        TextView txtItemCatName1;
        @BindView(R.id.txtItemproductprice1)
        TextView txtItemproductprice1;
        @BindView(R.id.swItemCatStock1)
        Switch swItemCatStock1;
        @BindView(R.id.txtItemproductdate1)
        TextView txtItemproductdate1;
        @BindView(R.id.lay1)
        LinearLayout lay1;
        @BindView(R.id.view)
        View view;
        public Product mItem;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            delete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public String id;

                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        selected_usersList.add(mItem);
                    } else {
                        selected_usersList.remove(mItem);
                    }
                    productDeleteMultipleListener.OnProductDeleteMultipleListener(selected_usersList, getAdapterPosition());
                    selectedItemsforDelete.put(getAdapterPosition(), b);
                }
            });

            btnItemCatDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // brandDeleteListener.onProductDelete(mItem,getAdapterPosition());
                }
            });


            swItemCatStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int status = (swItemCatStatus.isChecked()) ? 1 : 0;
                    brandStatusChangeListener.onProductStatusChange(mItem, getAdapterPosition(), String.valueOf(status));
                }
            });

            swItemCatStock.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    int stockStatus = (swItemCatStock.isChecked()) ? 1 : 0;
                    brandStockChangeListener.onProductStockChange(mItem, getAdapterPosition(), String.valueOf(stockStatus));
                }
            });

            if ((Authentication.getInstance().getData().getStatus().equalsIgnoreCase(AppConstants.USER_TYPE_ADMIN))) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onProductUpdateListener.onProductUpdate(mItem, getAdapterPosition());
                    }
                });
            }

        }

        public void bindData() {
            int adapterPosition = getAdapterPosition() + 1;
            txtItemCatName.setText(String.format("%s", mItem.getProduct_name()));
            txtItemproductprice.setText(String.format("%s", "₹ " + mItem.getPrice()));
            swItemCatStatus.setChecked((data.get(getAdapterPosition()).getFlag().equalsIgnoreCase("0") ? false : true));
            swItemCatStock.setChecked((data.get(getAdapterPosition()).getStock_status().equalsIgnoreCase("0") ? false : true));
            sno.setText("" + adapterPosition);
            delete.setChecked(selectedItemsforDelete.get(getAdapterPosition()));
            // String[] substring = mItem.getTimestamp().split("\\.");
            txtItemproductdate.setText("" + mItem.getTimestamp());



            if (!(Authentication.getInstance().getData().getStatus().equalsIgnoreCase(AppConstants.USER_TYPE_ADMIN))) {
          /*      btnItemCatDelete.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
                swItemCatStatus.setEnabled(false);
                swItemCatStock.setEnabled(false);*/

                lay1.setVisibility(View.VISIBLE);
                lay.setVisibility(View.GONE);
                sno1.setText("" + adapterPosition);
                txtItemproductprice1.setText(String.format("%s", "₹ " + mItem.getPrice()));
                txtItemCatName1.setText(String.format("%s", mItem.getProduct_name()));
                swItemCatStock1.setChecked((data.get(getAdapterPosition()).getStock_status().equalsIgnoreCase("0") ? false : true));
                txtItemproductdate1.setText("" + mItem.getTimestamp());
                swItemCatStock1.setEnabled(false);
            }

        }
    }
}
