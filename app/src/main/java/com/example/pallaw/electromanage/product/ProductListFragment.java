package com.example.pallaw.electromanage.product;


import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.AllFilterProduct;
import com.example.pallaw.electromanage.REST.Model.AllProduct;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.DeleteMultipleProduct;
import com.example.pallaw.electromanage.REST.Model.ProductOperation;
import com.example.pallaw.electromanage.REST.Model.SearchResult;
import com.example.pallaw.electromanage.REST.Model.resources.Product;
import com.example.pallaw.electromanage.REST.Model.resources.User;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.google.gson.Gson;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.swipe.SparseItemRemoveAnimator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener {

    public static final String TAG = ProductListFragment.class.getSimpleName();
    static List<Product> productList = new ArrayList<>();
    private List<Product> productListForSearch=new ArrayList<>();
    @BindView(R.id.delete)
    TextView delete;
    @BindView(R.id.lay)
    LinearLayout lay;
    @BindView(R.id.lay1)
    LinearLayout lay1;
    @BindView(R.id.list)
    SuperRecyclerView list;
    @BindView(R.id.fab)
    Button fab;
    @BindView(R.id.transparentOverlay)
    View transparentOverlay;
    @BindView(R.id.listContainer)
    FrameLayout listContainer;

    private LinearLayoutManager layoutManager;
    private ProductListAdapter adapter;
    private SparseItemRemoveAnimator mSparseAnimator;
    private MenuItem searchMenuItem;
    private MenuItem sortMenuItem;
    private SearchView searchView;
    private String flag = "";
    private ArrayList<Product> selected_usersList;
    private StringBuilder builder;
    private ActionMode mActionMode;

    private String deleteMultiplist;
    private MenuItem filterMenuItem;
    private HashMap<String, List<String>> filterData;
    private boolean isFilterApplied = false;
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            mActionMode.getMenuInflater().inflate(R.menu.context, menu);
            return true;

        }

        // Called each time the action mode is shown.
        // Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:

                    AlertDialog.Builder dilaogbuilder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
                    } else {
                        dilaogbuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogStyle);
                    }
                    dilaogbuilder.setTitle("Delete Product")
                            .setCancelable(true)
                            .setMessage("Are you sure you want to delete this Product?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    builder = new StringBuilder();
                                    for (Product product :
                                            selected_usersList) {
                                        builder.append(product.getId()).append(",");
                                    }
                                    deleteMultiplist = builder.toString().substring(0, builder.length() - 1);

                                    deleteProduct(deleteMultiplist);
                                    Log.e("this", "Ids are :" + builder.toString().substring(0, builder.length() - 1));
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();


                    return true;

                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };


    public ProductListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.product, menu);
        setupOptionMenu(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private boolean setupOptionMenu(Menu menu) {
        searchMenuItem = menu.findItem(R.id.searchItem);
        sortMenuItem = menu.findItem(R.id.action_sort);
        filterMenuItem = menu.findItem(R.id.action_filter);
        filterMenuItem.setVisible(true);
      /*  MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                sortMenuItem.setVisible(false);
                transparentOverlay.setVisibility(View.VISIBLE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                sortMenuItem.setVisible(true);
                transparentOverlay.setVisibility(View.GONE);
                getActivity().invalidateOptionsMenu();
                return true;
            }
        });
*/

        if (searchMenuItem != null) {
            searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
            if (null == searchView) {
                return true;
            }
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    //   submitQuerry(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    submitQuerry(query);
                    return true;
                }
            });
            searchView.setQueryHint(getString(R.string.action_search));
            //applying the font to hint text inside of SearchView
            SearchView.SearchAutoComplete searchText = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
            searchText.setHintTextColor(getContext().getResources().getColor(R.color.colorAccent));
            searchText.setTextColor(getContext().getResources().getColor(R.color.white));
            searchText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        return false;
    }

    private void submitQuerry(String query) {
//        if (isFilterApplied) {
//            searchLocaly(query);
//        } else {
//            searchRemote(query);
//        }
        searchLocaly(query);
    }

    private void searchLocaly(String query) {
        Timber.e(query);
        ArrayList<Product> searchList=new ArrayList<>();
        for (Product product : productListForSearch) {
            if (product.getProduct_name().toLowerCase().contains(query)||
                    product.getDescription().toLowerCase().contains(query)||
                    product.getCategory().getName().toLowerCase().contains(query)||
                    product.getBrand().getName().toLowerCase().contains(query)||
                    product.getModel().getName().toLowerCase().contains(query)||
                    product.getSub_category().getName().toLowerCase().contains(query)) {
                searchList.add(product);
            }
        }

        if(searchList.size()>0){//if found something related to search querry
            productList.clear();
            productList.addAll(searchList);
            adapter.notifyDataChanged();
        }
        Timber.e("querry- "+query+", search result size-"+searchList.size());
    }

    private void searchRemote(String query) {
        if (!(Authentication.getInstance().getData().getStatus().equalsIgnoreCase(AppConstants.USER_TYPE_ADMIN))) {
            flag = "0";
        } else {
            flag = "1";
        }
        DataManager.getInstance().searchProduct(query, flag, new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                Util.hideProgressDialog();
                Timber.i("search product", new Gson().toJson(response.body()));
                // Toast.makeText(getActivity(), ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    productList.clear();
                    productList.addAll(response.body().getProducts());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {
                Util.hideProgressDialog();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                SortDialog sortDialog = SortDialog.getInstance(new SortDialog.OnProductSortListener() {
                    @Override
                    public void onProductSort(Dialog dialog, String sortingType) {
                        dialog.dismiss();
                        sortListAccTo(sortingType);
                    }
                });
                sortDialog.show(getActivity().getSupportFragmentManager(), SortDialog.TAG);
                break;
            case R.id.action_filter:
                FilterDialogForProduct filterDialogForProduct = FilterDialogForProduct.getInstance(new FilterDialogForProduct.OnFilterListener() {

                    @Override
                    public void OnFilterListener(Dialog dialog) {
                        dialog.dismiss();
                        getProductfilterList();
                    }
                });
                filterDialogForProduct.show(getActivity().getSupportFragmentManager(), FilterDialogForProduct.TAG);

        }
        return super.onOptionsItemSelected(item);
    }

    private void getProductfilterList() {
        Util.showProgressDialog(getActivity());
        if (!(Authentication.getInstance().getData().getStatus().equalsIgnoreCase(AppConstants.USER_TYPE_ADMIN))) {
            flag = "0";
        }
        String filter = new Gson().toJson(FilterParams.getInstance());
        Log.e("this", filter);
        DataManager.getInstance().getFilterAllProduct(filter, flag, new Callback<AllFilterProduct>() {
            @Override
            public void onResponse(Call<AllFilterProduct> call, Response<AllFilterProduct> response) {
                Util.hideProgressDialog();
                Timber.i("getProductfilterList", new Gson().toJson(response));
                Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    /*
                    * Clear selections from FilterParams singleton
                    * */
                    FilterParams.getInstance().getSubcategory().clear();
                    FilterParams.getInstance().getBrands().clear();
                    FilterParams.getInstance().getCategory().clear();
                    FilterParams.getInstance().getModels().clear();

                    productList.clear();
                    productList.addAll(response.body().getProducts());
                    adapter.notifyDataSetChanged();

                    productListForSearch.clear();
                    productListForSearch.addAll(productList);

                } else {
                    FilterParams.getInstance().getSubcategory().clear();
                    FilterParams.getInstance().getBrands().clear();
                    FilterParams.getInstance().getCategory().clear();
                    FilterParams.getInstance().getModels().clear();
                    productList.clear();
                    adapter.notifyDataSetChanged();

                    productListForSearch.clear();
                    productListForSearch.addAll(productList);
                }
            }

            @Override
            public void onFailure(Call<AllFilterProduct> call, Throwable t) {
                Util.hideProgressDialog();
                t.printStackTrace();
            }
        });
    }

    private void sortListAccTo(String sortingType) {
        Util.showProgressDialog(getActivity());
        if (!(Authentication.getInstance().getData().getStatus().equalsIgnoreCase(AppConstants.USER_TYPE_ADMIN))) {
            flag = "0";
        }
        DataManager.getInstance().sortProduct(sortingType, flag, new Callback<AllProduct>() {
            @Override
            public void onResponse(Call<AllProduct> call, Response<AllProduct> response) {
                Util.hideProgressDialog();
                Timber.i("Sort product list", new Gson().toJson(response.body()));
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                if (!(response.body().isError())) {
                    productList.clear();
                    productList.addAll(response.body().getProducts());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<AllProduct> call, Throwable t) {
                Util.hideProgressDialog();
                t.printStackTrace();

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        downloadProductModels();
        layoutManager = new LinearLayoutManager(getActivity());
        adapter = new ProductListAdapter(productList, new OnProductDeleteListener() {
            @Override
            public void onProductDelete(Product mItem, int adapterPosition) {
                removeProduct(mItem, adapterPosition);
            }
        }, new OnProductStatusChangeListener() {
            @Override
            public void onProductStatusChange(Product mItem, int adapterPosition, String status) {
                changeProductStatus(mItem, adapterPosition, status);
            }
        }, new OnProductUpdateListener() {
            @Override
            public void onProductUpdate(Product mItem, int adapterPosition) {
                Intent toUpdateEmp = new Intent(getActivity(), AddProductActivity.class);
                toUpdateEmp.putExtra(AppConstants.EXTRA_UPDATE_ITEM, mItem);
                toUpdateEmp.putExtra(AppConstants.EXTRA_ADAPTER_POSITION, adapterPosition);
                startActivityForResult(toUpdateEmp, AppConstants.REQUEST_CODE_TO_UPADATE);
            }
        }, new OnProductStockChangeListener() {
            @Override
            public void onProductStockChange(Product mItem, int adapterPosition, String stockStatus) {
                changeProductStockStatus(mItem, adapterPosition, stockStatus);
            }
        }, new OnProductDeleteMultipleListener() {
            @Override
            public void OnProductDeleteMultipleListener(ArrayList<Product> userlist, int adapterPosition) {
                selected_usersList = userlist;
                if (selected_usersList.size() > 0) {
                    if (mActionMode == null) {
                        mActionMode = getActivity().startActionMode(mActionModeCallback);
                    }
                } else {
                    if (mActionMode != null) {
                        mActionMode.finish();
                        selected_usersList.clear();
                    }
                }
            }


        }

        );
        list.setRefreshListener(this);
        list.setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);
//        list.setupMoreListener(this, 1);

        if (!(Authentication.getInstance().getData().getStatus().equalsIgnoreCase(AppConstants.USER_TYPE_ADMIN))) {
            fab.setVisibility(View.GONE);
            flag = "0";
            // delete.setVisibility(View.GONE);

            lay.setVisibility(View.GONE);
            lay1.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void deleteProduct(String deleteMultiplist) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().deleteMultipleproduct(deleteMultiplist, new Callback<DeleteMultipleProduct>() {
            @Override
            public void onResponse(Call<DeleteMultipleProduct> call, Response<DeleteMultipleProduct> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), DeleteMultipleProduct.class);
                Timber.i("removeProduct", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    mActionMode.finish();
                    selected_usersList.clear();

                    productList.clear();
                    downloadProductModels();
                }
            }

            @Override
            public void onFailure(Call<DeleteMultipleProduct> call, Throwable t) {
                Timber.e("removeProduct", t.getMessage());
                t.printStackTrace();
            }
        });

    }

    private void changeProductStockStatus(Product mItem, final int adapterPosition, String stockStatus) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().productOperation(mItem.getProduct_name(), mItem.getDescription(), mItem.getFlag(), mItem.getAdmin_id(), mItem.getCategory().getId(), mItem.getSub_category().getId(), AppConstants.ACTION_TYPE_UPDATE, mItem.getId(), mItem.getBrand().getId(), mItem.getModel().getId(), stockStatus, mItem.getPrice(), mItem.getUnit_flag(), new Callback<ProductOperation>() {
            @Override
            public void onResponse(Call<ProductOperation> call, Response<ProductOperation> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), ProductOperation.class);
                Timber.i("changeProductStatus", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    adapter.updateStock(adapterPosition, response.body().getProduct().getStock_status());
                }
            }

            @Override
            public void onFailure(Call<ProductOperation> call, Throwable t) {
                Util.hideProgressDialog();
                t.printStackTrace();
            }
        });

    }

    private void changeProductStatus(Product mItem, final int adapterPosition, String status) {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().productOperation(mItem.getProduct_name(), mItem.getDescription(), status, mItem.getAdmin_id(), mItem.getCategory().getId(), mItem.getSub_category().getId(), AppConstants.ACTION_TYPE_UPDATE, mItem.getId(), mItem.getBrand().getId(), mItem.getModel().getId(), mItem.getStock_status(), mItem.getPrice(), mItem.getUnit_flag(), new Callback<ProductOperation>() {
            @Override
            public void onResponse(Call<ProductOperation> call, Response<ProductOperation> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), ProductOperation.class);
                Timber.i("changeProductStatus", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    adapter.updateStatus(adapterPosition, response.body().getProduct().getFlag());
                }
            }

            @Override
            public void onFailure(Call<ProductOperation> call, Throwable t) {
                Util.hideProgressDialog();
                t.printStackTrace();
            }
        });
    }

    private void removeProduct(Product mItem, final int adapterPosition) {
        Toast.makeText(getActivity(), "Category removed", Toast.LENGTH_SHORT).show();
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().productOperation(mItem.getProduct_name(), mItem.getDescription(), mItem.getFlag(), mItem.getAdmin_id(), mItem.getCategory().getId(), mItem.getSub_category().getId(), AppConstants.ACTION_TYPE_DELETE, mItem.getId(), mItem.getBrand().getId(), mItem.getModel().getId(), mItem.getStock_status(), mItem.getPrice(), mItem.getUnit_flag(), new Callback<ProductOperation>() {
            @Override
            public void onResponse(Call<ProductOperation> call, Response<ProductOperation> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), ProductOperation.class);
                Timber.i("removeProduct", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!(response.body().isError())) {
                    adapter.remove(adapterPosition);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ProductOperation> call, Throwable t) {
                Timber.e("removeProduct", t.getMessage());
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getActivity().getResources().getString(R.string.title_products_list_fragment));


    }

    private void downloadProductModels() {
        Util.showProgressDialog(getActivity());
        DataManager.getInstance().getAllProduct("", "", "", "", "", "", "", "", flag, new Callback<AllProduct>() {
            @Override
            public void onResponse(Call<AllProduct> call, Response<AllProduct> response) {
                Util.hideProgressDialog();
                String responseJson = new Gson().toJson(response.body(), AllProduct.class);
                Timber.i("downloadProductModels", responseJson);
                Toast.makeText(getActivity(), "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                if (!response.body().isError()) {
                    productList.clear();
                    productList.addAll(response.body().getProducts());
                    list.setLayoutManager(layoutManager);
                    list.setAdapter(adapter);
                    adapter.notifyDataChanged();

                    //replicate product list which will be used at the time of search.
                    productListForSearch.clear();
                    productListForSearch.addAll(productList);
                }
            }

            @Override
            public void onFailure(Call<AllProduct> call, Throwable t) {
                Util.hideProgressDialog();
                Timber.e("downloadProductModels", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
//        Toast.makeText(getActivity(), "Refresh", Toast.LENGTH_SHORT).show();
        downloadProductModels();
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        Toast.makeText(getActivity(), "More", Toast.LENGTH_SHORT).show();
        User user = new User();
        user.setFirst_name("More asked, more served");
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivityForResult(new Intent(getActivity(), AddProductActivity.class), AppConstants.REQUEST_CODE_TO_ADD);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppConstants.REQUEST_CODE_TO_ADD:
                    productList.add((Product) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM));
                    adapter.notifyDataSetChanged();
                    break;
                case AppConstants.REQUEST_CODE_TO_UPADATE:
                    productList.set(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION, 0),
                            ((Product) data.getParcelableExtra(AppConstants.EXTRA_ADDED_ITEM)));
                    adapter.notifyItemChanged(data.getIntExtra(AppConstants.EXTRA_ADAPTER_POSITION, 0));
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    interface OnProductDeleteListener {
        void onProductDelete(Product mItem, int adapterPosition);
    }

    interface OnProductStatusChangeListener {
        void onProductStatusChange(Product mItem, int adapterPosition, String status);


    }

    interface OnProductStockChangeListener {
        void onProductStockChange(Product mItem, int adapterPosition, String s);

    }

    interface OnProductUpdateListener {
        void onProductUpdate(Product mItem, int adapterPosition);
    }

    interface OnProductDeleteMultipleListener {

        void OnProductDeleteMultipleListener(ArrayList<Product> selected_usersList, int adapterPosition);
    }

    public interface OnProductListener {
        void OnProductListener(HashMap<String, List<String>> selected_categoryList);
    }


}
