package com.example.pallaw.electromanage.productmodel;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.Model.resources.Brand;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Android on 11/24/2017.
 */

class BrandListAdapterForFilter extends RecyclerView.Adapter<BrandListAdapterForFilter.ViewHolder>  {
    private final FragmentActivity activity;
    private final List<Brand> brandList;
    private final FilterDialogForBrands.OnProductFilterListener onProductFilterListener;
    SparseBooleanArray selectedItemsforFilter= new SparseBooleanArray();
    private ArrayList<String> selected_brandsList =new ArrayList<>();

    public BrandListAdapterForFilter(FragmentActivity activity, List<Brand> brandList, FilterDialogForBrands.OnProductFilterListener onProductFilterListener) {
        this.activity=activity;
        this.brandList=brandList;
        this.onProductFilterListener=onProductFilterListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_list_for_filter, parent, false);
        return new BrandListAdapterForFilter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem = brandList.get(position);
        holder.bindData();
    }

    public void notifyDataChanged() {
        selectedItemsforFilter.clear();
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return brandList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.category_name)
        TextView categoryName;
        public Brand mItem;
        @BindView(R.id.lay)
        LinearLayout lay;
        @BindView(R.id.check)
        CheckBox check;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    String id = mItem.getId();
                    if (b) {
                        selected_brandsList.add(id);
                    }
                    else {
                        selected_brandsList.remove(id);

                    }
                    onProductFilterListener.OnProductFilterListener(selected_brandsList,getAdapterPosition());
                    selectedItemsforFilter.put(getAdapterPosition(),b);
                }
            });
        }

        public void bindData() {
            categoryName.setText(String.format("%s", mItem.getName()));
            check.setChecked(selectedItemsforFilter.get(getAdapterPosition()));

        }
    }
}
