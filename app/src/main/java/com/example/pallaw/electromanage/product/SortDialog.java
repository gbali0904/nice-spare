package com.example.pallaw.electromanage.product;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioGroup;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.Utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SortDialog extends DialogFragment {


    public static final String TAG = SortDialog.class.getSimpleName();
    private static OnProductSortListener productSortListener;
    private static SortDialog mInstance;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    public SortDialog() {
        // Required empty public constructor
    }

    public static SortDialog getInstance(OnProductSortListener listener){
        productSortListener = listener;

        if(mInstance==null) {
            mInstance=new SortDialog();
        }
        return mInstance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sort_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() == null)
            return;
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.btnDialogSortOk, R.id.btnDialogSortCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnDialogSortOk:
                String sortingType="";
                switch (radioGroup.getCheckedRadioButtonId()){
                    case R.id.rbDialogSortPriceHighToLow:
                        sortingType= AppConstants.SHORTING_TYPE_HIGH_TO_LOW;
                        break;
                    case R.id.rbDialogSortPriceLowToHigh:
                        sortingType= AppConstants.SHORTING_TYPE_LOW_TO_HIGH;
                        break;
                    case R.id.rbDialogSortPriceNewestFirst:
                        sortingType= AppConstants.SHORTING_TYPE_NEWEST_FIRST;
                        break;
                    case R.id.rbDialogSortAtoZ:
                        sortingType= AppConstants.SHORTING_TYPE_ATOZ;
                        break;
                    case R.id.rbDialogSortZtoA:
                        sortingType= AppConstants.SHORTING_TYPE_ZTOA;
                        break;
                }

                productSortListener.onProductSort(getDialog(),sortingType);

                break;
            case R.id.btnDialogSortCancel:
                getDialog().dismiss();
                break;
        }
    }

    public interface OnProductSortListener{
        void onProductSort(Dialog dialog, String sortingType);
    }
}
