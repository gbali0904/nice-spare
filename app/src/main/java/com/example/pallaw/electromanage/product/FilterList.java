package com.example.pallaw.electromanage.product;

import android.os.Parcel;
import android.os.Parcelable;

public class FilterList implements Parcelable {

  private String filtertype;
  private String id;
  private String name;
  private boolean isFavorite;

  public FilterList(String name, String id, String filtertype, boolean isFavorite) {
    this.name = name;
    this.isFavorite = isFavorite;
    this.id = id;
    this.filtertype = filtertype;
  }

  protected FilterList(Parcel in) {
    name = in.readString();
  }

  public String getName() {
    return name;
  }
  public String getId() {
    return id;
  }
  public String getFiltertype() {
    return filtertype;
  }

  public boolean isFavorite() {
    return isFavorite;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof FilterList)) return false;

    FilterList artist = (FilterList) o;

    if (isFavorite() != artist.isFavorite()) return false;
    return getName() != null ? getName().equals(artist.getName()) : artist.getName() == null;

  }

  @Override
  public int hashCode() {
    int result = getName() != null ? getName().hashCode() : 0;
    result = 31 * result + (isFavorite() ? 1 : 0);
    return result;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(name);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<FilterList> CREATOR = new Creator<FilterList>() {
    @Override
    public FilterList createFromParcel(Parcel in) {
      return new FilterList(in);
    }

    @Override
    public FilterList[] newArray(int size) {
      return new FilterList[size];
    }
  };
}

