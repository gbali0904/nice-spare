package com.example.pallaw.electromanage.REST.Model.resources;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pallaw on 13/10/17.
 */

public class Product implements Parcelable {


    /**
     * id : 6
     * Description : here is my product
     * flag : 0
     * product_name : product name
     * stock_status : n
     * timestamp : 2017-10-12 05:22:50.371196
     * unit_flag : 10
     * price : 10000
     * admin_id : 1000
     * brand : {"brand_id":"8","brand_name":"lenovo"}
     * model : {"model_id":"2","model_name":"Y20TVB"}
     * category : {"category_id":"21","category_name":"Cooler"}
     * sub_category : {"sub_category_id":"9","sub_category_name":"New cooler"}
     */

    private String id;
    private String Description;
    private String flag;
    private String product_name;
    private String stock_status;
    private String timestamp;
    private String unit_flag;
    private String price;
    private String admin_id;
    private Brand brand;
    private ProductModel model;
    private Category category;
    private SubCategory sub_category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getStock_status() {
        return stock_status;
    }

    public void setStock_status(String stock_status) {
        this.stock_status = stock_status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUnit_flag() {
        return unit_flag;
    }

    public void setUnit_flag(String unit_flag) {
        this.unit_flag = unit_flag;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public ProductModel getModel() {
        return model;
    }

    public void setModel(ProductModel model) {
        this.model = model;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public SubCategory getSub_category() {
        return sub_category;
    }

    public void setSub_category(SubCategory sub_category) {
        this.sub_category = sub_category;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.Description);
        dest.writeString(this.flag);
        dest.writeString(this.product_name);
        dest.writeString(this.stock_status);
        dest.writeString(this.timestamp);
        dest.writeString(this.unit_flag);
        dest.writeString(this.price);
        dest.writeString(this.admin_id);
        dest.writeParcelable(this.brand, flags);
        dest.writeParcelable(this.model, flags);
        dest.writeParcelable(this.category, flags);
        dest.writeParcelable(this.sub_category, flags);
    }

    public Product() {
    }

    protected Product(Parcel in) {
        this.id = in.readString();
        this.Description = in.readString();
        this.flag = in.readString();
        this.product_name = in.readString();
        this.stock_status = in.readString();
        this.timestamp = in.readString();
        this.unit_flag = in.readString();
        this.price = in.readString();
        this.admin_id = in.readString();
        this.brand = in.readParcelable(Brand.class.getClassLoader());
        this.model = in.readParcelable(ProductModel.class.getClassLoader());
        this.category = in.readParcelable(Category.class.getClassLoader());
        this.sub_category = in.readParcelable(SubCategory.class.getClassLoader());
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
