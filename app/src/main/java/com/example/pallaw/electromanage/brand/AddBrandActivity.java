package com.example.pallaw.electromanage.brand;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.DataManager;
import com.example.pallaw.electromanage.REST.Model.Authentication;
import com.example.pallaw.electromanage.REST.Model.BrandManage;
import com.example.pallaw.electromanage.REST.Model.resources.Brand;
import com.example.pallaw.electromanage.Utils.AppConstants;
import com.example.pallaw.electromanage.Utils.Util;
import com.google.gson.Gson;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AddBrandActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtAddCatName)
    EditText edtAddCatName;
    @BindView(R.id.textInputLayout3)
    TextInputLayout textInputLayout3;
    @BindView(R.id.spiAddProductBrand)
    Spinner spiAddProductBrand;
    @BindView(R.id.lay)
    LinearLayout lay;
    @BindView(R.id.btnAddCat)
    Button btnAddCat;
    @BindView(R.id.btnUpdateCat)
    Button btnUpdateCat;
    private String name;
    private Brand brand;
    private int update_position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_brand);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().getExtras() != null) {
            brand = getIntent().getExtras().getParcelable(AppConstants.EXTRA_UPDATE_ITEM);
            update_position = getIntent().getExtras().getInt(AppConstants.EXTRA_ADAPTER_POSITION);
            if (brand != null) {
                setTitle(getResources().getString(R.string.title_update_brands));
                updateFields();
                return;
            }
        }

        setTitle(getResources().getString(R.string.title_add_brands));
    }

    private void updateFields() {
        btnAddCat.setVisibility(View.GONE);
        btnUpdateCat.setVisibility(View.VISIBLE);

        edtAddCatName.setText(String.format("%s", brand.getName()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(name)) {
            isValid = false;
            edtAddCatName.setError(getResources().getString(R.string.error_brand_name));
        }

        return isValid;
    }

    @OnClick({R.id.btnAddCat, R.id.btnUpdateCat})
    public void onViewClicked(View view) {
        name = edtAddCatName.getText().toString().trim();

        switch (view.getId()) {
            case R.id.btnAddCat:
                if (validation()) {
                    Util.showProgressDialog(AddBrandActivity.this);
                    DataManager.getInstance().brandManage(name, "", "0", Authentication.getInstance().getData().getId(), AppConstants.ACTION_TYPE_ADD, "", new Callback<BrandManage>() {
                        @Override
                        public void onResponse(Call<BrandManage> call, Response<BrandManage> response) {
                            Util.hideProgressDialog();
                            String responseJson = new Gson().toJson(response.body());
                            Timber.i("Add category", responseJson);
                            Toast.makeText(AddBrandActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            if (!(response.body().isError())) {
                                Intent intent = new Intent();
                                intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getBrand());
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<BrandManage> call, Throwable t) {

                        }
                    });

                }
                break;
            case R.id.btnUpdateCat:
                Util.showProgressDialog(AddBrandActivity.this);
                DataManager.getInstance().brandManage(name, "", brand.getFlag(), Authentication.getInstance().getData().getId(), AppConstants.ACTION_TYPE_UPDATE, brand.getId(), new Callback<BrandManage>() {
                    @Override
                    public void onResponse(Call<BrandManage> call, Response<BrandManage> response) {
                        Util.hideProgressDialog();
                        String responseJson = new Gson().toJson(response.body());
                        Timber.i("Add category", responseJson);
                        Toast.makeText(AddBrandActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        if (!(response.body().isError())) {
                            Intent intent = new Intent();
                            intent.putExtra(AppConstants.EXTRA_ADDED_ITEM, response.body().getBrand());
                            intent.putExtra(AppConstants.EXTRA_ADAPTER_POSITION, update_position);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<BrandManage> call, Throwable t) {

                    }
                });
                break;
        }
    }
}
