package com.example.pallaw.electromanage.REST.Model;

import com.example.pallaw.electromanage.REST.Model.resources.User;

import java.util.List;

/**
 * Created by pallaw on 07/10/17.
 */

public class AllEmployees {


    /**
     * error : false
     * code : 0
     * msg : List of all user fetched successfully
     * User : [{"id":"1000","first_name":"Gunjan","middel_name":"","last_name":"Bali","mobile_no":"7696343553","email_id":"gbali0904@gmail.com","flag":"1","Timestamp":"2017-10-06 05:44:19.737867","password":"j_CEf=By","admin_id":"1000"}]
     */

    private boolean error;
    private int code;
    private String msg;
    private List<com.example.pallaw.electromanage.REST.Model.resources.User> User;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<User> getUser() {
        return User;
    }

    public void setUser(List<User> User) {
        this.User = User;
    }

}
