package com.example.pallaw.electromanage.splash;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.Utils.UserManager;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                UserManager userManager = UserManager.startSession(getApplicationContext());
                userManager.checkLogin();
            }
        }, SPLASH_TIME_OUT);
    }
}
