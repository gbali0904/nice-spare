package com.example.pallaw.electromanage.subcategory;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.pallaw.electromanage.R;
import com.example.pallaw.electromanage.REST.Model.resources.Category;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Android on 11/14/2017.
 */

class CategoryListAdapterForFilter extends RecyclerView.Adapter<CategoryListAdapterForFilter.ViewHolder> {
    private final FragmentActivity activity;
    private final List<Category> categoryList;
    private ArrayList<String> selected_CategoryList=new ArrayList<>();
    private FilterDialogForCategory.OnCategoryFilterListener categoryMultipleListener;
    SparseBooleanArray selectedItemsforFilter= new SparseBooleanArray();

    public CategoryListAdapterForFilter(FragmentActivity activity, List<Category> categoryList, FilterDialogForCategory.OnCategoryFilterListener onCategoryFilterListener) {
        this.activity = activity;
        this.categoryList = categoryList;
        categoryMultipleListener=onCategoryFilterListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_list_for_filter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem = categoryList.get(position);
        holder.bindData();
    }

    @Override
    public int getItemCount() {
        return categoryList.size();

    }
    public void notifyDataChanged() {
        selectedItemsforFilter.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.category_name)
        TextView categoryName;
        public Category mItem;
        @BindView(R.id.lay)
        LinearLayout lay;
        @BindView(R.id.check)
        CheckBox check;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    String id = mItem.getId();
                    if (b) {
                        selected_CategoryList.add(id);
                    }
                    else {
                        selected_CategoryList.remove(id);

                    }
                    categoryMultipleListener.OnCategoryFilterListener(selected_CategoryList,getAdapterPosition());
                    selectedItemsforFilter.put(getAdapterPosition(),b);
                }
            });
        }

        public void bindData() {
            categoryName.setText(String.format("%s", mItem.getName()));
            check.setChecked(selectedItemsforFilter.get(getAdapterPosition()));

        }
    }
}
